$(document).ready(function () {
        var form_register = $('#formpersonal');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);

        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                txtNombre:{
                  required: true
                },
                txtApellidos:{
                    required: true
                },
                txtUsuario:{
                    required: true
                },
                txtPass:{
                    minlength: 6,
                    required: true
                },
                txtPass2: {
                    equalTo: txtPass,
                    minlength: 6,
                    required: true
                }
            },
            
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });

        //======================================================================================
        $('.rowlink').on('click',function(){
            $('li[class = rowlink]').each(function(){
              $(this).attr('style','cursor: pointer; border-bottom:1px solid #EEE');
            });
            var id = $(this).attr('data-id');
            var text = $(this).attr('data-text');
            $(this).attr('style','cursor: pointer; background-color:#EEE; border-radius:4px;');
            $('#txtIdMenu').val(id);
            $('#txtIdMenutext').val(text);
        });
        $('.linkrow').on('click',function(){
            console.log('click');
            $('li[class = linkrow]').each(function(){
              $(this).attr('style','cursor: pointer; border-bottom:1px solid #EEE');
            });
            var id = $(this).attr('data-id');
            $(this).attr('style','cursor: pointer; background-color:#EEE; border-radius:4px;');
            $('#txtRemove').val(id);
        });
        $('#add').click(function(){
            if ($('#txtIdMenu').val()=='') {
                toastr.warning('seleccione un menu');
            }else{
                var idmenu=$('#txtIdMenu').val();
                var textmenu=$('#txtIdMenutext').val();
                //var addmenu='<tr class="linkrow menu_'+idmenu+'" data-id='+idmenu+'><td><input type="hidden" name="addmenuselect" value="'+idmenu+'"><p>'+textmenu+'</p></td></tr>';

                var addmenu='<li class="linkrow menur_'+idmenu+'" style="cursor: pointer" data-id="'+idmenu+'"><input type="hidden" name="addmenuselect" id="addmenuselect" value="'+idmenu+'"><div class="menu-text">'+textmenu+'</strong></div></li>';
               $('#list').append(addmenu);

               $('#txtIdMenu').val('');
               $('#txtIdMenutext').val('');
               addfuncionremove();
            }            
        });
        $('#remove').click(function(){
            if ($('#txtRemove').val()=='') {
                toastr.warning('seleccione un menu');
            }else{
                var id=$('#txtRemove').val();
                $('.menur_'+id).remove();
                $('#txtRemove').val('');
            }   
        });
        $('#savep').click(function(event) {
            var $valid = $("#formpersonal").valid();
            console.log($valid);
            if($valid) {
                $.ajax({
                    type:'POST',
                    url: 'addpersonal',
                   // data: {id:id,nom:nom,ape:ape,perf:perf,mail:email,tcel:telcel,tcasa:telcasa,call:calle,next:noexte,est:estado,nint:noint,col:colonia,mun:municipio,cp:cop},
                    data: {
                        id: $('#persolnalid').val(),
                        nom: $('#txtNombre').val(),
                        ape: $('#txtApellidos').val(),
                        fnaci:$('#txtFN').val(),
                        sex: $('#cmbGenero').val(),
                        domic:$('#txtDomicilio').val(),
                        ciudad:$('#txtCiudad').val(),
                        estado: $('#cmbEstado option:selected').val(),
                        copo:$('#txtCP').val(),
                        telcasa: $('#txtTL').val(),
                        telcel: $('#txtTC').val(),
                        email: $('#txtEmail').val(),
                        turno: $('#cmbTurno option:selected').val(),
                        fechain: $('#txtFI').val(),
                        fechaba: $('#txtFB').val(),
                        sueldo: $('#txtSueldo').val(),
                        usuario:$('#txtUsuario').val(),
                        pass:$('#txtPass').val()
                    },
                    async: false,
                    statusCode:{
                        404: function(data){
                            toastr.error('Error!', 'No Se encuentra el archivo');
                        },
                        500: function(){
                            toastr.error('Error', '500');
                        }
                    },
                    success:function(data){
                        console.log(data);
                        setInterval(function(){ 
                            location.href='../Personal';
                        }, 3000);
                        //
                        //location.reload();
                        toastr.success('Guardado Correctamente','Hecho!');
                        var DATA  = [];
                        var TABLA   = $("#list > li");
                        TABLA.each(function(){                        
                            item = {};
                            item ["usu"] = data;
                            item ["menu"]   = $(this).find("input[id*='addmenuselect']").val();
                            DATA.push(item);
                        });
                        INFO  = new FormData();
                        aInfo   = JSON.stringify(DATA);
                        INFO.append('data', aInfo);
                        $.ajax({
                            data: INFO,
                            type: 'POST',
                            url : 'addmenuss',
                            processData: false, 
                            contentType: false,
                            success: function(data){
                                console.log(data);
                            }
                        });



                        
                    }
                });
            }
        });
  
  

});
function personaldelete(id){
    $.ajax({
            type:'POST',
            url: 'Personal/deletepersonal',
            data: {id:id},
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
                console.log(data);
                //location.reload();
                toastr.success('eliminado Correctamente','Hecho!' );
                var row = document.getElementById('trper_'+id);
                            row.parentNode.removeChild(row);
                
            }
        });
}
function addfuncionremove(){
    $('.linkrow').on('click',function(){
        console.log('click');
        $('li[class = linkrow]').each(function(){
          $(this).attr('style','cursor: pointer; border-bottom:1px solid #EEE');
        });
        var id = $(this).attr('data-id');
        $(this).attr('style','cursor: pointer; background-color:#EEE; border-radius:4px;');
        $('#txtRemove').val(id);
      });
}