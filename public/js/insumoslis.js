var base_url = $('#base_url').val();
var table;
$(document).ready(function () {
    table = $('#data-tables').DataTable();
    loadtable();
});

function loadtable(){
    table.destroy();
    table = $('#data-tables').DataTable({
        stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Insumos/getlistinsumo",
            type: "post",
        },
        "columns": [
            {"data": "insumosId"},
            {"data": "insumo"},
            {"data": "existencia"},
            {"data": "stockmin"},
            {"data": null,
                render:function(data,type,row){
                    var html='<div class="btn-group mr-1 mb-1">\
                                  <button type="button" class="btn btn-raised btn-outline-warning"><i class="fa fa-cog"></i></button>\
                                  <button type="button" class="btn btn-raised btn-outline-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
                                      <span class="sr-only">Toggle Dropdown</span>\
                                  </button>\
                                  <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(84px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">\
                                      <a class="dropdown-item" href="'+base_url+'Insumos/insumosadd/'+row.insumosId+'">Editar</a>\
                                      <a class="dropdown-item" onclick="insumodelete('+row.insumosId+');"href="#">Eliminar</a>\
                                  </div>\
                              </div>';
                    return html;
                }
            },            
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]],
        "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
            //console.log(aData.stock+'<='+aData.stockmin+'----'+aData.productoid);
                       if (aData.stock<=aData.stockmin){
                           $(nRow).addClass('alertastock');
                       }
                       
                   }
        
    });
}



function insumodelete(id){
  $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar este Insumo? El cambio será irreversible',
        type: 'red',
        typeAnimated: true,
        buttons: 
        {
            // Si le da click al botón de "confirmar"
            confirmar: function () 
            {
                    $.ajax({
                        type:'POST',
                        url: base_url+'Insumos/deleteinsumos',
                        data: {id:id},
                        async: false,
                        statusCode:{
                            404: function(data){
                                toastr.error('Error!', 'No Se encuentra el archivo');
                            },
                            500: function(){
                                toastr.error('Error', '500');
                            }
                        },
                        success:function(data){
                            toastr.success('Hecho!', 'eliminado Correctamente');
                            loadtable();
                            
                        }
                    });
                
  },
            // Si le da click a "cancelar"
            cancelar: function () 
            {
                
            }
        }
    });
}