$(document).ready(function() {	
	$('#btnabrirt').click(function(){
			$.ajax({
				type:'POST',
				url:'Ventas/abrirturno',
				data:{
					cantidad: $('#cantidadt').val(),
					nombre: $('#nombredelturno').val()
				},
				async:false,
				success:function(data){
					toastr.success('Hecho!', 'Turno Abierto');
					$('#modalturno').modal('hide');
					
				}
			});
	});
	$('#cerrarturno').click(function(){
			$.ajax({
				type:'POST',
				url:'Ventas/cerrarturno',
				data:{
					id: $('#idturno').val()
				},
				async:false,
				success:function(data){
					toastr.success('Turno cerrado', 'Hecho!');
					document.getElementById("cantidadt").disabled = false;
					document.getElementById("nombredelturno").disabled = false;
					document.getElementById("btnabrirt").disabled = false;
					document.getElementById("btncerrar").disabled = true;
					document.getElementById("cerrarturno").disabled = true;
					$('#cantidadt').val('');
					$('#nombredelturno').val('');
					$('#horainicial').val('');
					//alert(data);
				}
			});
		
	});
	$('#btncerrar').click(function(){
		params = {};
		params.fecha1 = $('#txtInicio').val();
		params.fecha2 = $('#txtFin').val();
		if(params.fecha1 != '' && params.fecha2 != ''){
			$.ajax({
				type:'POST',
				url:'Corte_caja/corte',
				data:params,
				async:false,
				success:function(data){
					console.log(data);
					var array = $.parseJSON(data);
					$('#tbCorte').html(array.tabla);
					$('#ddescuento').html(array.descuento);
					/*$('#cRealizadas').html(array.cRealizadas);
					$('#cContado').html(array.cContado);
					$('#cCredito').html(array.cCredito);
					$('#cTerminal').html(array.cTerminal);
					$('#cCheque').html(array.cCheque);
					$('#cTransferencia').html(array.cTransferencia);
					$('#cPagos').html(array.cPagos);
					$('#dContado').html('$ '+array.dContado);
					$('#dCredito').html('$ '+array.dCredito);
					$('#dTerminal').html('$ '+array.dTerminal);
					$('#dCheque').html('$ '+array.dCheque);
					$('#dTransferencia').html('$ '+array.dTransferencia);
					$('#dPagos').html('$ '+array.dPagos);*/
					$('#dTotal3').html(new Intl.NumberFormat('es-MX').format(array.dTotal));
                    //$('#dImpuestos').html(''+array.dImpuestos);
                    $('#dSubtotal').html(''+array.dSubtotal);




                    var cantidadt=$('#cantidadt').val();
                    $('#val2').html(cantidadt);
                    var caja=parseFloat(cantidadt)+parseFloat(array.dTotal);
                    $('#stotal2').html(new Intl.NumberFormat('es-MX').format(caja));
				}
			});
		}
		else{
			toastr.error('No existen fechas validas', 'Error');
			
		}
	});
});

function imprimir(){
      window.print();
    }