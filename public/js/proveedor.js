$(document).ready(function () {
        var form_register = $('#formproveedor');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);

        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                txtRazonSocial:{
                  required: true
                },
                
                
            },
            
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });

        //=====================================================================================================
         $('#saveprov').click(function(event) {
            var $valid = $("#formproveedor").valid();
            console.log($valid);
            if($valid) {
                $.ajax({
                    type:'POST',
                    url: 'preoveedoraddd',
                    data: {
                        id:$('#proveedorid').val(),
                        razonSocial:$('#txtRazonSocial').val(),
                        domicilio:$('#txtDomicilio').val(),
                        ciudad:$('#txtCiudad').val(),
                        cp:$('#txtCP').val(),
                        estado:$('#cmbEstado option:selected').val(),
                        contacto:$('#txtContacto').val(),
                        email:$('#txtEmail').val(),
                        rfc:$('#txtRFC').val(),
                        telefonoLocal:$('#txtTL').val(),
                        telefonoCelular:$('#txtTC').val(),
                        fax:$('#txtFax').val(),
                        obser:$('#txtobservacion').val()
                    },
                    async: false,
                    statusCode:{
                        404: function(data){
                            toastr.error('Error!', 'No Se encuentra el archivo');
                        },
                        500: function(){
                            toastr.error('Error', '500');
                        }
                    },
                    success:function(data){
                        toastr.success('Hecho!', 'Guardado Correctamente');
                        location.href='../Proveedores'; 
                        window.location.href = "../Proveedores";
                    }
                });
            }
        });
  
});
function buscarprove(){
    var search=$('#buscarprov').val();
    if (search.length>2) {
        $.ajax({
            type:'POST',
            url: 'Proveedores/buscarprov',
            data: {
                buscar: $('#buscarprov').val()
            },
            async: false,
            statusCode:{
                404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
                500: function(){ toastr.error('Error', '500');}
            },
            success:function(data){
                $('#tbodyresultadosprov2').html(data);
            }
        });
        $("#data-tables").css("display", "none");
        $("#data-tables2").css("display", "");
    }else{
        $("#data-tables2").css("display", "none");
        $("#data-tables").css("display", "");
    }
}