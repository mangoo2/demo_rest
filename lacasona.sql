-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 23-03-2022 a las 16:03:56
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `lacasona`
--

DELIMITER $$
--
-- Procedimientos
--
DROP PROCEDURE IF EXISTS `SP_ADD_PERSONAL`$$
CREATE PROCEDURE `SP_ADD_PERSONAL` (IN `_NOM` VARCHAR(120), IN `_SEX` INT(1))  NO SQL
INSERT INTO personal (nombre,sexo)VALUES (_NOM,_SEX)$$

DROP PROCEDURE IF EXISTS `SP_DEL_PERSONAL`$$
CREATE PROCEDURE `SP_DEL_PERSONAL` (IN `_ID` INT)  NO SQL
UPDATE personal SET estatus=0 where personalId=_ID$$

DROP PROCEDURE IF EXISTS `SP_GET_ALL_ESTADOS`$$
CREATE PROCEDURE `SP_GET_ALL_ESTADOS` ()  NO SQL
SELECT * FROM Estado$$

DROP PROCEDURE IF EXISTS `SP_GET_ALL_PERFILES`$$
CREATE PROCEDURE `SP_GET_ALL_PERFILES` ()  NO SQL
SELECT * FROM Perfiles$$

DROP PROCEDURE IF EXISTS `SP_GET_ALL_PERSONAL`$$
CREATE PROCEDURE `SP_GET_ALL_PERSONAL` ()  NO SQL
SELECT per.personalId,per.nombre,per.apellidos,usu.Usuario 
FROM personal as per 
LEFT JOIN usuarios as usu on usu.personalId=per.personalId
WHERE per.tipo=1 AND per.estatus=1$$

DROP PROCEDURE IF EXISTS `SP_GET_MENUS`$$
CREATE PROCEDURE `SP_GET_MENUS` (IN `PERFIL` INT)  NO SQL
SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, Perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId=PERFIL$$

DROP PROCEDURE IF EXISTS `SP_GET_PERSONAL`$$
CREATE PROCEDURE `SP_GET_PERSONAL` (IN `_ID` INT)  NO SQL
SELECT *  FROM personal where personalId=_ID$$

DROP PROCEDURE IF EXISTS `SP_GET_SESSION`$$
CREATE PROCEDURE `SP_GET_SESSION` (IN `USUA` VARCHAR(10))  NO SQL
SELECT usu.UsuarioID,per.personalId,per.nombre, usu.perfilId, usu.contrasena FROM usuarios as usu,personal as per where per.personalId = usu.personalId and usu.Usuario = USUA$$

DROP PROCEDURE IF EXISTS `SP_GET_USUARIOS`$$
CREATE PROCEDURE `SP_GET_USUARIOS` ()  NO SQL
SELECT usu.UsuarioID,usu.Usuario,per.nombre as perfil FROM usuarios as usu, Perfiles as per WHERE usu.perfilId=per.perfilId$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

DROP TABLE IF EXISTS `categoria`;
CREATE TABLE IF NOT EXISTS `categoria` (
  `categoriaId` int(11) NOT NULL AUTO_INCREMENT,
  `categoria` varchar(200) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`categoriaId`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`categoriaId`, `categoria`, `activo`) VALUES
(1, '', 0),
(2, 'Crepas saladas', 1),
(3, 'Crepas Dulces', 1),
(4, 'Crepas especiales dulces', 1),
(5, 'Crepas Premium', 0),
(6, 'Crepas especiales saladas', 1),
(7, 'Crepas Premium', 1),
(8, 'Bebida Fría', 1),
(9, 'Bebida Caliente', 1),
(10, 'Refresco ', 1),
(11, 'Agua Embotellada ', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

DROP TABLE IF EXISTS `clientes`;
CREATE TABLE IF NOT EXISTS `clientes` (
  `ClientesId` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Calle` varchar(45) DEFAULT NULL,
  `noExterior` varchar(45) DEFAULT NULL,
  `Colonia` varchar(45) DEFAULT NULL,
  `Localidad` varchar(45) DEFAULT NULL,
  `Municipio` varchar(45) DEFAULT NULL,
  `Estado` varchar(45) DEFAULT NULL,
  `Pais` varchar(45) DEFAULT NULL,
  `CodigoPostal` varchar(45) DEFAULT NULL,
  `Correo` varchar(100) NOT NULL,
  `noInterior` varchar(45) NOT NULL,
  `referencia` text NOT NULL,
  `nombrec` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `correoc` varchar(45) NOT NULL,
  `telefonoc` varchar(30) NOT NULL,
  `extencionc` varchar(20) NOT NULL,
  `nextelc` varchar(30) NOT NULL,
  `descripcionc` text NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1 COMMENT '1 activo 0 eliminado',
  PRIMARY KEY (`ClientesId`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`ClientesId`, `Nom`, `Calle`, `noExterior`, `Colonia`, `Localidad`, `Municipio`, `Estado`, `Pais`, `CodigoPostal`, `Correo`, `noInterior`, `referencia`, `nombrec`, `correoc`, `telefonoc`, `extencionc`, `nextelc`, `descripcionc`, `activo`) VALUES
(45, 'PUBLICO EN GENERAL', '', '', '', '', '', '', 'Mexico', '', '', '', '', '', '', '', '', '', '', 1),
(46, 'Mangoo Software', 'Av. Juaréz', '1625', 'La paz', NULL, 'Puebla', 'Puebla', NULL, NULL, 'jesus@mangoo.com.mx', 'PH', 'Torre JV', '', '', '25451565', '', '', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

DROP TABLE IF EXISTS `compras`;
CREATE TABLE IF NOT EXISTS `compras` (
  `id_compra` int(11) NOT NULL AUTO_INCREMENT,
  `id_proveedor` int(11) NOT NULL,
  `monto_total` double NOT NULL,
  `reg` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_compra`),
  KEY `constraint_fk_04` (`id_proveedor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compra_detalle`
--

DROP TABLE IF EXISTS `compra_detalle`;
CREATE TABLE IF NOT EXISTS `compra_detalle` (
  `id_detalle_compra` int(11) NOT NULL AUTO_INCREMENT,
  `id_compra` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `tipo_prod` varchar(1) NOT NULL DEFAULT '1' COMMENT '1=producto, 0=insumo',
  `cantidad` float NOT NULL,
  `precio_compra` double NOT NULL,
  PRIMARY KEY (`id_detalle_compra`),
  KEY `constraint_fk_08` (`id_compra`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

DROP TABLE IF EXISTS `estado`;
CREATE TABLE IF NOT EXISTS `estado` (
  `EstadoId` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(50) DEFAULT NULL,
  `Alias` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`EstadoId`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estado`
--

INSERT INTO `estado` (`EstadoId`, `Nombre`, `Alias`) VALUES
(1, 'AGUASCALIENTES', 'AS'),
(2, 'BAJA CALIFORNIA', 'BC'),
(3, 'BAJA CALIFORNIA SUR', 'BS'),
(4, 'CAMPECHE', 'CC'),
(5, 'COAHUILA', 'CL'),
(6, 'COLIMA', 'CM'),
(7, 'CHIAPAS', 'CS'),
(8, 'CHIHUAHUA', 'CH'),
(9, 'DISTRITO FEDERAL', 'DF'),
(10, 'DURANGO', 'DG'),
(11, 'GUANAJUATO', 'GT'),
(12, 'GUERRERO', 'GR'),
(13, 'HIDALGO', 'HG'),
(14, 'JALISCO', 'JC'),
(15, 'ESTADO DE MEXICO', 'MC'),
(16, 'MICHOACAN', 'MN'),
(17, 'MORELOS', ''),
(18, 'NAYARIT', 'NT'),
(19, 'NUEVO LEON', 'NL'),
(20, 'OAXACA', 'OC'),
(21, 'PUEBLA', 'PL'),
(22, 'QUERETARO', 'QT'),
(23, 'QUINTANA ROO', 'QR'),
(24, 'SAN LUIS POTOSI', 'SP'),
(25, 'SINALOA', 'SL'),
(26, 'SONORA', 'SR'),
(27, 'TABASCO', 'TC'),
(28, 'TAMAULIPAS', 'TS'),
(29, 'TLAXCALA', 'TL'),
(30, 'VERACRUZ', 'VZ'),
(31, 'YUCATAN', 'YN'),
(32, 'ZACATECAS', 'ZS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `insumos`
--

DROP TABLE IF EXISTS `insumos`;
CREATE TABLE IF NOT EXISTS `insumos` (
  `insumosId` int(11) NOT NULL AUTO_INCREMENT,
  `insumo` varchar(300) NOT NULL,
  `existencia` float NOT NULL,
  `stockmin` float NOT NULL,
  `operaciones` varchar(300) DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `reg` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`insumosId`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `insumos`
--

INSERT INTO `insumos` (`insumosId`, `insumo`, `existencia`, `stockmin`, `operaciones`, `activo`, `reg`) VALUES
(1, 'Philadelphia', 0, 2000, NULL, 1, '2021-02-23 05:01:03'),
(2, 'Nutella', 0, 3000, NULL, 1, '2021-02-23 05:03:05'),
(3, 'Nuez', -50, 500, NULL, 1, '2021-02-23 05:03:28'),
(4, 'Galleta Purito ', 0, 50, NULL, 1, '2021-02-23 05:03:50'),
(5, 'Cajeta', -1640, 1000, NULL, 1, '2021-04-18 00:27:53'),
(6, 'Mermelada de zarzamora', 0, 0, NULL, 1, '2021-04-18 00:33:22'),
(7, 'Mermelada de fresa', 0, 0, NULL, 1, '2021-04-18 00:33:52'),
(8, 'Mermelada de manzana', 0, 0, NULL, 1, '2021-04-18 00:34:33'),
(9, 'Lechera', 0, 0, NULL, 1, '2021-04-18 00:36:17'),
(10, 'Chocolate líquido Hershey\'s', 0, 0, NULL, 1, '2021-04-18 00:38:44'),
(11, 'Plátano', -60.5, 0, NULL, 1, '2021-04-18 00:39:05'),
(12, 'Fresa natural', 0, 0, NULL, 1, '2021-04-18 00:39:27'),
(13, 'Galleta Oreo', 0, 0, NULL, 1, '2021-04-18 00:40:30'),
(14, 'Barra Kinder', 0, 0, NULL, 1, '2021-04-18 00:41:50'),
(15, 'Barra Kinder', 0, 0, NULL, 0, '2021-04-18 00:41:51'),
(16, 'M&M\'s originales ', 0, 0, NULL, 1, '2021-04-18 00:43:16'),
(17, 'M&M\'s originales ', 0, 0, NULL, 0, '2021-04-18 00:43:19'),
(18, 'Crema batida', 0, 0, NULL, 1, '2021-04-18 00:43:58'),
(19, 'Queso manchego', 45, 0, NULL, 1, '2021-04-18 00:44:29'),
(20, 'Jamón', 90, 0, NULL, 1, '2021-04-18 00:44:56'),
(21, 'Jamón serrano', 0, 0, NULL, 1, '2021-04-18 00:45:24'),
(22, 'Salami', 0, 0, NULL, 1, '2021-04-18 00:45:54'),
(23, 'Chorizo', 0, 0, NULL, 1, '2021-04-18 00:46:31'),
(24, 'Champiñones', 0, 0, NULL, 1, '2021-04-18 00:47:08'),
(25, 'Conejito Turín', 0, 0, NULL, 1, '2021-04-18 00:47:28'),
(26, 'Kinder Bueno', 0, 0, NULL, 1, '2021-04-18 01:03:24'),
(27, 'Chocolate Ferrero', 0, 0, NULL, 1, '2021-04-18 01:03:44'),
(28, 'Mazapán', 0, 0, NULL, 1, '2021-04-18 01:04:20'),
(29, 'Chocolate Raffaello', 0, 0, NULL, 1, '2021-04-18 01:05:06'),
(30, 'Leche', 4050, 0, NULL, 1, '2021-04-18 01:07:05'),
(31, 'Harina de trigo', 891, 0, NULL, 1, '2021-04-18 01:07:30'),
(32, 'Harina integral', 264, 0, NULL, 1, '2021-04-18 01:07:46'),
(33, 'Azúcar', 1665, 0, NULL, 1, '2021-04-18 01:08:26'),
(34, 'Sal', 0, 0, NULL, 1, '2021-04-18 01:08:42'),
(35, 'Vainilla', 270, 0, NULL, 1, '2021-04-18 01:09:05'),
(36, 'Huevo', 54.3, 0, NULL, 1, '2021-04-18 01:09:21'),
(37, 'Mantequilla', 81, 0, NULL, 1, '2021-04-18 01:09:41'),
(38, 'Crema de maní', 0, 0, NULL, 1, '2021-04-18 01:29:24'),
(39, 'Hielo', 0, 0, NULL, 1, '2021-04-18 01:34:57'),
(40, 'Café', -125, 0, NULL, 1, '2021-04-18 01:35:14'),
(41, 'Polvo chocolate blanco', 0, 0, NULL, 1, '2021-04-18 01:36:59'),
(42, 'Jalapeño', 0, 0, NULL, 1, '2021-04-18 01:37:34'),
(43, 'Polvo neutro', 0, 0, NULL, 1, '2021-04-18 01:38:50'),
(44, 'Jarabe mango', 0, 0, NULL, 1, '2021-04-18 01:41:52'),
(45, 'Jarabe de fresa', 0, 0, NULL, 1, '2021-04-18 01:42:13'),
(46, 'Vaso cristal 16 oz', 0, 0, NULL, 1, '2021-04-18 01:44:59'),
(47, 'Tapa domo', 0, 0, NULL, 1, '2021-04-18 01:45:20'),
(48, 'Cono', 23, 0, NULL, 1, '2021-04-18 01:45:41'),
(49, 'Vaso encerado 12 oz', 35, 0, NULL, 1, '2021-04-18 01:48:41'),
(50, 'Tapa para café ', 35, 0, NULL, 1, '2021-04-18 01:49:27'),
(51, 'Popote', 0, 0, NULL, 1, '2021-04-18 01:54:26'),
(52, 'Frijoles', 0, 0, NULL, 1, '2021-04-18 02:08:36'),
(53, 'Piña', 0, 0, NULL, 1, '2021-04-18 02:08:54'),
(54, 'Queso americano ', 0, 0, NULL, 1, '2021-04-18 02:18:04'),
(55, 'Agua', 0, 0, NULL, 1, '2021-04-18 02:18:16'),
(56, 'Vaso encerado 3 oz', 0, 0, NULL, 1, '2021-04-18 02:19:38'),
(57, 'Salsa pizza', 0, 0, NULL, 1, '2021-04-18 02:32:43'),
(58, 'Shot de café', 0, 0, NULL, 1, '2021-04-18 03:10:19'),
(59, 'Chamoy', 0, 0, NULL, 1, '2021-04-18 03:23:02'),
(60, 'Miguelito', 0, 0, NULL, 1, '2021-04-18 03:23:34'),
(61, 'Crepa de combo', 0, 0, NULL, 0, '2021-04-18 03:32:07'),
(62, 'coco rayado ', 0, 0, NULL, 1, '2021-07-01 20:11:27'),
(63, 'Chocolate Blanco Turin', 0, 0, NULL, 1, '2021-07-01 20:11:47'),
(64, 'Chocolate de leche Turin ', 0, 0, NULL, 1, '2021-07-01 20:12:05'),
(65, 'Chocolate Kisses', 0, 0, NULL, 1, '2021-07-01 20:12:23'),
(66, 'Chocolate café turin', 0, 0, NULL, 1, '2021-07-01 20:23:19'),
(67, 'Crema Batida Rich', 0, 0, NULL, 1, '2021-07-01 23:39:04'),
(68, 'Capsulas CO2', 0, 0, NULL, 1, '2021-07-01 23:39:25'),
(69, 'Vainilla Francesa', 0, 0, NULL, 1, '2021-07-01 23:40:03'),
(70, 'Canela ', 0, 0, NULL, 1, '2022-01-11 18:30:35'),
(71, 'Capsula de Gas ', 0, 0, NULL, 0, '2022-01-18 20:57:56'),
(72, 'Aluminio ', 0, 0, NULL, 1, '2022-01-18 21:07:03'),
(73, 'Emplaye ', 0, 0, NULL, 1, '2022-01-18 21:07:28'),
(74, 'Charola de Unicel', 0, 0, NULL, 1, '2022-01-18 21:08:02');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

DROP TABLE IF EXISTS `menu`;
CREATE TABLE IF NOT EXISTS `menu` (
  `MenuId` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) DEFAULT NULL,
  `Icon` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`MenuId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`MenuId`, `Nombre`, `Icon`) VALUES
(1, 'Catálogos', 'fa fa-book'),
(2, 'Operaciones', 'fa fa-folder-open'),
(3, 'Configuración\n', 'fa fa fa-cogs');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu_sub`
--

DROP TABLE IF EXISTS `menu_sub`;
CREATE TABLE IF NOT EXISTS `menu_sub` (
  `MenusubId` int(11) NOT NULL AUTO_INCREMENT,
  `MenuId` int(11) NOT NULL,
  `Nombre` varchar(50) DEFAULT NULL,
  `Pagina` varchar(120) DEFAULT NULL,
  `Icon` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`MenusubId`),
  KEY `fk_menu_sub_menu_idx` (`MenuId`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `menu_sub`
--

INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`) VALUES
(1, 1, 'Productos', 'Productos', 'fa fa-barcode'),
(2, 3, 'Categoria', 'Categoria', 'fa fa-cogs'),
(3, 1, 'Personal', 'Personal', 'fa fa-group'),
(4, 2, 'Ventas', 'Ventas', 'fa fa-shopping-cart'),
(5, 2, 'Compras', 'Compras', 'fa fa-cogs'),
(6, 1, 'Clientes', 'Clientes', 'fa fa-user'),
(7, 1, 'Proveedores', 'Proveedores', 'fa fa-truck'),
(8, 2, 'Corte de caja', 'Corte_caja', 'fa fa-cogs'),
(9, 2, 'Lista de ventas', 'ListaVentas', 'fa fa-cogs'),
(10, 2, 'Turno', 'Turno', 'fa fa-cogs'),
(11, 2, 'Lista de turnos', 'ListaTurnos', 'fa fa-cogs'),
(12, 2, 'Lista de compras', 'Listacompras', 'fa fa-shopping-cart'),
(13, 3, 'Config. de ticket', 'Config_ticket', 'fa fa-cogs'),
(14, 1, 'Insumos', 'Insumos', 'fa fa-user');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notas`
--

DROP TABLE IF EXISTS `notas`;
CREATE TABLE IF NOT EXISTS `notas` (
  `id_nota` int(1) NOT NULL AUTO_INCREMENT,
  `mensaje` text NOT NULL,
  `usuario` varchar(120) NOT NULL,
  `reg` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_nota`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `notas`
--

INSERT INTO `notas` (`id_nota`, `mensaje`, `usuario`, `reg`) VALUES
(1, '<p>notass</p>\n', 'Administrador', '2018-05-10 19:01:36');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfiles`
--

DROP TABLE IF EXISTS `perfiles`;
CREATE TABLE IF NOT EXISTS `perfiles` (
  `perfilId` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`perfilId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `perfiles`
--

INSERT INTO `perfiles` (`perfilId`, `nombre`) VALUES
(1, 'Administrador'),
(2, 'Personal'),
(3, 'Residentes');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfiles_detalles`
--

DROP TABLE IF EXISTS `perfiles_detalles`;
CREATE TABLE IF NOT EXISTS `perfiles_detalles` (
  `Perfil_detalleId` int(11) NOT NULL AUTO_INCREMENT,
  `perfilId` int(11) NOT NULL,
  `MenusubId` int(11) NOT NULL,
  PRIMARY KEY (`Perfil_detalleId`),
  KEY `fk_Perfiles_detalles_Perfiles1_idx` (`perfilId`),
  KEY `fk_Perfiles_detalles_menu_sub1_idx` (`MenusubId`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `perfiles_detalles`
--

INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 2, 4),
(7, 2, 5),
(8, 1, 14);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal`
--

DROP TABLE IF EXISTS `personal`;
CREATE TABLE IF NOT EXISTS `personal` (
  `personalId` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(300) NOT NULL,
  `apellidos` varchar(300) NOT NULL,
  `fechanacimiento` date NOT NULL,
  `sexo` int(1) NOT NULL,
  `domicilio` varchar(500) NOT NULL,
  `ciudad` varchar(120) NOT NULL,
  `estado` int(11) NOT NULL,
  `codigopostal` int(5) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `celular` varchar(15) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `turno` int(1) NOT NULL,
  `fechaingreso` date NOT NULL,
  `fechabaja` date NOT NULL,
  `sueldo` decimal(10,2) NOT NULL,
  `tipo` int(1) NOT NULL DEFAULT 1 COMMENT '0 administrador 1 normal',
  `estatus` int(1) NOT NULL DEFAULT 1 COMMENT '1 visible 0 eliminado',
  PRIMARY KEY (`personalId`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `personal`
--

INSERT INTO `personal` (`personalId`, `nombre`, `apellidos`, `fechanacimiento`, `sexo`, `domicilio`, `ciudad`, `estado`, `codigopostal`, `telefono`, `celular`, `correo`, `turno`, `fechaingreso`, `fechabaja`, `sueldo`, `tipo`, `estatus`) VALUES
(1, 'Administrador', '', '0000-00-00', 0, '', '', 0, 0, '', '', '', 0, '0000-00-00', '0000-00-00', '0.00', 0, 1),
(2, 'Kevin Brian ', 'Cortés Aguirre', '1989-10-20', 1, 'Nimes 21 ', 'Puebla', 21, 72700, '2226614574', '2226614574', 'kevinbrianca@gmail.com', 1, '2010-01-01', '0000-00-00', '0.00', 1, 0),
(3, 'Nelly ', 'Cuñada', '0000-00-00', 1, '', '', 1, 0, '', '', '', 1, '0000-00-00', '0000-00-00', '0.00', 1, 0),
(4, 'Nelly ', 'Cuñada', '0000-00-00', 1, '', '', 1, 0, '', '', '', 1, '0000-00-00', '0000-00-00', '0.00', 1, 0),
(5, 'Danny ', 'Cortes', '0000-00-00', 1, '', '', 1, 0, '', '', '', 1, '0000-00-00', '0000-00-00', '0.00', 1, 0),
(6, 'Prima ', 'Nelly 1', '0000-00-00', 1, '', '', 1, 0, '', '', '', 1, '0000-00-00', '0000-00-00', '0.00', 1, 0),
(7, 'Nelly ', 'Prima2 ', '0000-00-00', 1, '', '', 1, 0, '', '', '', 1, '0000-00-00', '0000-00-00', '0.00', 1, 0),
(8, 'Yoshira ', 'Jiménez Ramírez ', '2000-12-21', 2, '', 'Puebla', 21, 72120, '', '2224976030', '', 3, '0000-00-00', '0000-00-00', '0.00', 1, 1),
(9, 'Michelle Ceyru', 'Arroyo Romero ', '2002-10-20', 2, '', 'Puebla', 21, 72120, '', '2221524458', '', 1, '2020-12-01', '0000-00-00', '0.00', 1, 1),
(10, 'Joaquin Alfonso ', 'Jiménez Ramírez ', '2004-12-18', 1, '', 'Puebla', 21, 72120, '', '2227784302', '', 1, '2021-03-12', '0000-00-00', '0.00', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal_menu`
--

DROP TABLE IF EXISTS `personal_menu`;
CREATE TABLE IF NOT EXISTS `personal_menu` (
  `personalmenuId` int(11) NOT NULL AUTO_INCREMENT,
  `personalId` int(11) NOT NULL,
  `MenuId` int(11) NOT NULL,
  PRIMARY KEY (`personalmenuId`),
  KEY `personal_fkpersona` (`personalId`),
  KEY `personal_fkmenu` (`MenuId`)
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `personal_menu`
--

INSERT INTO `personal_menu` (`personalmenuId`, `personalId`, `MenuId`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(8, 1, 8),
(9, 1, 9),
(10, 1, 10),
(11, 1, 11),
(12, 1, 12),
(13, 1, 13),
(18, 1, 14),
(33, 2, 1),
(34, 2, 3),
(35, 2, 6),
(36, 2, 7),
(37, 2, 14),
(38, 2, 4),
(39, 2, 5),
(40, 2, 8),
(41, 2, 9),
(42, 2, 10),
(43, 2, 11),
(44, 2, 12),
(45, 2, 2),
(46, 2, 13),
(47, 3, 1),
(48, 3, 7),
(49, 3, 14),
(50, 3, 4),
(51, 3, 5),
(52, 3, 13),
(53, 4, 1),
(54, 4, 7),
(55, 4, 14),
(56, 4, 4),
(57, 4, 5),
(58, 4, 13),
(59, 5, 1),
(60, 5, 7),
(61, 5, 14),
(62, 5, 4),
(63, 5, 5),
(64, 5, 12),
(65, 6, 1),
(66, 6, 7),
(67, 6, 14),
(68, 6, 5),
(69, 6, 13),
(70, 7, 1),
(71, 7, 7),
(72, 7, 14),
(73, 7, 5),
(74, 7, 13),
(89, 10, 1),
(90, 10, 3),
(91, 10, 4),
(92, 10, 6),
(93, 10, 10),
(94, 10, 11),
(95, 10, 13),
(96, 10, 14),
(97, 9, 1),
(98, 9, 3),
(99, 9, 4),
(100, 9, 5),
(101, 9, 6),
(102, 9, 9),
(103, 9, 10),
(104, 9, 11),
(105, 9, 13),
(106, 9, 14),
(107, 8, 1),
(108, 8, 3),
(109, 8, 6),
(110, 8, 7),
(111, 8, 14),
(112, 8, 4),
(113, 8, 5),
(114, 8, 8),
(115, 8, 9),
(116, 8, 10),
(117, 8, 11),
(118, 8, 12),
(119, 8, 2),
(120, 8, 13);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

DROP TABLE IF EXISTS `productos`;
CREATE TABLE IF NOT EXISTS `productos` (
  `productoid` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(20) NOT NULL,
  `nombre` varchar(120) NOT NULL,
  `stockok` tinyint(1) NOT NULL DEFAULT 0,
  `stock` float NOT NULL DEFAULT 0,
  `stockmin` float NOT NULL DEFAULT 0,
  `preciocompra` float NOT NULL DEFAULT 0,
  `preciopos` float NOT NULL DEFAULT 0,
  `preciouber` float NOT NULL DEFAULT 0,
  `preciorappi` float NOT NULL DEFAULT 0,
  `preciodid` float NOT NULL DEFAULT 0,
  `preciocomp_uber` float NOT NULL,
  `preciocomp_rappi` float NOT NULL,
  `preciocomp_didi` float NOT NULL,
  `img` varchar(120) DEFAULT NULL,
  `tipo` varchar(1) NOT NULL DEFAULT '0' COMMENT '0=normal, 1=combo',
  `cant_tot_prods` varchar(2) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1 COMMENT '1 actual 0 eliminado',
  `reg` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`productoid`)
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`productoid`, `codigo`, `nombre`, `stockok`, `stock`, `stockmin`, `preciocompra`, `preciopos`, `preciouber`, `preciorappi`, `preciodid`, `preciocomp_uber`, `preciocomp_rappi`, `preciocomp_didi`, `img`, `tipo`, `cant_tot_prods`, `activo`, `reg`) VALUES
(1, 'Mr. Ferrero ', 'Crepa de Nutella Nuez y Galleta.', 0, 0, 0, 18, 45, 55, 55, 55, 34, 34, 34, NULL, '0', '', 1, '2021-02-23 04:52:33'),
(2, 'Mr. Cajeta', 'Crepa de cajeta, nuez y plátano.', 0, 0, 0, 0, 45, 55, 55, 55, 0, 0, 0, NULL, '0', '', 1, '2021-03-01 02:58:51'),
(3, 'Mr. Snickers', 'Crepa de Nutella, crema de maní y nuez. ', 0, 0, 0, 0, 45, 55, 55, 55, 0, 0, 0, NULL, '0', '', 1, '2021-04-17 23:48:30'),
(4, 'Mr. Fresa', 'Crepa de nutella, fresa y queso philadelphia.', 0, 0, 0, 0, 45, 55, 55, 55, 0, 0, 0, NULL, '0', '', 1, '2021-04-17 23:55:49'),
(5, 'Mr. Zarza', 'Crepa de queso Philadelphia, zarzamora y lechera.', 0, 0, 0, 0, 45, 55, 55, 55, 0, 0, 0, NULL, '0', '', 1, '2021-04-17 23:57:49'),
(6, 'Mr. Canela ', 'Crepa de manzana, canela y queso philadelphia.', 0, 0, 0, 0, 45, 55, 55, 55, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 00:00:14'),
(7, 'Mr. Oreo ', 'Crepa de Philadelphia, oreo y lechera.', 0, 0, 0, 0, 45, 55, 55, 55, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 00:03:47'),
(8, 'Philadelphia con Zar', 'Crepa de queso Philadelphia con mermelada de zarzamora.', 0, 0, 0, 0, 35, 45, 45, 45, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 00:06:02'),
(9, 'Mr. Frutisima ', 'Crepa de fresa natural, plátano, Nutella y queso Philadelphia.', 0, -61, 0, 0, 50, 60, 60, 60, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 00:08:24'),
(10, 'Mr. Cheese', 'Crepa de queso manchego, queso philadelphia y queso amarillo.', 0, 0, 0, 0, 40, 50, 50, 50, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 00:10:08'),
(11, 'Philadelphia con Nut', 'Crepa de queso Philadelphia con Nutella.', 0, 0, 0, 0, 40, 50, 50, 50, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 00:10:16'),
(12, 'Mr. Crepizza ', 'Crepa de queso mozzarella, jamón serrano, salami y salsa de pizza.', 0, 0, 0, 0, 50, 60, 60, 60, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 00:12:12'),
(13, 'Philadelphia con Fre', 'Crepa de Queso Philadelphia con Fresa natural.', 0, 0, 0, 0, 40, 50, 50, 50, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 00:12:30'),
(14, 'Mr. Hawai', 'Crepa de queso manchego, jamón y piña.', 0, 0, 0, 0, 45, 55, 55, 55, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 00:13:31'),
(15, 'Nutella con Plátano', 'Crepa de Nutella con plátano.', 0, 0, 0, 0, 40, 50, 50, 50, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 00:14:11'),
(16, 'Mr. México', 'Crepa de queso manchego, chorizo, frijoles y jalapeños.', 0, 0, 0, 0, 45, 55, 55, 55, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 00:14:42'),
(17, 'Cajeta con Queso Phi', 'Crepa de cajeta con queso Philadelphia', 0, 0, 0, 0, 35, 45, 45, 45, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 00:15:05'),
(18, 'Nutella con Fresa na', 'Crepa de Nutella con fresa natural.', 0, 0, 0, 0, 40, 50, 50, 50, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 00:16:16'),
(19, 'Mr. Especial', 'Crepa de queso manchego, queso Philadelphia y jamón.', 0, 0, 0, 0, 40, 50, 50, 50, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 00:16:28'),
(20, 'Nutella con Nuez', 'Crepa de Nutella con nuez.', 0, 0, 0, 0, 40, 50, 50, 50, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 00:17:46'),
(21, 'Mr. Choriqueso', 'Crepa de queso manchego, chorizo y jalapeños.', 0, 0, 0, 0, 40, 50, 50, 50, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 00:18:15'),
(22, 'Cajeta con Nuez', 'Crepa de cajeta con nuez.', 0, 0, 0, 0, 40, 50, 50, 50, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 00:19:10'),
(23, 'Mr. Hawai Pizza', 'Crepa de jamón, manchego y salsa de pizza.', 0, 0, 0, 0, 50, 60, 60, 60, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 00:19:41'),
(24, 'Cajeta con Plátano', 'Crepa de cajeta con plátano', 0, 0, 0, 0, 35, 45, 45, 45, 0, 0, 0, NULL, '0', '1', 1, '2021-04-18 00:19:55'),
(25, 'Mr. Carnes Frías ', 'Crepa de jamón serrano, salami, jamón y queso manchego', 0, 0, 0, 0, 55, 65, 65, 65, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 00:21:57'),
(26, 'Jamón con Queso Manc', 'Crepa de jamón con queso manchego.', 0, 0, 0, 0, 35, 45, 45, 45, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 00:22:12'),
(27, 'Salami con Queso Man', 'Crepa de salami con queso manchego.', 0, 0, 0, 0, 35, 45, 45, 45, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 00:24:10'),
(28, 'Mr. Combo 1', 'Un Café Capuchino y Crepa de dos ingredientes ', 0, 0, 0, 0, 50, 60, 60, 60, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 00:24:33'),
(29, 'Mr Combo 2 ', 'Crepa de 2 ingredientes y 1 frape especial ', 0, 0, 0, 0, 65, 75, 75, 75, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 00:25:33'),
(30, 'Jamón Serrano con Qu', 'Crepa de jamón serrano con queso manchego.', 0, 0, 0, 0, 45, 55, 55, 55, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 00:26:04'),
(31, 'Mr Combo 3 ', '2 crepa de 2 ingredientes y 2 café capuchino ', 0, 0, 0, 0, 100, 120, 120, 120, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 00:26:43'),
(32, 'Jamón con Queso Phil', 'Crepa de jamón con queso Philadelphia.', 0, 0, 0, 0, 35, 45, 45, 45, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 00:27:33'),
(33, 'Mr Combo 4 ', '2 crepas especiales y 2 frappés especiales ', 0, 0, 0, 0, 150, 180, 180, 180, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 00:28:04'),
(34, 'Salami con Queso Phi', 'Crepa de salami con queso philadelphia', 0, 0, 0, 0, 35, 45, 45, 45, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 00:28:50'),
(35, 'Jamón Serrano con Qu', 'Crepa de jamón serrano con queso Philadelphia.', 0, 0, 0, 0, 45, 55, 55, 55, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 00:31:15'),
(36, 'Frappé Oreo', 'Frappe de Galleta Oreo', 0, 0, 0, 0, 35, 45, 45, 45, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 00:32:17'),
(37, 'Frappé de M&M', 'Chocolate de  M&M', 0, 0, 0, 0, 40, 50, 50, 50, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 00:33:32'),
(38, 'Frappé Zarzamora', 'Zarzamora ', 0, 0, 0, 0, 35, 45, 45, 45, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 00:34:34'),
(39, 'Frappé Mazapán', 'Mazapan', 0, 0, 0, 0, 35, 45, 45, 45, 0, 0, 0, NULL, '0', '', 0, '2021-04-18 00:37:03'),
(40, 'Frappé de Chocolate ', 'Chocolate Blanco', 0, 0, 0, 0, 30, 45, 45, 45, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 01:08:27'),
(41, 'Frappe Mazapan ', 'Mazapan', 0, 0, 0, 0, 35, 45, 45, 45, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 01:10:23'),
(42, 'Café expreso', 'Carga directa de café', 0, 0, 0, 0, 15, 20, 20, 20, 0, 0, 0, NULL, '0', '1', 1, '2021-04-18 01:13:39'),
(43, 'Frappe Moka ', 'Chocolate Heshey', 0, 0, 0, 0, 35, 45, 45, 45, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 01:15:07'),
(44, 'Café Moka', 'Café. Leche y Chocolate Hershey ', 0, 0, 0, 0, 25, 30, 30, 30, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 01:15:11'),
(45, 'Frappé Capuchino', 'leche, polvo capuccino, café, ', 0, 0, 0, 0, 30, 40, 40, 40, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 01:17:56'),
(46, 'Frappé Kinder Barra', 'Chocolate Kinder ', 0, 0, 0, 0, 60, 70, 70, 70, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 01:18:40'),
(47, 'Frappe Chocolate Tur', 'Chocolate Turin', 0, 0, 0, 0, 60, 70, 70, 70, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 01:19:23'),
(48, 'Café Ferrerolatte', 'Café, Leche, Ferrerolatte', 0, 0, 0, 0, 45, 55, 55, 55, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 01:20:00'),
(49, 'Frappé Raffaello', 'Chocolate Raffaello', 0, 0, 0, 0, 60, 70, 70, 70, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 01:20:42'),
(50, 'Café Kinderlatte', 'Café, Leche, Kinderlatte', 0, 0, 0, 0, 40, 50, 50, 50, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 01:21:03'),
(51, 'Frappé Ferrero', 'Chocolate Ferrero', 0, 0, 0, 0, 60, 70, 70, 70, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 01:21:34'),
(52, 'Café Nutellate ', 'Café, Leche, Nutella', 0, 0, 0, 0, 30, 40, 40, 40, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 01:22:16'),
(53, 'Café Cajelatte', 'Café, leche, Cajeta', 0, 0, 0, 0, 30, 40, 40, 40, 0, 0, 0, NULL, '0', '1', 1, '2021-04-18 01:23:21'),
(54, 'Café Capuccino', 'Café, leche', 0, 0, 0, 0, 20, 30, 30, 30, 0, 0, 0, NULL, '0', '1', 1, '2021-04-18 01:23:56'),
(55, 'Smoothie de Mango', 'Mango', 0, 0, 0, 0, 30, 40, 40, 40, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 01:27:09'),
(56, 'Smoothie de Fresa', 'Fresa', 0, 0, 0, 0, 30, 40, 40, 40, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 01:28:35'),
(57, 'Café Americano', 'Café con agua caliente', 0, 0, 0, 0, 18, 20, 20, 20, 0, 0, 0, NULL, '0', '1', 1, '2021-04-18 01:29:11'),
(58, 'Agua', 'Agua natural', 0, -1, 0, 0, 10, 15, 15, 15, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 01:29:56'),
(59, 'Chamoyada Smoothie ', 'Chamoy, Miguelito en polvo y sabor a elegir (Fresa o Mango)', 0, 0, 0, 0, 40, 50, 50, 50, 0, 0, 0, NULL, '0', '', 0, '2021-04-18 01:30:27'),
(60, 'Coca-Cola', 'Bebida gaseosa', 0, 0, 0, 0, 20, 30, 30, 30, 0, 0, 0, NULL, '0', '', 0, '2021-04-18 01:31:31'),
(61, 'Coca cola ', 'Coca cola ', 1, -8, 5, 0, 20, 25, 25, 25, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 01:31:58'),
(62, 'Boing', 'Jugo Boing (Sabor a elegir)', 0, 0, 0, 0, 15, 20, 20, 20, 0, 0, 0, NULL, '0', '', 1, '2021-04-18 01:32:59'),
(63, 'Mr. Kinder', 'Crepa, philadelphia, 3 barras de Kinder y Fresa natural', 0, 0, 0, 0, 50, 60, 60, 60, 0, 0, 0, NULL, '0', '', 1, '2021-07-01 19:39:03'),
(64, 'Mr. Ferrero Original', '3 Ferreros originales, Nutella, Nuez', 0, 0, 0, 0, 70, 85, 85, 85, 0, 0, 0, NULL, '0', '', 1, '2021-07-01 19:55:11'),
(65, 'Mr. Rafaelo', '3 Raffaellos, coco rayado, chocolate blanco Turin,  Queso Philadelphia', 0, 0, 0, 0, 75, 85, 85, 85, 0, 0, 0, NULL, '0', '', 1, '2021-07-01 20:00:30'),
(66, 'Mr. Kinder Bueno ', '2 barras de kinder bueno, fresa natural, nutella ', 0, 0, 0, 0, 70, 85, 85, 85, 0, 0, 0, NULL, '0', '', 1, '2021-07-01 20:04:43'),
(67, 'Mr. Conejito Turin', '2 conejos turin, nuez, 1 chocolate turin derretido', 0, 0, 0, 0, 70, 85, 85, 85, 0, 0, 0, NULL, '0', '', 1, '2021-07-01 20:08:16'),
(68, 'Mr. Kisses', '8 kisses de chocolate, Fresa natural, queso philadelphia', 0, 0, 0, 0, 70, 85, 85, 85, 0, 0, 0, NULL, '0', '', 1, '2021-07-01 20:17:13'),
(69, 'Frappé Ferrero ', '3 ferreros, leche, chocolate, crema batida', 0, 0, 0, 0, 70, 80, 80, 80, 0, 0, 0, NULL, '0', '', 1, '2021-07-01 21:39:02'),
(70, 'Frappé Raffaello', '3piezas de chocolate Raffaello, leche, chocolate, crema batida ', 0, 0, 0, 0, 70, 80, 80, 80, 0, 0, 0, NULL, '0', '', 1, '2021-07-01 21:42:21'),
(71, 'Frappé Kinder', '3 piezas de kinder, leche, crema batida, chocolate', 0, 0, 0, 0, 70, 80, 80, 80, 0, 0, 0, NULL, '0', '', 1, '2021-07-01 21:46:03'),
(72, 'Frappé Turin ', 'chocolate turin , chocolate blanco, leche, crema batida', 0, 0, 0, 0, 70, 80, 80, 80, 0, 0, 0, NULL, '0', '', 1, '2021-07-01 21:49:54'),
(73, 'Frappé Nutella ', 'Nutella, Leche, Crema batida, Chocolate blanco', 0, 0, 0, 0, 40, 50, 50, 50, 0, 0, 0, NULL, '0', '', 1, '2021-07-01 21:53:56'),
(74, 'Frappé Cajeta Corona', 'Cajeta, Leche, Chocolate, Crema batida', 0, 0, 0, 0, 40, 50, 50, 50, 0, 0, 0, NULL, '0', '', 1, '2021-07-01 21:55:59'),
(75, 'Café Turinlatte', 'Chocolate Turin, leche, crema batida,  Café', 0, 0, 0, 0, 45, 55, 55, 55, 0, 0, 0, NULL, '0', '1', 1, '2021-07-01 22:15:45'),
(76, 'extra nutella', 'nutella', 0, 0, 0, 9, 10, 20, 20, 20, 12, 12, 12, NULL, '0', '', 1, '2021-07-01 22:40:36'),
(77, 'Extra Cajeta ', 'Cajeta', 0, 0, 0, 2.5, 10, 15, 15, 15, 3, 3, 3, NULL, '0', '', 1, '2021-07-01 22:41:16'),
(78, 'Extra Mermelada de Z', 'Mermelada de Zarzamora', 0, 0, 0, 1, 5, 10, 10, 10, 1.35, 1.35, 1.35, NULL, '0', '', 1, '2021-07-01 22:42:24'),
(79, 'Extra Mermelada de F', 'Mermelada de Freza ', 0, 0, 0, 1.5, 5, 10, 10, 10, 2, 2, 2, NULL, '0', '', 1, '2021-07-01 22:43:18'),
(80, 'Extra Mermelada Manz', 'Mermelada de Manzana ', 0, 0, 0, 2, 5, 10, 10, 10, 3, 3, 3, NULL, '0', '', 1, '2021-07-01 22:44:33'),
(81, 'Extra Lechera', 'Lechera', 0, 0, 0, 1.5, 5, 10, 10, 10, 2, 2, 2, NULL, '0', '', 1, '2021-07-01 22:45:10'),
(82, 'Extra Chocolate Liqu', 'Chocolate Liquido Hershey', 0, 0, 0, 3, 5, 10, 10, 10, 4, 4, 4, NULL, '0', '', 1, '2021-07-01 22:46:15'),
(83, 'Extra Platano Natura', 'Platano', 0, 0, 0, 1.5, 5, 10, 10, 10, 2, 2, 2, NULL, '0', '', 1, '2021-07-01 22:47:42'),
(84, 'Extra Fresa Natural', 'Fresa', 0, 0, 0, 3, 10, 15, 15, 15, 4, 4, 4, NULL, '0', '', 1, '2021-07-01 22:48:24'),
(85, 'Extra Galleta Oreo', '2 Galleta Oreo', 0, 0, 0, 1.5, 5, 10, 10, 10, 2, 2, 2, NULL, '0', '', 1, '2021-07-01 22:49:11'),
(86, 'Extra Kinder', '2 barras de Kinder ', 0, 0, 0, 7, 10, 15, 15, 15, 9.5, 9.5, 9.5, NULL, '0', '', 1, '2021-07-01 22:50:20'),
(87, 'Extra M&M Originales', 'M&M', 0, 0, 0, 10, 10, 15, 15, 15, 13.5, 13.5, 13.5, NULL, '0', '', 1, '2021-07-01 22:53:42'),
(88, 'Extra Nuez', 'Nuez', 0, 0, 0, 2.5, 10, 15, 15, 15, 3, 3, 3, NULL, '0', '', 1, '2021-07-01 22:55:34'),
(89, 'Extra Crema Batida', 'Crema Batida', 0, 0, 0, 2.5, 10, 15, 15, 15, 3, 3, 3, NULL, '0', '', 1, '2021-07-01 22:57:19'),
(90, 'Extra Queso Manchego', 'Queso Manchego', 0, 0, 0, 5.5, 5, 10, 10, 10, 7, 7, 7, NULL, '0', '', 1, '2021-07-01 22:58:30'),
(91, 'Extra Jamón ', 'Jamón ', 0, 0, 0, 5, 5, 10, 10, 10, 7, 7, 7, NULL, '0', '', 1, '2021-07-01 23:00:15'),
(92, 'Extra Serrano', 'Serrano', 0, 0, 0, 8, 15, 20, 20, 20, 11, 11, 11, NULL, '0', '', 1, '2021-07-01 23:01:42'),
(93, 'Extra Queso Philadel', 'Queso Philadelphia ', 0, 0, 0, 4.5, 5, 10, 10, 10, 6, 6, 6, NULL, '0', '', 1, '2021-07-01 23:03:50'),
(94, 'Extra Salami', 'Salami', 0, 0, 0, 2, 5, 10, 10, 10, 3, 3, 3, NULL, '0', '', 1, '2021-07-01 23:04:43'),
(95, 'Extra Chorizo', 'Chorizo', 0, 0, 0, 4, 5, 10, 10, 10, 5, 5, 5, NULL, '0', '', 1, '2021-07-01 23:07:00'),
(96, 'Extra Champiñon ', 'Champiñon ', 0, 0, 0, 8.5, 10, 15, 15, 15, 11.5, 11.5, 11.5, NULL, '0', '', 1, '2021-07-01 23:08:28'),
(97, 'Extra Conejito Turin', '1 conejito turin', 0, 0, 0, 8, 15, 25, 25, 25, 11, 11, 11, NULL, '0', '', 1, '2021-07-01 23:11:12'),
(98, 'Extra Mazapán ', '1 pza Mazapán', 0, 0, 0, 4, 10, 15, 15, 15, 5.5, 5.5, 5.5, NULL, '0', '', 1, '2021-07-01 23:12:40'),
(99, 'Extra Ferrero', '1 pza. Ferrero', 0, 0, 0, 6, 10, 15, 15, 15, 8, 8, 8, NULL, '0', '', 1, '2021-07-01 23:13:22'),
(100, 'Extra Raffaello ', '1 pza. Raffaello ', 0, 0, 0, 5.4, 10, 15, 14, 15, 7.3, 7.3, 7.3, NULL, '0', '', 1, '2021-07-01 23:14:27'),
(101, 'Extra Kinder  Bueno ', '2 barras kinder bueno', 0, 0, 0, 15.5, 30, 35, 35, 35, 22.75, 22.75, 22.75, NULL, '0', '', 1, '2021-07-01 23:15:48'),
(102, 'Nutella ', 'Crepa de Nutella', 0, 0, 0, 0, 35, 45, 45, 45, 0, 0, 0, NULL, '0', '', 1, '2022-01-18 02:02:23'),
(103, 'Cajeta ', 'Crepa de Cajeta ', 0, 0, 0, 0, 30, 40, 40, 40, 0, 0, 0, NULL, '0', '', 1, '2022-01-18 02:04:17'),
(104, 'Lechera ', 'Crepa de lechera ', 0, 0, 0, 0, 30, 40, 40, 40, 0, 0, 0, NULL, '0', '', 1, '2022-01-18 02:06:41'),
(105, 'Mermelada de Fresa', 'Crepa de Mermelada de Fresa', 0, 0, 0, 0, 30, 40, 40, 40, 0, 0, 0, NULL, '0', '', 1, '2022-01-18 02:09:07'),
(106, 'Mermelada de Zarzamo', 'Crepa de Mermelada de Zarzamora', 0, 0, 0, 0, 30, 40, 40, 40, 0, 0, 0, NULL, '0', '', 1, '2022-01-18 02:11:10'),
(107, 'Mermelada de Manzana', 'Crepa de Mermelada de Manzana', 0, 0, 0, 0, 30, 40, 40, 40, 0, 0, 0, NULL, '0', '', 1, '2022-01-18 02:13:06'),
(108, 'Hershey ', 'Crepa de Hershey ', 0, 0, 0, 0, 30, 40, 40, 40, 0, 0, 0, NULL, '0', '', 1, '2022-01-18 02:16:11'),
(109, 'Philadelphia', 'Crepa de Philadelphia', 0, 0, 0, 0, 30, 40, 40, 40, 0, 0, 0, NULL, '0', '', 1, '2022-01-18 02:18:12'),
(110, 'Manchego', 'Crepa de Manchego', 0, 0, 0, 0, 30, 40, 40, 40, 0, 0, 0, NULL, '0', '', 1, '2022-01-18 02:20:02'),
(111, 'Tortilla de Crepa ', 'Solo la tortilla de la crepa ', 0, 0, 0, 0, 25, 35, 35, 35, 0, 0, 0, NULL, '0', '', 1, '2022-01-18 02:23:18'),
(112, 'Kit de llevar Crepa', 'Aluminio y Charola de Unicel ', 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, NULL, '0', '', 1, '2022-01-18 21:36:00'),
(113, 'Extra de Chamoyada', 'Shot de Chamoy y Polvo Miguelito ', 0, 0, 0, 0, 10, 20, 20, 20, 0, 0, 0, NULL, '0', '', 1, '2022-01-19 01:31:23'),
(114, 'CBO-1', 'Combo 1 de coca con crepa de cajeta', 0, 0, 0, 50, 50, 50, 50, 50, 50, 50, 50, NULL, '1', '', 1, '2022-01-25 19:44:45'),
(115, 'CMBO-PRUE', 'COMBO DE PRUEBA', 0, 0, 0, 50, 50, 50, 50, 50, 50, 50, 50, NULL, '1', '', 1, '2022-02-02 20:01:11'),
(116, 'CBO-02', 'COMBO DE PRUEBA 2', 0, 0, 0, 60, 60, 60, 60, 60, 60, 60, 60, NULL, '1', '', 1, '2022-02-02 20:50:36'),
(117, 'CBO-DIN-01', 'COMBO DINAMICO 1 DE 1 BEBIDA CALIENTE + 1 CREPA 2 INGREDIENTES ', 0, -1, 0, 50, 50, 55, 55, 55, 55, 55, 55, NULL, '1', '2', 1, '2022-02-28 17:24:05'),
(118, 'CRE-SAL-01', 'crepa salada 1', 0, 0, 0, 25, 25, 25, 25, 25, 25, 25, 25, NULL, '0', '1', 1, '2022-03-14 16:49:21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_categos_combos`
--

DROP TABLE IF EXISTS `productos_categos_combos`;
CREATE TABLE IF NOT EXISTS `productos_categos_combos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_categoria` int(11) NOT NULL,
  `cantidad` varchar(4) COLLATE utf8_spanish_ci NOT NULL,
  `id_producto` int(11) NOT NULL,
  `tipo` varchar(1) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_reg` datetime NOT NULL,
  `cant_tot_prods` varchar(2) COLLATE utf8_spanish_ci NOT NULL,
  `estatus` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `productos_categos_combos`
--

INSERT INTO `productos_categos_combos` (`id`, `id_categoria`, `cantidad`, `id_producto`, `tipo`, `fecha_reg`, `cant_tot_prods`, `estatus`) VALUES
(1, 2, '1', 116, '1', '2022-02-25 19:35:03', '', '1'),
(2, 8, '2', 116, '1', '2022-02-25 19:35:03', '', '1'),
(3, 2, '1', 117, '1', '2022-02-28 12:24:56', '', '1'),
(4, 3, '1', 117, '1', '2022-02-28 12:24:56', '', '1'),
(5, 9, '1', 117, '1', '2022-02-28 12:24:56', '', '1'),
(6, 3, '1', 24, '0', '2022-02-28 13:10:37', '', '1'),
(7, 9, '1', 75, '0', '2022-02-28 13:12:13', '', '1'),
(8, 9, '1', 57, '0', '2022-02-28 13:12:28', '', '1'),
(9, 9, '1', 54, '0', '2022-02-28 13:12:42', '', '1'),
(10, 9, '1', 53, '0', '2022-02-28 13:13:31', '', '1'),
(11, 9, '1', 42, '0', '2022-02-28 13:13:52', '', '1'),
(12, 2, '1', 118, '0', '2022-03-14 10:49:21', '', '1'),
(13, 3, '1', 103, '0', '2022-03-14 18:05:39', '', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_combo`
--

DROP TABLE IF EXISTS `productos_combo`;
CREATE TABLE IF NOT EXISTS `productos_combo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_producto` int(11) NOT NULL,
  `id_producto_combo` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `activo` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `productos_combo`
--

INSERT INTO `productos_combo` (`id`, `id_producto`, `id_producto_combo`, `cantidad`, `activo`) VALUES
(1, 114, 61, 1, '1'),
(2, 114, 103, 1, '1'),
(3, 115, 103, 1, '1'),
(4, 115, 54, 1, '1'),
(5, 116, 103, 1, '1'),
(6, 116, 54, 1, '1'),
(7, 116, 22, 1, '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_insumos`
--

DROP TABLE IF EXISTS `productos_insumos`;
CREATE TABLE IF NOT EXISTS `productos_insumos` (
  `insumoId` int(11) NOT NULL AUTO_INCREMENT,
  `productoId` int(11) NOT NULL,
  `cantidad` float NOT NULL DEFAULT 0,
  `insumo` int(11) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`insumoId`),
  KEY `insumo_fk_producto` (`productoId`),
  KEY `insumo_fk_insumo` (`insumo`)
) ENGINE=InnoDB AUTO_INCREMENT=772 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `productos_insumos`
--

INSERT INTO `productos_insumos` (`insumoId`, `productoId`, `cantidad`, `insumo`, `activo`) VALUES
(1, 1, 40, 1, 0),
(2, 1, 50, 2, 1),
(3, 1, 2, 4, 1),
(4, 1, 20, 3, 1),
(5, 20, 50, 2, 1),
(6, 22, 40, 5, 1),
(7, 22, 15, 3, 1),
(8, 20, 15, 3, 1),
(9, 2, 40, 5, 1),
(10, 2, 20, 3, 1),
(11, 2, 0.5, 11, 1),
(12, 3, 50, 2, 1),
(13, 3, 20, 3, 1),
(14, 3, 45, 38, 1),
(15, 4, 50, 2, 1),
(16, 4, 50, 12, 1),
(17, 4, 40, 1, 1),
(18, 6, 25, 8, 1),
(19, 6, 40, 1, 1),
(20, 6, 1, 48, 1),
(21, 1, 1, 48, 1),
(22, 2, 1, 48, 1),
(23, 3, 1, 48, 1),
(24, 4, 1, 48, 1),
(25, 5, 40, 1, 1),
(26, 5, 25, 6, 1),
(27, 5, 20, 9, 1),
(28, 5, 1, 48, 1),
(29, 7, 40, 1, 1),
(30, 7, 2, 13, 1),
(31, 7, 20, 9, 1),
(32, 7, 1, 48, 1),
(33, 8, 40, 1, 1),
(34, 8, 25, 6, 1),
(35, 8, 1, 48, 1),
(36, 9, 50, 12, 1),
(37, 9, 0.5, 11, 1),
(38, 9, 50, 2, 1),
(39, 9, 40, 1, 1),
(40, 9, 1, 48, 1),
(41, 40, 40, 41, 1),
(42, 40, 250, 39, 1),
(43, 40, 178, 30, 1),
(44, 40, 1, 51, 1),
(45, 40, 1, 46, 1),
(46, 40, 1, 47, 1),
(47, 40, 0.1, 68, 1),
(48, 10, 70, 19, 1),
(49, 10, 40, 1, 1),
(50, 42, 1, 56, 1),
(51, 36, 3, 13, 1),
(52, 36, 178, 30, 1),
(53, 36, 1, 46, 1),
(54, 36, 1, 47, 1),
(55, 36, 1, 51, 1),
(56, 36, 70, 18, 0),
(57, 36, 250, 39, 1),
(58, 10, 2, 54, 1),
(59, 10, 1, 48, 1),
(60, 11, 40, 1, 1),
(61, 11, 50, 2, 1),
(62, 11, 1, 48, 1),
(63, 45, 1, 46, 1),
(64, 57, 1, 50, 1),
(65, 37, 35, 16, 1),
(66, 37, 1, 46, 1),
(67, 37, 1, 47, 1),
(68, 37, 1, 51, 1),
(69, 37, 70, 18, 0),
(70, 37, 250, 39, 1),
(71, 37, 90, 30, 1),
(72, 12, 70, 19, 1),
(73, 12, 20, 21, 1),
(74, 12, 25, 22, 1),
(75, 38, 80, 6, 1),
(76, 38, 1, 46, 1),
(77, 38, 1, 47, 1),
(78, 38, 1, 51, 1),
(79, 38, 70, 18, 0),
(80, 38, 250, 39, 1),
(81, 38, 178, 30, 1),
(82, 12, 1, 48, 1),
(83, 12, 30, 57, 1),
(84, 13, 40, 1, 1),
(85, 13, 50, 12, 1),
(86, 13, 1, 48, 1),
(87, 14, 70, 19, 1),
(88, 14, 90, 20, 1),
(89, 14, 60, 53, 1),
(90, 14, 1, 48, 1),
(91, 39, 1.5, 28, 1),
(92, 39, 70, 18, 1),
(93, 39, 1, 46, 1),
(94, 39, 1, 47, 1),
(95, 39, 1, 51, 1),
(96, 39, 178, 30, 1),
(97, 39, 250, 39, 1),
(98, 43, 70, 18, 0),
(99, 43, 1, 46, 1),
(100, 43, 1, 47, 1),
(101, 43, 1, 51, 1),
(102, 43, 50, 10, 1),
(103, 43, 250, 39, 1),
(104, 43, 178, 30, 1),
(105, 15, 50, 2, 1),
(106, 15, 60, 11, 1),
(107, 15, 1, 48, 1),
(108, 54, 350, 30, 1),
(109, 54, 75, 40, 1),
(110, 54, 1, 49, 1),
(111, 54, 1, 50, 1),
(112, 46, 3, 14, 1),
(113, 46, 1, 46, 1),
(114, 46, 1, 47, 1),
(115, 46, 1, 51, 1),
(116, 46, 70, 18, 1),
(117, 46, 178, 30, 1),
(118, 46, 250, 39, 1),
(119, 16, 70, 19, 1),
(120, 16, 70, 23, 1),
(121, 16, 40, 52, 1),
(122, 16, 35, 42, 1),
(123, 16, 1, 48, 1),
(124, 53, 350, 30, 1),
(125, 53, 75, 40, 1),
(126, 53, 40, 5, 1),
(127, 53, 1, 49, 1),
(128, 53, 1, 50, 1),
(129, 17, 40, 5, 1),
(130, 17, 40, 1, 1),
(131, 17, 1, 48, 1),
(132, 47, 70, 18, 1),
(133, 47, 2, 25, 1),
(134, 47, 1, 46, 1),
(135, 47, 1, 47, 1),
(136, 47, 1, 51, 1),
(137, 47, 178, 30, 1),
(138, 47, 250, 39, 1),
(139, 18, 50, 2, 1),
(140, 18, 50, 12, 1),
(141, 18, 1, 48, 1),
(142, 57, 1, 49, 1),
(143, 57, 350, 55, 1),
(144, 57, 75, 40, 1),
(145, 49, 70, 18, 1),
(146, 49, 250, 39, 1),
(147, 49, 178, 30, 1),
(148, 49, 1, 46, 1),
(149, 49, 1, 47, 1),
(150, 49, 1, 51, 1),
(151, 49, 3, 29, 1),
(152, 19, 70, 19, 1),
(153, 19, 40, 1, 1),
(154, 19, 90, 20, 1),
(155, 19, 1, 48, 1),
(156, 20, 1, 48, 1),
(157, 51, 3, 27, 1),
(158, 51, 1, 46, 1),
(159, 51, 1, 47, 1),
(160, 51, 1, 51, 1),
(161, 51, 70, 18, 1),
(162, 51, 250, 39, 1),
(163, 51, 178, 30, 1),
(164, 52, 1, 50, 1),
(165, 52, 1, 49, 1),
(166, 52, 75, 40, 1),
(167, 52, 50, 2, 1),
(168, 52, 350, 30, 1),
(169, 21, 70, 19, 1),
(170, 21, 70, 23, 1),
(171, 21, 35, 42, 1),
(172, 45, 1, 47, 1),
(173, 45, 1, 51, 1),
(174, 45, 70, 18, 0),
(175, 45, 250, 39, 1),
(176, 45, 90, 30, 1),
(177, 45, 45, 41, 1),
(178, 45, 35, 58, 1),
(179, 22, 1, 48, 1),
(180, 21, 1, 48, 1),
(181, 50, 1, 49, 1),
(182, 50, 1, 50, 1),
(183, 50, 75, 40, 1),
(184, 50, 350, 30, 1),
(185, 50, 4, 14, 1),
(186, 23, 90, 20, 1),
(187, 23, 70, 19, 1),
(188, 23, 30, 57, 1),
(189, 23, 1, 48, 1),
(190, 48, 1, 49, 1),
(191, 48, 1, 50, 1),
(192, 48, 350, 30, 1),
(193, 48, 75, 40, 1),
(194, 48, 2, 27, 1),
(195, 55, 1, 47, 1),
(196, 55, 1, 46, 1),
(197, 55, 1, 51, 1),
(198, 55, 250, 39, 1),
(199, 55, 100, 44, 1),
(200, 55, 120, 30, 1),
(201, 56, 1, 46, 1),
(202, 56, 1, 51, 1),
(203, 56, 1, 47, 1),
(204, 56, 250, 39, 1),
(205, 56, 120, 30, 1),
(206, 56, 100, 45, 1),
(207, 44, 1, 49, 1),
(208, 44, 1, 50, 1),
(209, 44, 75, 40, 1),
(210, 44, 350, 30, 1),
(211, 44, 70, 10, 1),
(212, 24, 40, 5, 1),
(213, 24, 60, 11, 1),
(214, 24, 1, 48, 1),
(215, 42, 75, 40, 1),
(216, 25, 20, 21, 1),
(217, 25, 25, 22, 1),
(218, 25, 90, 20, 1),
(219, 25, 70, 19, 1),
(220, 25, 1, 48, 1),
(221, 26, 90, 20, 1),
(222, 26, 70, 19, 1),
(223, 26, 1, 48, 1),
(224, 27, 25, 22, 1),
(225, 27, 70, 19, 1),
(226, 27, 1, 48, 1),
(227, 59, 1, 46, 1),
(228, 59, 1, 47, 1),
(229, 59, 1, 51, 1),
(230, 59, 250, 39, 1),
(231, 59, 90, 30, 1),
(232, 59, 100, 59, 1),
(233, 59, 2, 60, 1),
(234, 30, 20, 21, 1),
(235, 30, 70, 19, 1),
(236, 30, 1, 48, 1),
(237, 28, 350, 30, 0),
(238, 28, 75, 40, 0),
(239, 28, 1, 49, 1),
(240, 28, 1, 50, 1),
(241, 28, 1, 48, 1),
(242, 28, 1, 61, 1),
(243, 32, 90, 20, 1),
(244, 32, 40, 1, 1),
(245, 32, 1, 48, 1),
(246, 35, 20, 21, 1),
(247, 35, 40, 1, 1),
(248, 35, 1, 48, 1),
(249, 34, 40, 1, 1),
(250, 34, 25, 22, 1),
(251, 34, 1, 48, 1),
(252, 63, 1, 48, 1),
(253, 63, 40, 1, 1),
(254, 63, 50, 12, 1),
(255, 63, 3, 14, 1),
(256, 64, 1, 48, 1),
(257, 64, 3, 27, 1),
(258, 64, 50, 2, 1),
(259, 64, 20, 3, 1),
(260, 65, 1, 48, 1),
(261, 65, 3, 29, 1),
(262, 65, 1, 25, 1),
(263, 65, 40, 1, 1),
(264, 66, 1, 48, 1),
(265, 66, 2, 26, 1),
(266, 66, 50, 12, 1),
(267, 66, 50, 2, 1),
(268, 67, 1, 48, 1),
(269, 67, 2, 25, 1),
(270, 67, 20, 3, 1),
(271, 67, 40, 66, 1),
(272, 68, 1, 48, 1),
(273, 68, 50, 12, 1),
(274, 68, 8, 65, 1),
(275, 68, 40, 1, 1),
(276, 23, 60, 53, 1),
(277, 65, 20, 62, 1),
(278, 65, 40, 63, 1),
(279, 37, 40, 43, 1),
(280, 36, 40, 43, 1),
(281, 38, 40, 43, 1),
(282, 39, 40, 43, 1),
(283, 41, 40, 43, 1),
(284, 41, 1, 46, 1),
(285, 41, 1, 47, 1),
(286, 41, 70, 18, 0),
(287, 41, 250, 39, 1),
(288, 41, 178, 30, 1),
(289, 41, 2, 28, 1),
(290, 43, 40, 43, 1),
(291, 43, 75, 58, 1),
(292, 69, 1, 46, 1),
(293, 69, 1, 47, 1),
(294, 69, 3, 27, 1),
(295, 69, 70, 18, 0),
(296, 69, 178, 30, 1),
(297, 69, 250, 39, 1),
(298, 69, 40, 41, 1),
(299, 70, 1, 46, 1),
(300, 70, 1, 47, 1),
(301, 70, 1, 51, 1),
(302, 70, 3, 29, 1),
(303, 70, 70, 18, 1),
(304, 70, 250, 39, 1),
(305, 70, 178, 30, 1),
(306, 70, 40, 41, 1),
(307, 69, 1, 51, 1),
(308, 71, 1, 46, 1),
(309, 71, 1, 47, 1),
(310, 71, 1, 51, 1),
(311, 71, 70, 18, 0),
(312, 71, 178, 30, 1),
(313, 71, 40, 41, 1),
(314, 71, 250, 39, 1),
(315, 71, 4, 14, 1),
(316, 72, 50, 66, 1),
(317, 72, 40, 43, 1),
(318, 72, 1, 46, 1),
(319, 72, 1, 47, 1),
(320, 72, 1, 51, 1),
(321, 72, 178, 30, 1),
(322, 72, 250, 39, 1),
(323, 72, 0.1, 68, 1),
(324, 73, 1, 46, 1),
(325, 73, 1, 51, 1),
(326, 73, 1, 47, 1),
(327, 73, 178, 30, 1),
(328, 73, 250, 39, 1),
(329, 73, 70, 18, 0),
(330, 73, 50, 2, 1),
(331, 73, 40, 43, 1),
(332, 74, 1, 46, 1),
(333, 74, 1, 47, 1),
(334, 74, 1, 51, 1),
(335, 74, 178, 30, 1),
(336, 74, 250, 39, 1),
(337, 74, 70, 18, 0),
(338, 74, 50, 5, 1),
(339, 74, 40, 43, 1),
(340, 75, 1, 49, 1),
(341, 75, 1, 50, 1),
(342, 75, 75, 40, 1),
(343, 75, 350, 30, 1),
(344, 75, 50, 66, 1),
(345, 84, 50, 12, 1),
(346, 83, 60, 11, 1),
(347, 85, 2, 13, 1),
(348, 86, 2, 14, 1),
(349, 87, 35, 16, 1),
(350, 88, 20, 3, 1),
(351, 89, 0.1, 68, 1),
(352, 90, 70, 19, 1),
(353, 92, 20, 21, 1),
(354, 91, 90, 20, 1),
(355, 93, 40, 1, 1),
(356, 94, 25, 22, 1),
(357, 95, 70, 23, 1),
(358, 96, 50, 24, 1),
(359, 97, 1, 25, 1),
(360, 98, 1, 28, 1),
(361, 99, 1, 27, 1),
(362, 100, 1, 29, 1),
(363, 101, 2, 26, 1),
(364, 1, 100, 30, 1),
(365, 1, 33, 31, 1),
(366, 1, 8, 32, 1),
(367, 1, 5, 33, 1),
(368, 1, 0.1, 36, 1),
(369, 1, 10, 35, 1),
(370, 1, 3, 37, 1),
(371, 2, 100, 30, 1),
(372, 2, 33, 31, 1),
(373, 2, 8, 32, 1),
(374, 2, 5, 33, 1),
(375, 2, 0.1, 36, 1),
(376, 2, 10, 35, 1),
(377, 2, 3, 37, 1),
(378, 3, 100, 30, 1),
(379, 3, 33, 31, 1),
(380, 3, 8, 32, 1),
(381, 3, 5, 33, 1),
(382, 3, 0.1, 36, 1),
(383, 3, 10, 35, 1),
(384, 3, 3, 37, 1),
(385, 4, 100, 30, 1),
(386, 4, 33, 31, 1),
(387, 4, 8, 32, 1),
(388, 4, 5, 33, 1),
(389, 4, 0.1, 36, 1),
(390, 4, 10, 35, 1),
(391, 4, 3, 37, 1),
(392, 5, 100, 30, 1),
(393, 5, 33, 31, 1),
(394, 5, 8, 32, 1),
(395, 5, 5, 33, 1),
(396, 5, 0.1, 36, 1),
(397, 5, 10, 35, 1),
(398, 5, 3, 37, 1),
(399, 6, 5, 70, 1),
(400, 6, 100, 30, 1),
(401, 6, 33, 31, 1),
(402, 6, 8, 32, 1),
(403, 6, 5, 33, 1),
(404, 6, 0.1, 36, 1),
(405, 6, 5, 35, 1),
(406, 6, 3, 37, 1),
(407, 7, 100, 30, 1),
(408, 7, 33, 31, 1),
(409, 7, 8, 32, 1),
(410, 7, 5, 33, 1),
(411, 7, 0.1, 36, 1),
(412, 7, 10, 35, 1),
(413, 7, 3, 37, 1),
(414, 8, 100, 30, 1),
(415, 8, 33, 31, 1),
(416, 8, 8, 32, 1),
(417, 8, 5, 33, 1),
(418, 8, 0.1, 36, 1),
(419, 8, 10, 35, 1),
(420, 8, 3, 37, 1),
(421, 9, 100, 30, 1),
(422, 9, 33, 31, 1),
(423, 9, 8, 32, 1),
(424, 9, 5, 33, 1),
(425, 9, 0.1, 36, 1),
(426, 9, 10, 35, 1),
(427, 9, 3, 37, 1),
(428, 10, 100, 30, 1),
(429, 10, 33, 31, 1),
(430, 10, 8, 32, 1),
(431, 10, 5, 33, 1),
(432, 10, 0.1, 36, 1),
(433, 10, 10, 35, 1),
(434, 10, 3, 37, 1),
(435, 11, 100, 30, 1),
(436, 11, 33, 31, 1),
(437, 11, 8, 32, 1),
(438, 11, 5, 33, 1),
(439, 11, 0.1, 36, 1),
(440, 11, 10, 35, 1),
(441, 11, 3, 37, 1),
(442, 12, 100, 30, 1),
(443, 12, 33, 31, 1),
(444, 12, 8, 32, 1),
(445, 12, 5, 33, 1),
(446, 12, 0.1, 36, 1),
(447, 12, 10, 35, 1),
(448, 12, 3, 37, 1),
(449, 13, 100, 30, 1),
(450, 13, 33, 31, 1),
(451, 13, 8, 32, 1),
(452, 13, 5, 33, 1),
(453, 13, 0.1, 36, 1),
(454, 13, 10, 35, 1),
(455, 13, 3, 37, 1),
(456, 14, 100, 30, 1),
(457, 14, 33, 31, 1),
(458, 14, 8, 32, 1),
(459, 14, 5, 33, 1),
(460, 14, 0.1, 36, 1),
(461, 14, 10, 35, 1),
(462, 14, 3, 37, 1),
(463, 15, 100, 30, 1),
(464, 15, 33, 31, 1),
(465, 15, 8, 32, 1),
(466, 15, 5, 33, 1),
(467, 15, 0.1, 36, 1),
(468, 15, 10, 35, 1),
(469, 15, 3, 37, 1),
(470, 16, 100, 30, 1),
(471, 16, 33, 31, 1),
(472, 16, 8, 32, 1),
(473, 16, 5, 33, 1),
(474, 16, 0.1, 36, 1),
(475, 16, 10, 35, 1),
(476, 16, 3, 37, 1),
(477, 17, 100, 30, 1),
(478, 17, 33, 31, 1),
(479, 17, 8, 32, 1),
(480, 17, 5, 33, 1),
(481, 17, 0.1, 36, 1),
(482, 17, 10, 35, 1),
(483, 17, 3, 37, 1),
(484, 18, 100, 30, 1),
(485, 18, 33, 31, 1),
(486, 18, 8, 32, 1),
(487, 18, 5, 33, 1),
(488, 18, 0.1, 36, 1),
(489, 18, 10, 35, 1),
(490, 18, 3, 37, 1),
(491, 19, 100, 30, 1),
(492, 19, 33, 31, 1),
(493, 19, 8, 32, 1),
(494, 19, 5, 33, 1),
(495, 19, 0.1, 36, 1),
(496, 19, 10, 35, 1),
(497, 19, 3, 37, 1),
(498, 20, 100, 30, 1),
(499, 20, 33, 31, 1),
(500, 20, 8, 32, 1),
(501, 20, 5, 33, 1),
(502, 20, 0.1, 36, 1),
(503, 20, 10, 35, 1),
(504, 20, 3, 37, 1),
(505, 21, 100, 30, 1),
(506, 21, 33, 31, 1),
(507, 21, 8, 32, 1),
(508, 21, 5, 33, 1),
(509, 21, 0.1, 36, 1),
(510, 21, 10, 35, 1),
(511, 21, 3, 37, 1),
(512, 22, 100, 30, 1),
(513, 22, 33, 31, 1),
(514, 22, 8, 32, 1),
(515, 22, 5, 33, 1),
(516, 22, 0.1, 36, 1),
(517, 22, 10, 35, 1),
(518, 22, 3, 37, 1),
(519, 23, 100, 30, 1),
(520, 23, 33, 31, 1),
(521, 23, 8, 32, 1),
(522, 23, 5, 33, 1),
(523, 23, 0.1, 36, 1),
(524, 23, 10, 35, 1),
(525, 23, 3, 37, 1),
(526, 24, 100, 30, 1),
(527, 24, 33, 31, 1),
(528, 24, 8, 32, 1),
(529, 24, 5, 33, 1),
(530, 24, 0.1, 36, 1),
(531, 24, 10, 35, 1),
(532, 24, 3, 37, 1),
(533, 25, 100, 30, 1),
(534, 25, 33, 31, 1),
(535, 25, 8, 32, 1),
(536, 25, 5, 33, 1),
(537, 25, 0.1, 36, 1),
(538, 25, 10, 35, 1),
(539, 25, 3, 37, 1),
(540, 26, 100, 30, 1),
(541, 26, 33, 31, 1),
(542, 26, 8, 32, 1),
(543, 26, 5, 33, 1),
(544, 26, 0.1, 36, 1),
(545, 26, 10, 35, 1),
(546, 26, 3, 37, 1),
(547, 27, 100, 30, 1),
(548, 27, 33, 31, 1),
(549, 27, 8, 32, 1),
(550, 27, 5, 33, 1),
(551, 27, 0.1, 36, 1),
(552, 27, 10, 35, 1),
(553, 27, 3, 37, 1),
(554, 30, 100, 30, 1),
(555, 30, 33, 31, 1),
(556, 30, 8, 32, 1),
(557, 30, 5, 33, 1),
(558, 30, 0.1, 36, 1),
(559, 30, 10, 35, 1),
(560, 30, 3, 37, 1),
(561, 32, 100, 30, 1),
(562, 32, 33, 31, 1),
(563, 32, 8, 32, 1),
(564, 32, 5, 33, 1),
(565, 32, 0.1, 36, 1),
(566, 32, 10, 35, 1),
(567, 32, 3, 37, 1),
(568, 34, 100, 30, 1),
(569, 34, 33, 31, 1),
(570, 34, 8, 32, 1),
(571, 34, 5, 33, 1),
(572, 34, 0.1, 36, 1),
(573, 34, 10, 35, 1),
(574, 34, 3, 37, 1),
(575, 35, 100, 30, 1),
(576, 35, 33, 31, 1),
(577, 35, 8, 32, 1),
(578, 35, 5, 33, 1),
(579, 35, 0.1, 36, 1),
(580, 35, 10, 35, 1),
(581, 35, 3, 37, 1),
(582, 63, 100, 30, 1),
(583, 63, 33, 31, 1),
(584, 63, 8, 32, 1),
(585, 63, 5, 33, 1),
(586, 63, 0.1, 36, 1),
(587, 63, 10, 35, 1),
(588, 63, 3, 37, 1),
(589, 64, 100, 30, 1),
(590, 64, 33, 31, 1),
(591, 64, 8, 32, 1),
(592, 64, 5, 33, 1),
(593, 64, 0.1, 36, 1),
(594, 64, 10, 35, 1),
(595, 64, 3, 37, 1),
(596, 65, 100, 30, 1),
(597, 65, 33, 31, 1),
(598, 65, 8, 32, 1),
(599, 65, 5, 33, 1),
(600, 65, 0.1, 36, 1),
(601, 65, 10, 35, 1),
(602, 65, 3, 37, 1),
(603, 66, 100, 30, 1),
(604, 66, 33, 31, 1),
(605, 66, 8, 32, 1),
(606, 66, 5, 33, 1),
(607, 66, 0.1, 36, 1),
(608, 66, 10, 35, 1),
(609, 66, 3, 37, 1),
(610, 67, 100, 30, 1),
(611, 67, 33, 31, 1),
(612, 67, 8, 32, 1),
(613, 67, 5, 33, 1),
(614, 67, 0.1, 36, 1),
(615, 67, 10, 35, 1),
(616, 67, 3, 37, 1),
(617, 68, 100, 30, 1),
(618, 68, 33, 31, 1),
(619, 68, 8, 32, 1),
(620, 68, 5, 33, 1),
(621, 68, 0.1, 36, 1),
(622, 68, 10, 35, 1),
(623, 68, 3, 37, 1),
(624, 102, 50, 2, 1),
(625, 102, 1, 48, 1),
(626, 102, 100, 30, 1),
(627, 102, 33, 31, 1),
(628, 102, 8, 32, 1),
(629, 102, 5, 33, 1),
(630, 102, 0.1, 36, 1),
(631, 102, 10, 35, 1),
(632, 102, 3, 37, 1),
(633, 103, 40, 5, 1),
(634, 103, 1, 48, 1),
(635, 103, 100, 30, 1),
(636, 103, 33, 31, 1),
(637, 103, 8, 32, 1),
(638, 103, 5, 33, 1),
(639, 103, 0.1, 36, 1),
(640, 103, 10, 35, 1),
(641, 103, 3, 37, 1),
(642, 104, 30, 9, 1),
(643, 104, 1, 48, 1),
(644, 104, 100, 30, 1),
(645, 104, 33, 31, 1),
(646, 104, 8, 32, 1),
(647, 104, 5, 33, 1),
(648, 104, 0.1, 36, 1),
(649, 104, 10, 35, 1),
(650, 104, 3, 37, 1),
(651, 105, 35, 7, 1),
(652, 105, 1, 48, 1),
(653, 105, 100, 30, 1),
(654, 105, 33, 31, 1),
(655, 105, 8, 32, 1),
(656, 105, 5, 33, 1),
(657, 105, 10, 35, 1),
(658, 105, 0.1, 36, 1),
(659, 105, 3, 37, 1),
(660, 106, 35, 6, 1),
(661, 106, 1, 48, 1),
(662, 106, 100, 30, 1),
(663, 106, 33, 31, 1),
(664, 106, 8, 32, 1),
(665, 106, 0.1, 36, 1),
(666, 106, 5, 33, 1),
(667, 106, 10, 35, 1),
(668, 106, 3, 37, 1),
(669, 107, 35, 8, 1),
(670, 107, 1, 48, 1),
(671, 107, 100, 30, 1),
(672, 107, 33, 31, 1),
(673, 107, 8, 32, 1),
(674, 107, 5, 33, 1),
(675, 107, 0.1, 36, 1),
(676, 107, 10, 35, 1),
(677, 107, 3, 37, 1),
(678, 108, 30, 10, 1),
(679, 108, 1, 48, 1),
(680, 108, 100, 30, 1),
(681, 108, 33, 31, 1),
(682, 108, 8, 32, 1),
(683, 108, 5, 33, 1),
(684, 108, 0.1, 36, 1),
(685, 108, 10, 35, 1),
(686, 108, 3, 37, 1),
(687, 109, 40, 1, 1),
(688, 109, 1, 48, 1),
(689, 109, 100, 30, 1),
(690, 109, 33, 31, 1),
(691, 109, 8, 32, 1),
(692, 109, 5, 33, 1),
(693, 109, 0.1, 36, 1),
(694, 109, 10, 35, 1),
(695, 109, 3, 37, 1),
(696, 110, 70, 19, 1),
(697, 110, 1, 48, 1),
(698, 110, 100, 30, 1),
(699, 110, 33, 31, 1),
(700, 110, 8, 32, 1),
(701, 110, 5, 33, 1),
(702, 110, 0.1, 36, 1),
(703, 110, 10, 35, 1),
(704, 110, 3, 37, 1),
(705, 111, 1, 48, 1),
(706, 111, 100, 30, 1),
(707, 111, 33, 31, 1),
(708, 111, 8, 32, 1),
(709, 111, 5, 33, 1),
(710, 111, 0.1, 36, 1),
(711, 111, 10, 35, 1),
(712, 111, 3, 37, 1),
(713, 112, 0, 72, 1),
(714, 112, 0, 74, 1),
(715, 69, 30, 2, 1),
(716, 36, 0.1, 68, 1),
(717, 36, 25, 30, 1),
(718, 36, 25, 67, 1),
(719, 41, 0.1, 68, 1),
(720, 41, 25, 30, 1),
(721, 41, 25, 67, 1),
(722, 38, 0.1, 68, 1),
(723, 38, 25, 30, 1),
(724, 38, 25, 67, 1),
(725, 41, 1, 51, 1),
(726, 43, 0.1, 68, 1),
(727, 43, 25, 30, 1),
(728, 43, 25, 67, 1),
(729, 45, 25, 67, 1),
(730, 45, 0.1, 68, 1),
(731, 45, 25, 30, 1),
(732, 40, 25, 30, 1),
(733, 40, 25, 67, 1),
(734, 37, 0.1, 68, 1),
(735, 37, 25, 30, 1),
(736, 37, 25, 67, 1),
(737, 74, 0.1, 68, 1),
(738, 74, 25, 30, 1),
(739, 74, 25, 67, 1),
(740, 73, 0.1, 68, 1),
(741, 73, 25, 30, 1),
(742, 73, 25, 67, 1),
(743, 72, 25, 30, 1),
(744, 72, 25, 67, 1),
(745, 71, 0.1, 68, 1),
(746, 71, 25, 30, 1),
(747, 71, 25, 67, 1),
(748, 70, 0.1, 68, 1),
(749, 70, 25, 30, 1),
(750, 70, 25, 67, 1),
(751, 69, 0.1, 68, 1),
(752, 69, 25, 30, 1),
(753, 69, 25, 67, 1),
(754, 55, 20, 43, 1),
(755, 56, 20, 43, 1),
(756, 89, 25, 30, 1),
(757, 89, 25, 67, 1),
(758, 113, 35, 59, 1),
(759, 113, 15, 60, 1),
(760, 76, 50, 2, 1),
(761, 77, 40, 5, 1),
(762, 78, 40, 6, 1),
(763, 79, 40, 7, 1),
(764, 80, 40, 8, 1),
(765, 81, 30, 9, 1),
(766, 82, 30, 10, 1),
(767, 48, 30, 2, 1),
(768, 114, 1, 61, 0),
(770, 118, 1, 20, 1),
(771, 118, 1, 19, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

DROP TABLE IF EXISTS `proveedores`;
CREATE TABLE IF NOT EXISTS `proveedores` (
  `id_proveedor` int(11) NOT NULL AUTO_INCREMENT,
  `razon_social` varchar(50) NOT NULL,
  `domicilio` varchar(30) CHARACTER SET utf8 NOT NULL COMMENT 'calle',
  `ciudad` varchar(50) CHARACTER SET utf8 NOT NULL,
  `cp` varchar(8) CHARACTER SET utf8 NOT NULL,
  `id_estado` int(11) NOT NULL,
  `telefono_local` varchar(10) NOT NULL,
  `telefono_celular` varchar(10) NOT NULL,
  `contacto` varchar(100) NOT NULL,
  `email_contacto` varchar(60) NOT NULL,
  `rfc` varchar(13) NOT NULL,
  `fax` varchar(20) NOT NULL,
  `obser` text NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id_proveedor`),
  KEY `constraint_fk_27` (`id_estado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ticket`
--

DROP TABLE IF EXISTS `ticket`;
CREATE TABLE IF NOT EXISTS `ticket` (
  `id_ticket` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` text CHARACTER SET utf8 NOT NULL,
  `mensaje` text CHARACTER SET utf8 NOT NULL,
  `mensaje2` text CHARACTER SET utf8 NOT NULL,
  `fuente` int(1) NOT NULL,
  `tamano` int(2) NOT NULL,
  `margensup` int(2) NOT NULL,
  PRIMARY KEY (`id_ticket`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ticket`
--

INSERT INTO `ticket` (`id_ticket`, `titulo`, `mensaje`, `mensaje2`, `fuente`, `tamano`, `margensup`) VALUES
(1, 'Los mejores haciendo crepas!', '¡GRACIAS POR SU COMPRA!', 'ESTE TICKET NO ES UNA REPRESENTACIÓN FISCAL', 2, 9, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `turno`
--

DROP TABLE IF EXISTS `turno`;
CREATE TABLE IF NOT EXISTS `turno` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `horaa` time NOT NULL,
  `fechacierre` date NOT NULL,
  `horac` time NOT NULL,
  `cantidad` float NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `user` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `turno`
--

INSERT INTO `turno` (`id`, `fecha`, `horaa`, `fechacierre`, `horac`, `cantidad`, `nombre`, `status`, `user`) VALUES
(59, '2016-10-14', '00:37:05', '0000-00-00', '00:37:45', 100, 'x', 'cerrado', 'user'),
(60, '2016-10-14', '00:39:37', '0000-00-00', '06:11:06', 1, 'b', 'cerrado', 'user'),
(61, '2016-10-24', '00:38:48', '0000-00-00', '00:40:57', 100, 'dia', 'cerrado', 'user'),
(62, '2016-10-24', '00:43:15', '0000-00-00', '11:03:17', 100, '100', 'cerrado', 'user'),
(63, '2017-05-19', '11:03:33', '0000-00-00', '20:00:22', 100, 'aaa', 'cerrado', 'user'),
(64, '2018-01-10', '20:02:43', '0000-00-00', '20:11:01', 0, '', 'cerrado', 'user'),
(65, '2018-01-10', '20:19:13', '0000-00-00', '09:20:55', 0, '', 'cerrado', 'user'),
(66, '2018-01-15', '09:21:24', '0000-00-00', '00:00:00', 0, 'ESMERLDA', 'abierto', 'user'),
(67, '2018-01-15', '09:21:24', '0000-00-00', '00:00:00', 0, 'ESMERLDA', 'abierto', 'user'),
(68, '2018-01-15', '09:21:27', '0000-00-00', '10:01:26', 0, 'ESMERLDA', 'cerrado', 'user'),
(69, '2018-01-15', '10:01:28', '0000-00-00', '00:00:00', 0, '', 'abierto', 'user'),
(70, '2018-01-15', '10:01:43', '0000-00-00', '14:10:45', 500, 'ESMERALDA', 'cerrado', 'user'),
(71, '2018-01-15', '14:11:09', '0000-00-00', '13:49:39', 600, 'marijo', 'cerrado', 'user'),
(72, '2018-01-16', '13:49:46', '0000-00-00', '00:00:00', 0, '', 'abierto', 'user'),
(73, '2018-01-16', '13:50:01', '0000-00-00', '00:00:00', 500, 'MARIJO', 'abierto', 'user'),
(74, '2018-01-16', '13:50:11', '0000-00-00', '13:45:17', 500, 'MARIJO', 'cerrado', 'user'),
(75, '2018-01-17', '13:45:36', '0000-00-00', '14:03:13', 0, '', 'cerrado', 'user'),
(76, '2018-01-17', '14:03:34', '0000-00-00', '14:48:36', 500, 'MARI JOSE', 'cerrado', 'user'),
(77, '2018-01-19', '14:48:47', '0000-00-00', '18:19:52', 500, 'marijo', 'cerrado', 'user'),
(78, '2018-01-22', '18:24:30', '0000-00-00', '08:39:02', 500, 'marijo', 'cerrado', 'user'),
(79, '2018-01-23', '08:39:13', '0000-00-00', '00:00:00', 0, '', 'abierto', 'user'),
(80, '2018-01-23', '08:39:27', '0000-00-00', '14:02:30', 500, 'ESMERALDA', 'cerrado', 'user'),
(81, '2018-01-23', '14:02:40', '0000-00-00', '00:00:00', 0, '', 'abierto', 'user'),
(82, '2018-01-23', '14:02:49', '0000-00-00', '09:41:46', 0, 'MARIJO', 'cerrado', 'user'),
(83, '2018-01-24', '09:44:21', '0000-00-00', '14:04:56', 500, 'ESME', 'cerrado', 'user'),
(84, '2018-01-24', '14:05:03', '0000-00-00', '14:05:20', 0, '', 'cerrado', 'user'),
(85, '2018-01-24', '14:05:30', '0000-00-00', '00:00:00', 0, '', 'abierto', 'user'),
(86, '2018-01-24', '14:05:55', '0000-00-00', '00:00:00', 0, '', 'abierto', 'user'),
(87, '2018-01-24', '14:06:08', '0000-00-00', '14:07:24', 0, 'MARIJO', 'cerrado', 'user'),
(88, '2018-01-24', '14:07:32', '0000-00-00', '00:00:00', 0, '', 'abierto', 'user'),
(89, '2018-01-24', '14:07:44', '0000-00-00', '07:30:20', 500, 'MARIJO', 'cerrado', 'user'),
(90, '2018-01-25', '07:30:32', '0000-00-00', '00:00:00', 500, '', 'abierto', 'user'),
(91, '2018-01-25', '07:30:39', '0000-00-00', '14:10:39', 500, 'esmeralda', 'cerrado', 'user'),
(92, '2018-01-25', '14:10:55', '0000-00-00', '00:00:00', 500, 'MARIJO', 'abierto', 'user'),
(93, '2018-01-25', '14:10:58', '0000-00-00', '07:42:52', 500, 'MARIJO', 'cerrado', 'user'),
(94, '2018-01-26', '07:43:03', '0000-00-00', '11:23:32', 500, 'esme', 'cerrado', 'user'),
(95, '2018-05-08', '12:00:16', '2018-05-11', '17:58:24', 400, 'prueba', 'cerrado', 'user'),
(96, '2018-05-15', '13:29:48', '2020-11-17', '19:00:06', 100, 'xxx', 'cerrado', 'user'),
(97, '2020-11-17', '19:00:12', '0000-00-00', '00:00:00', 0, '', 'abierto', 'user'),
(98, '2020-11-17', '19:00:58', '0000-00-00', '00:00:00', 220, 'Turno Juan', 'abierto', 'user'),
(99, '2020-11-17', '19:01:24', '2020-11-17', '19:06:07', 220, 'Turno Juan', 'cerrado', 'user'),
(100, '2020-11-17', '19:46:46', '2020-12-11', '17:46:44', 220, 'Pepe', 'cerrado', 'user'),
(101, '2020-12-12', '11:25:30', '2020-12-12', '11:25:47', 0, '', 'cerrado', 'user'),
(102, '2020-12-12', '11:31:44', '0000-00-00', '00:00:00', 100, 'Prueba 12dic', 'abierto', 'user');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `UsuarioID` int(11) NOT NULL AUTO_INCREMENT,
  `perfilId` int(11) NOT NULL,
  `personalId` int(11) NOT NULL,
  `Usuario` varchar(45) DEFAULT NULL,
  `contrasena` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`UsuarioID`),
  KEY `fk_usuarios_Perfiles1_idx` (`perfilId`),
  KEY `fk_usuarios_personal1_idx` (`personalId`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`UsuarioID`, `perfilId`, `personalId`, `Usuario`, `contrasena`) VALUES
(1, 1, 1, 'admin', '$2y$10$.yBpfsTTEmhYhECki.qy3eL45XaLo3MinuIPgiPsjmXBYItZXHUga'),
(2, 2, 2, 'kcortesa', '$2y$10$EYkSAydhj1aVTsVv903a9u518.lR0EbVC7FuCNNnIuNscn7GgelKO'),
(3, 2, 3, 'NellyCu', '$2y$10$9AogAieMYzGmlq65C/KX/OsQq/HXDptZMMpvmvMRFc2l4x8jJeYkq'),
(4, 2, 4, 'NellyCu', '$2y$10$9tQiTsX902im8xJ9JjIuPON9wRbsB/H8KSMkOFEMcIawyhyvu09C.'),
(5, 2, 5, 'Simio7', '$2y$10$6Islvm9PNHd1aiakjKPmre0C6dXxc5.DkcDOklxhLrNAhdscUKS6S'),
(6, 2, 6, 'Prima1', '$2y$10$eoSf9Vs2Y1p6dTec56fr7eNmyF9XbZymNqpiSI00Xac6O09bgCiQu'),
(7, 2, 7, 'Prima2', '$2y$10$OCqStQiIoENNhdfDhHY80eI4Zqhdymv4EVhA1oZO1Sr.TqNpYcEg6'),
(8, 2, 8, 'yoshira ', '$2y$10$U0ZpXpqbGTuD7W4NiS0U3uIsmQQgjSKPlX4wJ5ZoyGEG4falCIAI6'),
(9, 2, 9, 'ceyru', '$2y$10$LWUOTWuCXlijlpMoO6/Z/urSf6h8jxRZGwcOZJs3z62LFYBatGPJ2'),
(10, 2, 10, 'poncho ', '$2y$10$Q9FeLmUU0WwGSeZXsSEdouEqWSsOCLcbXQcarCS8tP0gBAexapNhO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

DROP TABLE IF EXISTS `ventas`;
CREATE TABLE IF NOT EXISTS `ventas` (
  `id_venta` int(11) NOT NULL AUTO_INCREMENT,
  `id_personal` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `tipo_costo` varchar(1) NOT NULL COMMENT '1=mostrador,2=mesa,3=docimilio',
  `metodo` int(1) NOT NULL COMMENT '1 efectivo, 2 credito, 3 debito, 4=plataforma',
  `subtotal` double NOT NULL,
  `descuento` double NOT NULL COMMENT '0 % 5 % 7%',
  `descuentocant` double NOT NULL,
  `monto_total` double NOT NULL,
  `pagotarjeta` float NOT NULL,
  `efectivo` float NOT NULL,
  `cancelado` int(11) NOT NULL,
  `hcancelacion` date NOT NULL,
  `pagado` varchar(1) NOT NULL DEFAULT '1' COMMENT '1=pagado, 0 = no pagado',
  `mesa` varchar(3) NOT NULL,
  `reg` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_venta`),
  UNIQUE KEY `id_venta_UNIQUE` (`id_venta`),
  KEY `constraint_fk_30` (`id_cliente`),
  KEY `constraint_fk_31` (`id_personal`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ventas`
--

INSERT INTO `ventas` (`id_venta`, `id_personal`, `id_cliente`, `tipo_costo`, `metodo`, `subtotal`, `descuento`, `descuentocant`, `monto_total`, `pagotarjeta`, `efectivo`, `cancelado`, `hcancelacion`, `pagado`, `mesa`, `reg`) VALUES
(1, 1, 45, '0', 1, 70, 0, 0, 70, 0, 150, 0, '0000-00-00', '1', '', '2022-01-25 23:15:43'),
(2, 1, 45, '0', 1, 70, 0, 0, 70, 0, 200, 0, '0000-00-00', '1', '', '2022-01-25 23:28:36'),
(3, 1, 45, '0', 1, 50, 0, 0, 50, 0, 100, 0, '0000-00-00', '1', '', '2022-01-26 00:03:36'),
(4, 1, 45, '0', 1, 50, 0, 0, 50, 0, 100, 0, '0000-00-00', '1', '', '2022-01-26 00:16:43'),
(5, 1, 45, '0', 1, 50, 0, 0, 50, 0, 100, 0, '0000-00-00', '1', '', '2022-01-26 01:53:32'),
(6, 1, 45, '0', 1, 30, 0, 0, 30, 0, 50, 0, '0000-00-00', '1', '', '2022-01-26 16:06:36'),
(7, 1, 45, '0', 1, 20, 0, 0, 20, 0, 20, 0, '0000-00-00', '1', '', '2022-01-26 16:09:04'),
(8, 1, 45, '0', 1, 50, 0, 0, 50, 0, 50, 0, '0000-00-00', '1', '', '2022-01-26 16:13:19'),
(9, 1, 45, '0', 1, 50, 0, 0, 50, 0, 100, 0, '0000-00-00', '1', '', '2022-01-26 17:05:15'),
(10, 1, 45, '0', 1, 50, 0, 0, 50, 0, 200, 0, '0000-00-00', '1', '', '2022-01-26 17:07:11'),
(11, 1, 45, '0', 1, 50, 0, 0, 50, 0, 100, 0, '0000-00-00', '1', '', '2022-01-26 17:12:27'),
(12, 1, 45, '0', 1, 70, 0, 0, 70, 0, 100, 0, '0000-00-00', '1', '', '2022-02-02 06:00:00'),
(13, 1, 45, '0', 1, 70, 0, 0, 70, 0, 100, 0, '0000-00-00', '1', '', '2022-02-02 19:56:36'),
(14, 1, 45, '0', 1, 50, 0, 0, 50, 0, 100, 0, '0000-00-00', '1', '', '2022-02-02 20:01:56'),
(15, 1, 45, '0', 1, 50, 0, 0, 50, 0, 100, 0, '0000-00-00', '1', '', '2022-02-02 20:04:08'),
(16, 1, 45, '0', 1, 50, 0, 0, 50, 0, 50, 0, '0000-00-00', '1', '', '2022-02-02 20:12:02'),
(17, 1, 45, '0', 1, 50, 0, 0, 50, 0, 50, 0, '0000-00-00', '1', '', '2022-02-02 20:14:16'),
(18, 1, 45, '0', 1, 50, 0, 0, 50, 0, 100, 0, '0000-00-00', '1', '', '2022-02-02 20:14:45'),
(19, 1, 45, '0', 1, 50, 0, 0, 50, 0, 100, 0, '0000-00-00', '1', '', '2022-02-02 20:16:26'),
(20, 1, 45, '0', 1, 70, 0, 0, 70, 0, 100, 0, '0000-00-00', '1', '', '2022-02-02 20:17:43'),
(21, 1, 45, '0', 1, 50, 0, 0, 50, 0, 50, 0, '0000-00-00', '1', '', '2022-02-02 20:19:27'),
(22, 1, 45, '0', 1, 50, 0, 0, 50, 0, 50, 0, '0000-00-00', '1', '', '2022-02-02 20:22:44'),
(23, 1, 45, '0', 1, 50, 0, 0, 50, 0, 50, 0, '0000-00-00', '1', '', '2022-02-02 20:24:31'),
(24, 1, 45, '0', 1, 70, 0, 0, 70, 0, 100, 0, '0000-00-00', '1', '', '2022-02-02 20:24:56'),
(25, 1, 45, '0', 1, 70, 0, 0, 70, 0, 100, 0, '0000-00-00', '1', '', '2022-02-02 20:29:15'),
(26, 1, 45, '0', 1, 70, 0, 0, 70, 0, 100, 0, '0000-00-00', '1', '', '2022-02-02 20:31:07'),
(27, 1, 45, '0', 1, 70, 0, 0, 70, 0, 100, 0, '0000-00-00', '1', '', '2022-02-02 20:33:14'),
(28, 1, 45, '0', 1, 70, 0, 0, 70, 0, 100, 0, '0000-00-00', '1', '', '2022-02-02 20:36:06'),
(29, 1, 45, '0', 1, 70, 0, 0, 70, 0, 100, 0, '0000-00-00', '1', '', '2022-02-02 20:38:54'),
(30, 1, 45, '0', 1, 70, 0, 0, 70, 0, 100, 0, '0000-00-00', '1', '', '2022-02-02 20:40:21'),
(31, 1, 45, '0', 1, 50, 0, 0, 50, 0, 50, 0, '0000-00-00', '1', '', '2022-02-02 20:41:26'),
(32, 1, 45, '0', 1, 70, 0, 0, 70, 0, 200, 0, '0000-00-00', '1', '', '2022-02-02 20:42:20'),
(33, 1, 45, '0', 1, 50, 0, 0, 50, 0, 50, 0, '0000-00-00', '1', '', '2022-02-02 20:46:54'),
(34, 1, 45, '0', 1, 80, 0, 0, 80, 0, 80, 0, '0000-00-00', '1', '', '2022-02-02 20:51:08'),
(35, 1, 45, '0', 1, 80, 0, 0, 80, 0, 80, 0, '0000-00-00', '1', '', '2022-02-02 20:52:36'),
(36, 1, 45, '0', 1, 20, 0, 0, 20, 0, 50, 0, '0000-00-00', '1', '', '2022-03-14 16:20:42'),
(37, 1, 45, '0', 1, 50, 0, 0, 50, 0, 100, 0, '0000-00-00', '1', '', '2022-03-14 18:09:53'),
(38, 1, 45, '0', 1, 50, 0, 0, 50, 0, 50, 0, '0000-00-00', '1', '', '2022-03-14 18:14:17'),
(39, 1, 45, '0', 1, 50, 0, 0, 50, 0, 50, 0, '0000-00-00', '1', '', '2022-03-14 18:25:09'),
(40, 1, 45, '0', 1, 50, 0, 0, 50, 0, 50, 0, '0000-00-00', '1', '', '2022-03-14 18:27:16'),
(41, 1, 45, '0', 1, 50, 0, 0, 50, 0, 50, 0, '0000-00-00', '1', '', '2022-03-14 18:39:38'),
(42, 1, 45, '0', 1, 70, 0, 0, 70, 0, 100, 0, '0000-00-00', '1', '', '2022-03-14 22:12:31'),
(43, 1, 45, '0', 1, 20, 0, 0, 20, 0, 50, 0, '0000-00-00', '1', '', '2022-03-15 00:04:06'),
(44, 1, 45, '0', 1, 100, 0, 0, 100, 0, 100, 0, '0000-00-00', '1', '', '2022-03-15 01:05:32'),
(45, 1, 45, '0', 1, 100, 0, 0, 100, 0, 200, 0, '0000-00-00', '1', '', '2022-03-17 16:02:56'),
(46, 1, 45, '0', 1, 100, 0, 0, 100, 0, 200, 0, '0000-00-00', '1', '', '2022-03-17 16:06:24'),
(47, 1, 45, '0', 1, 140, 0, 0, 140, 0, 150, 0, '0000-00-00', '1', '', '2022-03-17 16:13:33'),
(48, 1, 45, '2', 1, 0, 0, 0, 0, 0, 0, 0, '0000-00-00', '1', '', '2022-03-18 23:08:35'),
(49, 1, 45, '2', 1, 25, 0, 0, 25, 0, 0, 0, '0000-00-00', '1', '', '2022-03-18 23:19:39'),
(50, 1, 45, '2', 1, 80, 0, 0, 80, 0, 0, 0, '0000-00-00', '0', '1', '2022-03-19 00:06:01'),
(51, 1, 45, '1', 1, 55, 0, 0, 55, 0, 55, 0, '0000-00-00', '1', '0', '2022-03-22 18:25:33'),
(52, 1, 46, '3', 1, 80, 0, 0, 80, 0, 80, 0, '0000-00-00', '0', '0', '2022-03-22 18:55:13'),
(53, 1, 45, '2', 1, 90, 0, 0, 90, 0, 100, 0, '0000-00-00', '1', '5', '2022-03-22 19:07:29'),
(54, 1, 45, '1', 1, 25, 0, 0, 25, 0, 50, 0, '0000-00-00', '1', '0', '2022-03-22 19:09:45'),
(55, 1, 45, '2', 1, 150, 0, 0, 150, 0, 200, 0, '0000-00-00', '1', '2', '2022-03-22 19:56:16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta_combo_prods`
--

DROP TABLE IF EXISTS `venta_combo_prods`;
CREATE TABLE IF NOT EXISTS `venta_combo_prods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_venta` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `reg` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `venta_combo_prods`
--

INSERT INTO `venta_combo_prods` (`id`, `id_venta`, `id_producto`, `reg`) VALUES
(2, 55, 24, '2022-03-22 13:56:16'),
(3, 55, 54, '2022-03-22 13:56:16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta_detalle`
--

DROP TABLE IF EXISTS `venta_detalle`;
CREATE TABLE IF NOT EXISTS `venta_detalle` (
  `id_detalle_venta` int(11) NOT NULL AUTO_INCREMENT,
  `id_venta` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` float NOT NULL,
  `precio` decimal(5,2) NOT NULL,
  PRIMARY KEY (`id_detalle_venta`),
  UNIQUE KEY `id_detalle_venta_UNIQUE` (`id_detalle_venta`),
  KEY `constraint_fk_15` (`id_producto`),
  KEY `constraint_fk_16` (`id_venta`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `venta_detalle`
--

INSERT INTO `venta_detalle` (`id_detalle_venta`, `id_venta`, `id_producto`, `cantidad`, `precio`) VALUES
(1, 1, 61, 1, '20.00'),
(2, 1, 114, 1, '50.00'),
(3, 2, 61, 1, '20.00'),
(4, 2, 114, 1, '50.00'),
(5, 3, 114, 1, '50.00'),
(6, 4, 114, 1, '50.00'),
(7, 4, 114, 1, '50.00'),
(8, 5, 61, 1, '50.00'),
(9, 5, 114, 1, '50.00'),
(10, 6, 103, 1, '30.00'),
(11, 7, 61, 1, '20.00'),
(12, 8, 61, 1, '50.00'),
(13, 8, 114, 1, '50.00'),
(14, 9, 114, 1, '50.00'),
(15, 10, 114, 1, '50.00'),
(16, 11, 61, 1, '20.00'),
(17, 11, 103, 1, '30.00'),
(18, 12, 61, 1, '20.00'),
(19, 12, 114, 1, '50.00'),
(20, 13, 61, 1, '20.00'),
(21, 13, 114, 1, '50.00'),
(22, 14, 115, 1, '50.00'),
(23, 14, 115, 1, '50.00'),
(24, 15, 115, 1, '50.00'),
(25, 15, 115, 1, '50.00'),
(26, 16, 115, 1, '50.00'),
(27, 16, 115, 1, '50.00'),
(28, 17, 115, 1, '50.00'),
(29, 19, 114, 1, '50.00'),
(30, 20, 114, 1, '50.00'),
(31, 20, 61, 1, '20.00'),
(32, 21, 115, 1, '50.00'),
(33, 21, 115, 1, '50.00'),
(34, 23, 114, 1, '50.00'),
(35, 24, 114, 1, '50.00'),
(36, 24, 61, 1, '20.00'),
(37, 25, 61, 1, '20.00'),
(38, 26, 61, 1, '20.00'),
(39, 27, 61, 1, '20.00'),
(40, 28, 61, 1, '20.00'),
(41, 29, 61, 1, '20.00'),
(42, 30, 114, 1, '50.00'),
(43, 30, 61, 1, '20.00'),
(44, 31, 115, 1, '50.00'),
(45, 32, 115, 1, '50.00'),
(46, 32, 61, 1, '20.00'),
(47, 33, 115, 1, '50.00'),
(48, 34, 116, 1, '60.00'),
(49, 34, 116, 1, '60.00'),
(50, 34, 61, 1, '20.00'),
(51, 35, 116, 1, '60.00'),
(52, 35, 61, 1, '20.00'),
(53, 36, 61, 1, '20.00'),
(54, 39, 117, 1, '50.00'),
(55, 40, 117, 1, '50.00'),
(56, 41, 117, 1, '50.00'),
(57, 42, 117, 1, '50.00'),
(58, 42, 61, 1, '20.00'),
(59, 43, 61, 1, '20.00'),
(60, 44, 117, 1, '50.00'),
(61, 44, 117, 1, '50.00'),
(62, 46, 117, 1, '50.00'),
(63, 46, 117, 1, '50.00'),
(64, 47, 117, 1, '50.00'),
(65, 47, 117, 1, '50.00'),
(66, 47, 61, 2, '20.00'),
(67, 49, 61, 1, '25.00'),
(68, 50, 61, 1, '25.00'),
(75, 51, 117, 1, '55.00'),
(76, 52, 117, 1, '55.00'),
(77, 52, 61, 1, '25.00'),
(78, 53, 117, 1, '55.00'),
(79, 53, 103, 1, '40.00'),
(80, 53, 61, 1, '25.00'),
(81, 54, 61, 1, '25.00'),
(82, 55, 117, 1, '55.00'),
(83, 55, 2, 1, '55.00'),
(84, 55, 61, 1, '25.00'),
(85, 55, 58, 1, '15.00'),
(86, 53, 61, 1, '25.00');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `compras`
--
ALTER TABLE `compras`
  ADD CONSTRAINT `fk_compra_proveedor` FOREIGN KEY (`id_proveedor`) REFERENCES `proveedores` (`id_proveedor`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `compra_detalle`
--
ALTER TABLE `compra_detalle`
  ADD CONSTRAINT `fk_compra_compra` FOREIGN KEY (`id_compra`) REFERENCES `compras` (`id_compra`);

--
-- Filtros para la tabla `menu_sub`
--
ALTER TABLE `menu_sub`
  ADD CONSTRAINT `fk_menu_sub_menu` FOREIGN KEY (`MenuId`) REFERENCES `menu` (`MenuId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `perfiles_detalles`
--
ALTER TABLE `perfiles_detalles`
  ADD CONSTRAINT `fk_Perfiles_detalles_Perfiles1` FOREIGN KEY (`perfilId`) REFERENCES `perfiles` (`perfilId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Perfiles_detalles_menu_sub1` FOREIGN KEY (`MenusubId`) REFERENCES `menu_sub` (`MenusubId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `personal_menu`
--
ALTER TABLE `personal_menu`
  ADD CONSTRAINT `personal_fkmenu` FOREIGN KEY (`MenuId`) REFERENCES `menu_sub` (`MenusubId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `personal_fkpersona` FOREIGN KEY (`personalId`) REFERENCES `personal` (`personalId`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `productos_insumos`
--
ALTER TABLE `productos_insumos`
  ADD CONSTRAINT `insumo_fk_insumo` FOREIGN KEY (`insumo`) REFERENCES `insumos` (`insumosId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `insumo_fk_producto` FOREIGN KEY (`productoId`) REFERENCES `productos` (`productoid`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `fk_usuarios_Perfiles1` FOREIGN KEY (`perfilId`) REFERENCES `perfiles` (`perfilId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`personalId`) REFERENCES `personal` (`personalId`);

--
-- Filtros para la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD CONSTRAINT `fk_venta_cliente` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`ClientesId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_venta_presonal` FOREIGN KEY (`id_personal`) REFERENCES `personal` (`personalId`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `venta_detalle`
--
ALTER TABLE `venta_detalle`
  ADD CONSTRAINT `fk_detalle_producto` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`productoid`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_detalle_venta` FOREIGN KEY (`id_venta`) REFERENCES `ventas` (`id_venta`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
