<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloProductos extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function filas() {
        $strq = "SELECT COUNT(*) as total FROM productos where activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function total_paginados($por_pagina,$segmento) {
        //$consulta = $this->db->get('productos',$por_pagina,$segmento);
        //return $consulta;
        if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }
        $strq = "SELECT pro.productoid,pro.nombre,pro.img,pro.codigo,pro.descripcion,cat.categoria,pro.precioventa,pro.stock FROM productos as pro
        inner join categoria as cat on cat.categoriaId=pro.categoria 
        where pro.activo=1 LIMIT $por_pagina $segmento";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
//======
    function productosall() {
        $strq = "SELECT pro.productoid,pro.nombre,pro.img,pro.codigo,pro.descripcion,cat.categoria,pro.precioventa,pro.stock FROM productos as pro
        inner join categoria as cat on cat.categoriaId=pro.categoria 
        where pro.activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function total_paginadosp($por_pagina,$segmento) {
        //$consulta = $this->db->get('productos',$por_pagina,$segmento);
        //return $consulta;
        if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }
        $strq = "SELECT pro.productoid,pro.nombre,pro.img,pro.codigo FROM productos as pro
        inner join categoria as cat on cat.categoriaId=pro.categoria 
        where pro.activo=1 LIMIT $por_pagina $segmento";
        return $strq;
    }
    function productoallsearch($pro){
        $strq = "SELECT * FROM productos where activo=1 and codigo like '%".$pro."%' or activo=1 and nombre like '%".$pro."%'";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    /*
    function categorias() {
        $strq = "SELECT * FROM categoria where activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    *
    public function productosinsert($cod,$pfiscal,$nom,$des,$catego,$stock,$preciocompra,$porcentaje,$precioventa,$pmmayoreo,$cpmmayoreo,$pmayoreo,$cpmayoreo,$stockmin){
            $strq = "INSERT INTO productos(codigo, productofiscal, nombre, descripcion, categoria, stock, preciocompra, porcentaje, precioventa, mediomayoreo, canmediomayoreo, mayoreo, canmayoreo,stockmin) 
                                   VALUES ('$cod',$pfiscal,'$nom','$des',$catego,$stock,'$preciocompra',$porcentaje,$precioventa,$pmmayoreo,$cpmmayoreo,$pmayoreo,$cpmayoreo,$stockmin)";
            $this->db->query($strq);
            $id=$this->db->insert_id();
            return $id;
    }
    public function productosupdate($id,$cod,$pfiscal,$nom,$des,$catego,$stock,$preciocompra,$porcentaje,$precioventa,$pmmayoreo,$cpmmayoreo,$pmayoreo,$cpmayoreo,$stockmin){
            $strq = "UPDATE productos SET codigo='$cod',productofiscal=$pfiscal,nombre='$nom',descripcion='$des',categoria=$catego,stock=$stock,preciocompra=$preciocompra,porcentaje=$porcentaje,precioventa=$precioventa,mediomayoreo=$pmmayoreo,canmediomayoreo=$cpmmayoreo,mayoreo=$pmayoreo,canmayoreo=$cpmayoreo,stockmin=$stockmin WHERE productoid=$id";
            $this->db->query($strq);
    }*/
    public function imgpro($file,$pro){
        $strq = "UPDATE productos SET img='$file' WHERE productoid=$pro";
        $this->db->query($strq);
    }
    function totalproductosenexistencia() {
        $strq = "SELECT ROUND(sum(stock),2) as total FROM `productos` where activo=1 ";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function totalproductopreciocompra() {
        $strq = "SELECT ROUND(sum(preciocompra),2) as total FROM `productos` where activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function totalproductoporpreciocompra(){
        $strq = "SELECT ROUND(sum(preciocompra*stock),2) as total FROM `productos` where activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function getproducto($id){
        $strq = "SELECT * FROM productos where productoid=$id";
        $query = $this->db->query($strq);
        return $query;
    }
    function productosdelete($id){
        $strq = "UPDATE productos SET activo=0 WHERE productoid=$id";
        $this->db->query($strq);
    }
//=====
    function getlistproductos($params){
        $columns = array(
            0=>'pro.productoid',
            1=>'pro.codigo',
            2=>'pro.nombre',
            3=>'pro.preciopos',
            4=>'pro.preciocompra',
            5=>'pro.stock'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('productos pro');
        $this->db->where(array('pro.activo'=>1));
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    function getlistproductost($params){
        $columns = array(
            0=>'pro.productoid',
            1=>'pro.codigo',
            2=>'pro.nombre',
            3=>'pro.preciopos',
            4=>'pro.preciocompra',
            5=>'pro.stock'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('productos pro');
        $this->db->where(array('pro.activo'=>1));

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                    $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $query=$this->db->get();
        return $query->row()->total;
    }
    function insumossearch($usu){
        $strq = "SELECT * FROM insumos where activo=1 and insumo like '%".$usu."%' ORDER BY insumo ASC";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    function procutossearch($pro){
        $strq = "SELECT * FROM productos where activo=1 and codigo like '%".$pro."%' or activo=1 and nombre like '%".$pro."%' ORDER BY codigo ASC";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    public function getCategosProdsCombo($id,$tipo){
        $this->db->select("pc.*, c.categoria");
        $this->db->from("productos_categos_combos pc");
        $this->db->join("categoria c","c.categoriaId=pc.id_categoria");
        $this->db->where("pc.id_producto",$id);
        $this->db->where("c.activo",1);
        $this->db->where("pc.estatus",1);
        $this->db->where("pc.tipo",$tipo);
        $this->db->order_by("pc.id_categoria","asc");
        $query=$this->db->get();
        return $query->result();
    }

    public function getProdsCombo($id){
        $this->db->select("c.categoria, pc.id_categoria, pc.cantidad, pc.tipo, p.nombre,p.codigo,pc.id_producto");
        $this->db->from("productos p");
        $this->db->join("productos_categos_combos pc","pc.id_producto=p.productoid");
        $this->db->join("categoria c","c.categoriaId=pc.id_categoria");
        $this->db->where("pc.id_categoria",$id);
        $this->db->where("pc.estatus",1);
        $this->db->where("pc.tipo",0);
        $this->db->order_by("pc.id_categoria","asc");
        $query=$this->db->get();
        return $query->result();
    }

}