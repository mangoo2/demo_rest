<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloVentas extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function ingresarventa($uss,$cli,$tipoc,$mpago,$sbtotal,$desc,$descu,$total,$efectivo,$tarjeta,$fecha,$pagado,$mesa){
    	$strq = "INSERT INTO ventas(id_personal, id_cliente, tipo_costo, metodo, subtotal, descuento,descuentocant, monto_total,pagotarjeta,efectivo,reg,pagado,mesa) 
                VALUES ($uss,$cli,$tipoc,$mpago,$sbtotal,$desc,$descu,$total,'$tarjeta','$efectivo','$fecha',$pagado,$mesa)";
        $query = $this->db->query($strq);
        $id=$this->db->insert_id();
        //$this->db->close();
        return $id;
    }
    function ingresarventad($idventa,$producto,$cantidad,$precio){
    	$strq = "INSERT INTO venta_detalle(id_venta, id_producto, cantidad, precio) VALUES ($idventa,$producto,$cantidad,$precio)";
        $query = $this->db->query($strq);
        //$this->db->close();

        $strq = "UPDATE productos SET stock=stock-$cantidad WHERE productoid=$producto";
        $query = $this->db->query($strq);
        //$this->db->close();
    }

    function ingresarventadCombo($producto,$cantidad){
        $strq = "UPDATE productos SET stock=stock-$cantidad WHERE productoid=$producto";
        $query = $this->db->query($strq);
        //$this->db->close();
    }

    function ingresarventadReceta($idventa,$producto,$cantidad,$precio){
        $strq = "INSERT INTO venta_detalle(id_venta, id_producto, cantidad, precio) VALUES ($idventa,$producto,$cantidad,$precio)";
        $query = $this->db->query($strq);
    }

    function editaStockReceta($ins,$cant){
        $strq = "UPDATE insumos SET existencia=existencia-$cant WHERE insumosId=$ins";
        $query = $this->db->query($strq);
    }

    function configticket(){
        $strq = "SELECT * FROM ticket";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function clientepordefecto(){
        $strq = "SELECT * FROM  clientes LIMIT 1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function getventas($id){
        $strq = "SELECT v.*, concat('Calle: ',c.Calle,' ',c.noExterior,', # int. ',c.noInterior,', Col. ',c.Colonia,', ',c.Municipio,' ',c.Estado) as direcc
        FROM ventas v
        left join clientes c on c.ClientesId=v.id_cliente
        where id_venta=$id";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    function getventasd($id){
        $strq = "SELECT vendell.cantidad, pro.nombre, vendell.precio
        FROM venta_detalle as vendell
        inner join productos as pro on pro.productoid=vendell.id_producto
        where vendell.id_venta=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function ingresarcompra($uss,$prov,$total){
        $strq = "INSERT INTO compras(id_proveedor, monto_total) VALUES ($prov,$total)";
        $query = $this->db->query($strq);
        $id=$this->db->insert_id();
        $this->db->close();
        return $id;
    }
    function ingresarcomprad($idcompra,$producto,$cantidad,$precio){
        $strq = "INSERT INTO compra_detalle(id_compra, id_producto, cantidad, precio_compra) VALUES ($idcompra,$producto,$cantidad,$precio)";
        $query = $this->db->query($strq);
        $this->db->close();

        $strq = "UPDATE productos SET stock=stock+$cantidad WHERE productoid=$producto";
        $query = $this->db->query($strq);
        $this->db->close();
    }

    function ingresarcompradIns($idcompra,$producto,$cantidad,$precio,$tipo){
        $strq = "INSERT INTO compra_detalle(id_compra, id_producto, cantidad, precio_compra, tipo_prod) VALUES ($idcompra,$producto,$cantidad,$precio,$tipo)";
        $query = $this->db->query($strq);
    }

    function ingresarStockInsumo($ins,$cant){
        $strq = "UPDATE insumos SET existencia=existencia+$cant WHERE insumosId=$ins";
        $query = $this->db->query($strq);
        $this->db->close();
    }
    function turnos(){
        $strq = "SELECT * FROM turno ORDER BY id DESC LIMIT 1";
        $query = $this->db->query($strq);
        $this->db->close();
        $status='cerrado';
        foreach ($query->result() as $row) {
            $status =$row->status;
        }
        return $status;
    }
    function turnoss(){
        $strq = "SELECT * FROM turno ORDER BY id DESC LIMIT 1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function abrirturno($cantidad,$nombre,$fecha,$horaa){
        $strq = "INSERT INTO turno(fecha, horaa, cantidad, nombre, status, user) VALUES ('$fecha','$horaa','$cantidad','$nombre','abierto','user')";
        $query = $this->db->query($strq);
        $this->db->close();
    }
    function cerrarturno($id,$horaa){
        $fechac=date('Y-m-d');
        $strq = "UPDATE turno SET horac='$horaa',fechacierre='$fechac', status='cerrado' WHERE id=$id";
        $query = $this->db->query($strq);
        $this->db->close();
    }
    function corte($inicio,$fin,$tipo){
        $where = "";
        if($tipo!=4){
            $where="and tipo_costo=$tipo";
        }

        $strq = "SELECT cl.ClientesId,cl.Nom, v.reg, v.id_venta, v.id_cliente, v.id_personal, v.monto_total,v.pagotarjeta,v.efectivo, v.subtotal, p.personalId, concat(p.nombre,p.apellidos) as vendedor, tipo_costo, prod.preciocompra, vd.precio, prod.preciocomp_uber, prod.preciocomp_rappi, prod.preciocomp_didi, v.metodo, vd.cantidad
                FROM ventas as v 
                left join personal as p on v.id_personal=p.personalId 
                join venta_detalle as vd on vd.id_venta=v.id_venta
                join productos as prod on prod.productoid=vd.id_producto
                join clientes as cl on cl.ClientesId=v.id_cliente
                WHERE v.reg between '$inicio 00:00:00' and '$fin 23:59:59' and v.cancelado=0 
                and pagado=1 $where
                group by v.id_venta";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function consultarturnoname($inicio,$fin){
        $strq = "SELECT * FROM turno where fecha>='$inicio' and fechacierre<='$fin'";
        $query = $this->db->query($strq);
        $this->db->close();
        //$nombre=$cfecha.'/'.$chora;
        return $query;
    }
    function cortesum($inicio,$fin,$tipo){
        $where = "";
        if($tipo!=4){
            $where="and tipo_costo=$tipo";
        }

        $strq = "SELECT sum(monto_total) as total , sum(subtotal) as subtotal, sum(descuentocant) as descuento 
                FROM ventas 
                where reg between '$inicio 00:00:00' and '$fin 23:59:59' and cancelado=0 and pagado=1
                $where";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function filas($tipo) {
        $strq = "SELECT COUNT(*) as total FROM ventas where tipo_costo='$tipo'";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function total_paginados($por_pagina,$segmento,$tipo) {
        //$consulta = $this->db->get('productos',$por_pagina,$segmento);
        //return $consulta;
        if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }
        $strq = "SELECT ven.id_venta,ven.reg, cli.Nom as cliente, concat(per.nombre,' ',per.apellidos) as vendedor,ven.metodo,ven.subtotal, ven.monto_total,ven.cancelado, ven.tipo_costo, ven.pagado
                FROM ventas as ven 
                inner join personal as per on per.personalId=ven.id_personal
                inner join clientes as cli on cli.ClientesId=ven.id_cliente
                where tipo_costo = '$tipo'
                ORDER BY ven.id_venta DESC
                /*LIMIT $por_pagina $segmento*/ ";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function filastur() {
        $strq = "SELECT COUNT(*) as total FROM turno";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function cancalarventa($id){
        $fecha = date('Y-m-d');
        $strq = "UPDATE ventas SET cancelado=1,hcancelacion='$fecha' WHERE `id_venta`=$id";
        $query = $this->db->query($strq);
        $this->db->close();
    }
    function ventadetalles($id){
        $strq = "SELECT * FROM venta_detalle where id_venta=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function regresarpro($id,$can){
        $strq = "UPDATE productos set stock=stock+$can where productoid=$id";
        $query = $this->db->query($strq);
        $this->db->close();
    }
    function filasturnos() {
        $strq = "SELECT COUNT(*) as total FROM turno";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function total_paginadosturnos($por_pagina,$segmento) {
        //$consulta = $this->db->get('productos',$por_pagina,$segmento);
        //return $consulta;
        if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }
        $strq = "SELECT * FROM turno ORDER BY id DESC
                LIMIT $por_pagina $segmento";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function consultarturno($id){
        $strq = "SELECT * FROM turno where id=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function consultartotalturno($fecha,$horaa,$horac,$fechac){
        $strq = "SELECT sum(monto_total) as total FROM ventas where reg between '$fecha $horaa' and '$fechac $horac' and cancelado=0";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        }
        return $total;
    }
    function consultartotalturno2($fecha,$horaa,$horac,$fechac){
        $strq = "SELECT sum(p.preciocompra*vd.cantidad) as preciocompra 
                from venta_detalle as vd
                inner join productos as p on vd.id_producto=p.productoid
                inner join ventas as v on vd.id_venta=v.id_venta
                where v.cancelado=0 and v.reg>='$fecha $horaa' and v.reg<='$fechac $horac'";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->preciocompra;
        }
        return $total;
    }
    function consultartotalturnopro($fecha,$horaa,$horac,$fechac){
        $strq = "SELECT p.nombre as producto,vd.cantidad, vd.precio
                from venta_detalle as vd
                inner join productos as p on vd.id_producto=p.productoid
                inner join ventas as v on vd.id_venta=v.id_venta
                where v.cancelado=0 and v.reg>='$fecha $horaa' and v.reg<='$fechac $horac'";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function consultartotalturnopromas($fecha,$horaa,$horac,$fechac){
        $strq = "SELECT p.nombre as producto, sum(vd.cantidad) as total 
                from venta_detalle as vd 
                inner join productos as p on vd.id_producto=p.productoid 
                inner join ventas as v on vd.id_venta=v.id_venta 
                where v.cancelado=0 and v.reg>='$fecha $horaa' and v.reg<='$fechac $horac' GROUP BY producto ORDER BY `total` DESC LIMIT 1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function filaslcompras(){
        $strq = "SELECT COUNT(*) as total FROM compra_detalle";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function total_paginadoslcompras($por_pagina,$segmento) {
        //$consulta = $this->db->get('productos',$por_pagina,$segmento);
        //return $consulta;
        if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }
        /*$prestrq = "SELECT compdll.id_detalle_compra,compdll.cantidad,compdll.precio_compra,compdll.tipo_prod
                FROM compra_detalle as compdll 
                ORDER BY compdll.id_detalle_compra DESC
                LIMIT $por_pagina $segmento";
        $prequery = $this->db->query($prestrq);

        foreach ($prequery->result() as $key) {
            $tipo_prod = $key->tipo_prod;
            
            
            if($tipo_prod==1){
                $prod = "pro.nombre as producto";
                $join = "inner join productos as pro on pro.productoid=compdll.id_producto";
            }if($tipo_prod==0){
                $prod = "ins.insumo as producto";
                $join = "inner join insumos as ins on ins.insumosId=compdll.id_producto";
            }
            log_message('error', 'tipo_prod: '.$tipo_prod);
            $strq = "SELECT compdll.id_detalle_compra,comp.reg, ".$prod.", prov.razon_social,compdll.cantidad,compdll.precio_compra 
                    FROM compra_detalle as compdll 
                    inner join compras as comp on comp.id_compra=compdll.id_compra 
                    ".$join." 
                    inner join proveedores as prov on prov.id_proveedor=comp.id_proveedor 
                    ORDER BY compdll.id_detalle_compra DESC
                    LIMIT $por_pagina $segmento";
            
        }*/

        $strq = "SELECT compdll.id_detalle_compra,compdll.id_detalle_compra as id_dcp, comp.reg, pro.nombre as producto, prov.razon_social,compdll.cantidad,compdll.precio_compra 
                    FROM compra_detalle as compdll 
                    inner join compras as comp on comp.id_compra=compdll.id_compra 
                    inner join productos as pro on pro.productoid=compdll.id_producto
                    inner join proveedores as prov on prov.id_proveedor=comp.id_proveedor 
                    where tipo_prod=1

                    union 
                    SELECT compdll2.id_detalle_compra,compdll2.id_detalle_compra as id_dci, comp.reg, ins.insumo as producto, prov.razon_social,compdll2.cantidad,compdll2.precio_compra 
                    FROM compra_detalle as compdll2 
                    inner join compras as comp on comp.id_compra=compdll2.id_compra 
                    inner join insumos as ins on ins.insumosId=compdll2.id_producto 
                    inner join proveedores as prov on prov.id_proveedor=comp.id_proveedor 
                    where tipo_prod=0
                    ORDER BY id_detalle_compra DESC
                    LIMIT $por_pagina $segmento";
        $query = $this->db->query($strq);
        return $query;
    }
    function lcomprasconsultar($inicio,$fin) {
        $prestrq = "SELECT compdll.id_detalle_compra,compdll.cantidad,compdll.precio_compra,compdll.tipo_prod
                FROM compra_detalle as compdll 
                ORDER BY compdll.id_detalle_compra DESC
                LIMIT $por_pagina $segmento";
        $prequery = $this->db->query($prestrq);

        foreach ($prequery as $key) {
            $tipo_prod = $key->tipo_prod;
        }
        if($tipo_prod==1){
            $prod = "pro.nombre as producto";
            $join = "inner join productos as pro on pro.productoid=compdll.id_producto";
        }else{
            $prod = "ins.insumo as producto";
            $join = "inner join insumos as ins on ins.insumosId=compdll.id_producto";
        }

        $strq = "SELECT compdll.id_detalle_compra,comp.reg,$pro,prov.razon_social,compdll.cantidad,compdll.precio_compra 
                FROM compra_detalle as compdll 
                inner join compras as comp on comp.id_compra=compdll.id_compra 
                $join
                inner join proveedores as prov on prov.id_proveedor=comp.id_proveedor 
                where comp.reg>='$inicio 00:00:00' and comp.reg<='$fin 23:59:59'
                ORDER BY compdll.id_detalle_compra DESC";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function ventassearch($search){
        $strq = "SELECT ven.id_venta,ven.reg, cli.Nom as cliente, concat(per.nombre,' ',per.apellidos) as vendedor,ven.metodo,ven.subtotal, ven.monto_total,ven.cancelado
                FROM ventas as ven 
                inner join personal as per on per.personalId=ven.id_personal
                inner join clientes as cli on cli.ClientesId=ven.id_cliente
                where ven.id_venta like '%".$search."%' or 
                      ven.reg like '%".$search."%' or
                      cli.Nom like '%".$search."%' or
                      per.nombre like '%".$search."%' or
                      ven.monto_total like '%".$search."%'
                ";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    public function getProdsVentaComanda($id,$tipo){
        $this->db->select("v.mesa, vd.id_detalle_venta, vd.id_producto, vd.cantidad, vd.precio, p.codigo, p.nombre, p.tipo");
        $this->db->from("ventas v");
        $this->db->join("venta_detalle vd","vd.id_venta=v.id_venta");
        $this->db->join("productos p","p.productoid=vd.id_producto");
        $this->db->where("v.id_venta",$id);
        $this->db->where("p.tipo",$tipo);
        $query=$this->db->get();
        return $query->result();
    }

    public function getProdsVentaComandaCombo($id){
        $this->db->select("v.mesa, vd.id_producto, vd.cantidad, vd.precio, p.codigo, p.nombre, p.tipo");
        $this->db->from("ventas v");
        $this->db->join("venta_combo_prods vc","vc.id_venta=v.id_venta");
        $this->db->join("venta_detalle vd","vd.id_venta=v.id_venta");
        $this->db->join("productos p","p.productoid=vc.id_producto");
        $this->db->where("v.id_venta",$id);
        $this->db->group_by("vc.id_producto");

        $query=$this->db->get();
        return $query->result();
    }
    
}