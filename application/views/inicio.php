<!--<div class="row">
    <div class="col-md-12">
      
    </div>

    <div class="col-xl-3 col-lg-6 col-md-6 col-12">
        <div class="card gradient-blackberry">
            <div class="card-body">
                <div class="card-block pt-2 pb-0">
                    <div class="media">
                        <div class="media-body white text-left">
                            <h3 class="font-large-1 mb-0"><?php echo number_format($total_rows,0,'.',',');?></h3>
                            <span>Productos registrados</span>
                        </div>
                        <div class="media-right white text-right">
                            <i class="icon-pie-chart font-large-1"></i>
                        </div>
                    </div>
                </div>
                <div id="Widget-line-chart" class="height-75 WidgetlineChart WidgetlineChartshadow mb-2">                   
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-lg-6 col-md-6 col-12">
        <div class="card gradient-ibiza-sunset">
            <div class="card-body">
                <div class="card-block pt-2 pb-0">
                    <div class="media">
                        <div class="media-body white text-left">
                            <h3 class="font-large-1 mb-0"><?php echo number_format($totalexistencia,0,'.',',');?></h3>
                            <span>Productos en existencia</span>
                        </div>
                        <div class="media-right white text-right">
                            <i class="icon-bulb font-large-1"></i>
                        </div>
                    </div>
                </div>
                <div id="Widget-line-chart1" class="height-75 WidgetlineChart WidgetlineChartshadow mb-2">                  
                </div>

            </div>
        </div>
    </div>

    <div class="col-xl-3 col-lg-6 col-md-6 col-12">
        <div class="card gradient-green-tea">
            <div class="card-body">
                <div class="card-block pt-2 pb-0">
                    <div class="media">
                        <div class="media-body white text-left">
                            <h3 class="font-large-1 mb-0">$ <?php echo number_format($totalproductopreciocompra,2,'.',',');?></h3>
                            <span>Total de Precios unitarios</span>
                        </div>
                        <div class="media-right white text-right">
                            <i class="icon-graph font-large-1"></i>
                        </div>
                    </div>
                </div>
                <div id="Widget-line-chart2" class="height-75 WidgetlineChart WidgetlineChartshadow mb-2">              
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-lg-6 col-md-6 col-12">
        <div class="card gradient-pomegranate">
            <div class="card-body">
                <div class="card-block pt-2 pb-0">
                    <div class="media">
                        <div class="media-body white text-left">
                            <h3 class="font-large-1 mb-0">$ <?php echo number_format($totalproductoporpreciocompra,2,'.',',');?></h3>
                            <span>Total de Precios</span>
                        </div>
                        <div class="media-right white text-right">
                            <i class="icon-wallet font-large-1"></i>
                        </div>
                    </div>
                </div>
                <div id="Widget-line-chart3" class="height-75 WidgetlineChart WidgetlineChartshadow mb-2">                  
                </div>
            </div>
        </div>
    </div>
</div>-->
  <!--Statistics cards Ends-->

<div class="row">
    <div class="col-md-12">
      
    </div>

    <div class="col-xl-3 col-lg-6 col-md-6 col-12">
        <div class="card gradient-brady-brady-fun-fun">
            <a href="<?php echo base_url() ?>Productos">
                <div class="card-body">
                    <div class="card-block pt-2 pb-0">
                        <div class="media">
                            <div class="media-body white text-left">
                                <h3 class="font-large-1 mb-0">Productos</h3>
                                <span>Catálogo de Productos</span>
                            </div>
                            <div class="media-right white text-right">
                                <i class="icon-diamond font-large-1"></i>
                            </div>
                        </div>
                    </div>
                    <div id="Widget-line-chart" class="height-75 WidgetlineChart WidgetlineChartshadow mb-2">                   
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-xl-3 col-lg-6 col-md-6 col-12">
        <div class="card gradient-brady-brady-fun-fun">
            <a href="<?php echo base_url() ?>Clientes">
                <div class="card-body">
                    <div class="card-block pt-2 pb-0">
                        <div class="media">
                            <div class="media-body white text-left">
                                <h3 class="font-large-1 mb-0">Clientes</h3>
                                <span>Catálogo de Clientes</span>
                            </div>
                            <div class="media-right white text-right">
                                <i class="icon-user font-large-1"></i>
                            </div>
                        </div>
                    </div>
                    <div id="Widget-line-chart1" class="height-75 WidgetlineChart WidgetlineChartshadow mb-2">                  
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-xl-3 col-lg-6 col-md-6 col-12">
        <div class="card gradient-brady-brady-fun-fun">
            <a href="<?php echo base_url() ?>Ventas">
                <div class="card-body">
                    <div class="card-block pt-2 pb-0">
                        <div class="media">
                            <div class="media-body white text-left">
                                <h3 class="font-large-1 mb-0">Ventas</h3>
                                <span>Generar una Venta</span>
                            </div>
                            <div class="media-right white text-right">
                                <i class="icon-wallet font-large-1"></i>
                            </div>
                        </div>
                    </div>
                    <div id="Widget-line-chart2" class="height-75 WidgetlineChart WidgetlineChartshadow mb-2">              
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-xl-3 col-lg-6 col-md-6 col-12">
        <div class="card gradient-brady-brady-fun-fun">
            <a href="<?php echo base_url() ?>ListaVentas">
                <div class="card-body">
                    <div class="card-block pt-2 pb-0">
                        <div class="media">
                            <div class="media-body white text-left">
                                <h3 class="font-large-1 mb-0">Lista de Ventas</h3>
                                <span>Ventas Generadas</span>
                            </div>
                            <div class="media-right white text-right">
                                <i class="icon-graph font-large-1"></i>
                            </div>
                        </div>
                    </div>
                    <div id="Widget-line-chart2" class="height-75 WidgetlineChart WidgetlineChartshadow mb-2">              
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-xl-3 col-lg-6 col-md-6 col-12">
        <div class="card gradient-brady-brady-fun-fun">
            <a href="<?php echo base_url() ?>Corte_caja">
                <div class="card-body">
                    <div class="card-block pt-2 pb-0">
                        <div class="media">
                            <div class="media-body white text-left">
                                <h3 class="font-large-1 mb-0">Corte de caja</h3>
                                <span>Generar Corte de caja</span>
                            </div>
                            <div class="media-right white text-right">
                                <i class="icon-calculator font-large-1"></i>
                            </div>
                        </div>
                    </div>
                    <div id="Widget-line-chart3" class="height-75 WidgetlineChart WidgetlineChartshadow mb-2">                  
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>

  <!--Line with Area Chart 1 Starts-->
<div class="row">
    <div class="col-sm-12">
        <!--<div class="card">
            <div class="card-header">
                <h4 class="card-title">PRODUCTS SALES</h4>
            </div>
            <div class="card-body">
                <div class="card-block">
                    
                </div>
            </div>
        </div>-->
    </div>
</div>
