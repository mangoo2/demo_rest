<?php 
if (isset($_GET['id'])) {
    $id=$_GET['id'];
    $personalv=$this->ModeloPersonal->personalview($id);
    $menusseleccionados=$this->ModeloPersonal->getMenusPerfil($id);
    foreach ($personalv->result() as $item){
      $label='Editar Personal';
      $id=$item->personalId;
      $nombre = $item->nombre;
      $apellidos = $item->apellidos;
      $fechanacimiento = $item->fechanacimiento;
      $sexo = $item->sexo;
      $domicilio = $item->domicilio;
      $ciudad = $item->ciudad;
      $estado = $item->estado;
      $codigopostal = $item->codigopostal;
      $telefono = $item->telefono;
      $celular = $item->celular;
      $correo = $item->correo;
      $turno = $item->turno;
      $fechaingreso = $item->fechaingreso;
      $fechabaja = $item->fechabaja;
      $sueldo = $item->sueldo;
      $mesuselect=1; 
    }
    $personalvusu=$this->ModeloPersonal->personalusuview($id);
    foreach ($personalvusu->result() as $item){
        $susuario=$item->Usuario;
        $spass='xxxxxx';
    }
}else{
  $label='Nuevo Personal';
  $id='';
  $nombre ='';
  $apellidos ='';
  $fechanacimiento ='';
  $sexo ='';
  $domicilio ='';
  $ciudad ='';
  $estado ='';
  $codigopostal ='';
  $telefono ='';
  $celular ='';
  $correo ='';
  $turno ='';
  $fechaingreso ='';
  $fechabaja ='';
  $sueldo ='';
  $mesuselect=0; 
  $susuario='';
  $spass='';    
} 
?>
<!-------------------------------------------------------------------------->
<style type="text/css">
  .content-list .list-wrapper > li {
    padding-top: 5px;
    padding-bottom: 5px;
    list-style: none;
    border-bottom: 1px solid #EEE;
    position: relative;
    text-align: left;
  }
  .vd_red{
    font-weight: bold;
    color: red;
    margin-bottom: 5px;
  }
  .vd_green{
    color: #009688; 
  }
  input,select,textarea{
    margin-bottom: 15px;
  }
</style>
<div class="row">
                <div class="col-md-12">
                  <h2>Personal</h2>
                </div>
              </div>
              <!--Statistics cards Ends-->
              <!--Line with Area Chart 1 Starts-->
              <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Personal</h4>
                        </div>
                        <div class="card-body">
                            <div class="card-block form-horizontal">
                                <!--------//////////////-------->
                                <div class="row">
                                  <form method="post"  role="form" id="formpersonal">
                                    <div class="col-md-12">
                                      <h3>Datos personales</h3>
                                      <hr />
                                    </div>
                                    <div class="col-md-12">
                                      <div class="form-group">
                                        <label class="col-sm-3 control-label">Nombre del empleado:</label>
                                        <div class="col-sm-8 controls">
                                            <input type="text" name="txtNombre" class="form-control" id="txtNombre" value="<?php echo $nombre;?>" >
                                            <input type="text" placeholder="Username" id="persolnalid" value="<?php echo $id; ?>" hidden>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-12">
                                      <div class="form-group">
                                        <label class="col-sm-3 control-label">Apellidos:</label>
                                        <div class="col-sm-8 controls">
                                            <input type="text" name="txtApellidos" class="form-control" id="txtApellidos" value="<?php echo $apellidos;?>" >
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-12">
                                      <div class="form-group">
                                              <label class="col-sm-3 control-label">Fecha de nacimiento:</label>
                                                <div class="col-sm-3 controls">
                                                    <input type="date" id="txtFN" class="form-control" name="txtFN" value="<?php echo $fechanacimiento;?>" />
                                                </div>
                                                <label class="col-sm-2 control-label">Género:</label>
                                                <div class="col-sm-3 controls">
                                                    <select id="cmbGenero" name="cmbGenero" class="form-control">
                                                        <option value="1" <?php if($sexo==1){echo "selected";} ?>>Hombre</option>
                                                        <option value="2" <?php if($sexo==2){echo "selected";} ?>>Mujer</option>
                                                        
                                                    </select>
                                                </div>
                                          </div>
                                    </div>
                                    <div class="col-md-12">
                                      <h3>Datos de contacto</h3>
                                      <hr />
                                    </div>
                                    <div class="col-md-12">
                                      <div class="form-group">
                                          <label class="col-sm-3 control-label">Domicilio:</label>
                                          <div class="col-sm-8 controls">
                                              <input type="text" name="txtDomicilio" id="txtDomicilio" class="form-control" value="<?php echo $domicilio;?>">
                                          </div>
                                      </div>
                                    </div>
                                    <div class="col-md-12">
                                      <div class="form-group">
                                          <label class="col-sm-3 control-label">Ciudad:</label>
                                          <div class="col-sm-3 controls">
                                              <input type="text" id="txtCiudad" name="txtCiudad" class="form-control" value="<?php echo $ciudad;?>" />
                                          </div>
                                          <label class="col-sm-2 control-label">Estado:</label>
                                          <div class="col-sm-3 controls">
                                              <select id="cmbEstado" name="cmbEstado" class="form-control">
                                                <?php foreach ($estadosp->result() as $item){ ?>
                                                  <option value="<?php echo $item->EstadoId; ?>" <?php if($estado == $item->EstadoId){ echo "SELECTED"; }?> ><?php echo $item->Nombre; ?></option>
                                                <?php } ?>
                                              </select>
                                          </div>
                                      </div>
                                    </div>
                                    <div class="col-md-12">
                                      <div class="form-group">
                                          <label class="col-sm-3 control-label">Código postal:</label>
                                          <div class="col-sm-3 controls">
                                              <input type="text" id="txtCP" name="txtCP" value="<?php echo $codigopostal;?>" class="form-control"/>
                                          </div>
                                          <label class="col-sm-2 control-label">Teléfono local:</label>
                                          <div class="col-sm-3 controls">
                                              <input type="text" id="txtTL" name="txtTL" value="<?php echo $telefono;?>" class="form-control"/>
                                          </div>
                                      </div>
                                    </div>
                                    <div class="col-md-12">
                                      <div class="form-group">
                                          <label class="col-sm-3 control-label">Teléfono celular:</label>
                                          <div class="col-sm-3 controls">
                                              <input type="text" id="txtTC" name="txtTC" value="<?php echo $celular;?>" class="form-control"/>
                                          </div>
                                          <label class="col-sm-2 control-label">Email:</label>
                                          <div class="col-sm-3 controls">
                                              <input type="text" id="txtEmail" name="txtEmail" value="<?php echo $correo;?>" class="form-control"/>
                                          </div>
                                      </div>
                                    </div>
                                    <div class="col-md-12">
                                      <h3>Datos laborales</h3>
                                      <hr />
                                    </div>
                                    <div class="col-md-12">
                                      <div class="form-group">
                                        <label class="col-sm-3 control-label">Turno:</label>
                                          <div class="col-sm-3 controls">
                                            <select id="cmbTurno" name="cmbTurno" class="form-control">
                                              <option value="1" <?php if ($turno==1) { echo "selected"; } ?>>Matutino</option>
                                              <option value="2" <?php if ($turno==1) { echo "selected"; } ?>>Vespertino</option>
                                              <option value="3" <?php if ($turno==1) { echo "selected"; }?>>Completo</option>
                                            </select>
                                          </div>
                                      </div>
                                    </div>
                                    <div class="col-md-12">
                                      <div class="form-group">
                                        <label class="col-sm-3 control-label">Fecha de ingreso:</label>
                                          <div class="col-sm-3 controls">
                                            <input type="date" id="txtFI" name="txtFI" value="<?php echo $fechaingreso;?>" class="form-control"/>
                                          </div>
                                          <label class="col-sm-2 control-label">Fecha de baja:</label>
                                          <div class="col-sm-3 controls">
                                            <input type="date" id="txtFB" name="txtFB" value="<?php echo $fechabaja;?>" class="form-control"/>
                                          </div>
                                      </div>
                                    </div>
                                    <div class="col-md-12">
                                      <div class="form-group">
                                        <label class="col-sm-3 control-label">Sueldo:</label>
                                          <div class="col-sm-3 controls">
                                            <input type="text" id="txtSueldo" name="txtSueldo" value="<?php echo $sueldo;?>" class="form-control"/>
                                          </div>
                                      </div>
                                    </div>
                                    <div class="col-md-12">
                                      <h3>Acceso</h3>
                                      <hr>
                                    </div>
                                    <div class="col-md-12">
                                      <div class="form-group">
                                        <label class="col-sm-3 control-label">Usuario</label>
                                        <div class="col-sm-3 controls">
                                            <input type="text" id="txtUsuario" name="txtUsuario" value="<?php echo $susuario ;?>" class="form-control">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-12">
                                      <div class="form-group">
                                        <label class="col-sm-3 control-label">Contraseña</label>
                                        <div class="col-sm-2 controls">
                                              <input type="password" id="txtPass" name="txtPass" value="<?php echo $spass ;?>" class="form-control">
                                        </div>
                                        <div class="col-sm-1 controls">
                                          
                                              <span class="input-group-addon" onmouseover="mouseoverPass();" onmouseout="mouseoutPass();">
                                                <i class="fa fa-eye"></i>
                                              </span>
                                        </div>
                                      </div>
                                      <script type="text/javascript">
                                        function mouseoverPass(obj) {
                                          var obj = document.getElementById('txtPass');
                                          var obj2 = document.getElementById('txtPass2');
                                          obj.type = "text";
                                          obj2.type = "text";
                                        }
                                        function mouseoutPass(obj) {
                                          var obj = document.getElementById('txtPass');
                                          var obj2 = document.getElementById('txtPass2');
                                          obj.type = "password";
                                          obj2.type = "password";
                                        }
                                      </script>
                                    </div>
                                    <div class="col-md-12">
                                      <div class="form-group">
                                        <label class="col-sm-3 control-label">Verificar Contraseña</label>
                                        <div class="col-sm-3 controls">
                                            <input type="password" class="form-control" id="txtPass2" name="txtPass2" value="<?php echo $spass ;?>">
                                        </div>
                                      </div>
                                    </div>
                                  </form>
                                  <div>
                                    <input type="hidden" id="txtIdMenu"  placeholder="ocultar add id" />
                                    <input type="hidden" id="txtIdMenutext"  placeholder="ocultar add text" />
                                    <input type="hidden" id="txtRemove" placeholder="ocultar remove id"/>
                                  </div>
                                      <div class="col-md-12">
                                        <div class="col-md-5">
                                          <div class="panel widget light-widget">
                                            <div class="panel-body-list">
                                              <div class="content-list content-image menu-action-right">
                                                <div  data-rel="scroll" >
                                                  <ul class="list-wrapper pd-lr-15">
                                                    <?php foreach ($menuse->result() as $item){ ?>
                                                      <li class="rowlink" style="cursor: pointer;" data-id="<?php echo $item->MenusubId; ?>" data-text="<?php echo $item->menu.' - '.$item->submenu; ?>">
                                                        <div class="menu-icon"></div>
                                                        <div class="menu-text"><strong><?php echo $item->menu.' - '.$item->submenu; ?></strong></div>
                                                      </li>
                                                    <?php } ?>
                                                  </ul>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="col-md-2" align="center">
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <button type="button" data-original-title="Agregar modulo a menu" data-toggle="tooltip" data-placement="right" class="btn btn-raised gradient-mint white shadow-z-4" id="add">>></button>
                                            
                                            <br />
                                            <br />
                                            <button type="button" data-original-title="Quitar modulo" data-toggle="tooltip" data-placement="right"  class="btn btn-raised gradient-pomegranate white big-shadow" id="remove"><<</button>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="panel widget light-widget">
                                                <div class="panel-body-list">
                                                  <div class="content-list content-image menu-action-right">
                                                    <div  data-rel="scroll" >
                                                      <u class="list-wrapper pd-lr-15" id="list">
                                                        <?php 
                                                          if (isset($_GET['id'])) {
                                                            foreach ($menusseleccionados->result() as $item){ ?>
                                                              <li class="linkrow menur_<?php echo $item->MenusubId;?>" style="cursor: pointer" data-id="<?php echo $item->MenusubId;?>">
                                                                <input type="hidden" name="addmenuselect" id="addmenuselect" value="<?php echo $item->MenusubId;?>">
                                                                <div class="menu-text"><?php echo $item->menu;?> - <?php echo $item->submenu;?></div>
                                                              </li>

                                                           <?php  }
                                                          }
                                                        ?>
                                                      </u>
                                                    </div>
                                                  </div>
                                                </div>
                                            </div>
                                        </div>
                                      </div>
                                  
                    
                                        
                                  
                                        <button class="btn btn-raised gradient-purple-bliss white shadow-z-1-hover"  type="submit"  id="savep">Guardar</button>
                                  
                                  
                                </div>
                                
                                
                                <!--------//////////////-------->
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              

	