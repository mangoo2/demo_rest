<style type="text/css">
  .vd_red{
    font-weight: bold;
    color: red;
    margin-bottom: 5px;
  }
  .vd_green{
    color: #009688; 
  }
  input,select,textarea{
    margin-bottom: 15px;
  }
  .switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>

<div class="row">
  <div class="col-md-12">
    <h2>Producto</h2>
  </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-sm-12">
      <div class="card">
          <div class="card-header">
              <h4 class="card-title"><?php echo $label;?></h4>
          </div>
          <div class="card-body">
              <div class="card-block form-horizontal">
                  <!--------//////////////-------->
                    <form method="post"  role="form" id="formproductos">
                      <input type="hidden" name="productoid" id="productoid" value="<?php echo $productoid;?>">
                      <div class="row">
                        <label class="col-md-2 control-label">Clave:</label>
                        <div class="col-md-4">
                          <input type="text" class="form-control" name="codigo" id="codigo" value="<?php echo $codigo;?>" aria-describedby="button-addon2">
                        </div>
                        <div class="col-md-2">
                          <a href="#" class="btn btn-raised btn-primary" id="generacodigo" >Generar</a>
                        </div>
                      </div>
                      <div class="row">
                        <label class="col-md-2 control-label">Concepto:</label>
                        <div class="col-md-10">
                          <input type="text" class="form-control" id="nombre" name="nombre" value="<?php echo $nombre;?>">
                        </div>
                      </div>
                      <div class="row">
                        <label class="col-md-2 control-label">Existencia <input title="Dar click solo si el producto es cerrado, ¡Un producto sin receta!" type="checkbox" name="stockok" id="stockok" <?php if($stockok==1){ echo 'checked';} ?>  >:</label>
                        <div class="col-md-4">
                          <input type="number" class="form-control" id="stock" name="stock" value="<?php echo $stock;?>" placeholder="unidades">
                        </div>
                        <label class="col-md-2 control-label">Stock mínimo (GR/ML/PZA):</label>
                        <div class="col-md-4">
                          <input type="number" class="form-control" id="stockmin" name="stockmin" value="<?php echo $stockmin;?>" placeholder="Stock mínimo">
                        </div>
                      </div>
                      <div class="row">
                        <h3>Precios</h3>
                        <hr />
                      </div>
                      <div class="row">
                        <label class="col-md-2 control-label">Precio POS:</label>
                        <div class="col-md-4">
                          <input type="number" class="form-control" id="preciopos" name="preciopos" value="<?php echo $preciopos;?>" >
                        </div>
                        <label class="col-md-2 control-label">Costo compra / Producción:</label>
                        <div class="col-md-4">
                          <input type="number" class="form-control" id="preciocompra" name="preciocompra" value="<?php echo $preciocompra;?>" >
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12"><br><br></div>
                        <div class="col-md-3">
                          <label class="col-md-6 control-label" for="">Producto simple</label>
                          <label class="switch">
                            <input type="radio" <?php if($tipo==0) echo "checked"; if($tipo=="") echo "checked"; ?> name="tipo" id="tipo1" class="detipo">
                            <span class="slider round detipo"></span>
                          </label>
                        </div>
                        <div class="col-md-3">
                          <label class="col-md-6 control-label" for="">Combo</label>
                          <label class="switch">
                            <input type="radio" <?php if($tipo==1) echo "checked"; ?> name="tipo" id="tipo2" class="detipo">
                            <span class="slider round detipo"></span>
                          </label>
                        </div>
                      </div>
                      
                    </form>

                    <div id="div_cats_combo">
                      <div class="col-md-12"></div>
                      <div class="row">
                        <div class="col-md-12"><hr></div>
                        <div class="col-md-10">
                          <br>
                          <h3 id="titlecat">CATEGORÍA(S) A LA QUE PERTENECE EL COMBO</h3>
                        </div>
                        <div class="form-group">
                          <label class="col-md-4 control-label">Categoría</label>
                          <div class="col-md-8 form-group">
                            <select id="id_catego" class="form-control">
                              <option value="" disabled selected>Elige:</option>
                              <?php foreach ($cats->result() as $item){ ?>      
                              <option data-name="<?php echo $item->categoria; ?>" value="<?php echo $item->categoriaId; ?>"><?php echo $item->categoria; ?></option>
                              <?php } ?>
                            </select> 
                          </div>
                        </div>
                        <div class="col-md-1">
                          <button class="btn btn-raised gradient-purple-bliss white shadow-z-1-hover" id="addcategos"><i class="fa fa-plus"></i> </button>
                        </div>
                        <div class="col-md-6">
                          <label class="col-md-6 control-label">Cantidad de Productos Totales:</label>
                          <div class="col-md-4">
                            <input type="number" maxlength="99" class="form-control" id="cant_tot_prods" name="cant_tot_prods" value="<?php echo $cant_tot_prods; ?>">
                          </div>
                        </div>
                        <div class="col-md-12">
                          <table class="table" id="table-cats">
                            <tbody class="btable-cats">
                                <?php if(isset($categos)) { 
                                  foreach ($categos as $c) { ?>
                                    <tr class="pro_si_catego_<?php echo $c->id; ?>">
                                      <td>Cantidad de categorías</td>
                                      <td>
                                        <input type="hidden" class="form-control" id="tipo" value="<?php echo $c->tipo ?>">
                                        <input type="hidden" class="form-control" id="id_cat_prod" value="<?php echo $c->id ?>">
                                        <input type="hidden" class="form-control" id="id_categoria" value="<?php echo $c->id_categoria ?>">
                                        <input type="number" class="form-control" min="1" id="cantidad" value="<?php echo $c->cantidad ?>" style="max-width: 100px">
                                      </td>
                                      <td><?php echo $c->categoria; ?></td>
                                      <td>
                                        <button class="btn btn-raised gradient-flickr white sidebar-shadow"
                                          onclick="deletecatego(<?php echo $c->id; ?>,0)"
                                          data-toggle="tooltip" data-placement="top"
                                          data-original-title="Eliminar">
                                                <i class="fa fa-times"></i>
                                        </button>
                                      </td>
                                    </tr>
                                <?php }
                                  } ?>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-12"><br><br></div>
                    <div class="row" id="div_receta">
                      <div class="col-md-10">
                        <br>
                        <h3 id="title_tipo">RECETA DEL PRODUCTO (EN CASO DE EXISTIR)</h3>
                      </div>
                      <div class="col-md-2">
                        <button class="btn btn-raised gradient-purple-bliss white shadow-z-1-hover" id="addinsumos">Nuevo insumo</button>
                      </div>
                      <div class="col-md-12">
                        <table class="table" id="table-insumos">
                          <tbody class="btable-insumos">
                            
                          </tbody>
                        </table>
                      </div>
                    </div>

                    <div class="row">
                       <div class="col-md-12">
                          <button class="btn btn-raised gradient-purple-bliss white shadow-z-1-hover"  type="submit"  id="savepr">Guardar</button>
                        </div> 
                    </div>
                    
                  <!--------//////////////-------->
              </div>
          </div>
      </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $('#generacodigo').click(function(event) {
      var d = new Date();
      var dia=d.getDate();//1 31
      var dias=d.getDay();//0 6
      var mes = d.getMonth();//0 11
      var yy = d.getFullYear();//9999
      var hr = d.getHours();//0 24
      var min = d.getMinutes();//0 59
      var seg = d.getSeconds();//0 59
      var yyy = 18;
      var ram = Math.floor((Math.random() * 10) + 1);
      var codigo=seg+''+min+''+hr+''+yyy+''+ram+''+mes+''+dia+''+dias;
      //var condigo0=condigo.substr(0,13);
      //console.log(codigo);
      $('#codigo').val(codigo);
    });
    $(".detipo").on("click",function(){
      if($("#tipo1").is(":checked")==true){
        $("#title_tipo").html("RECETA DEL PRODUCTO (EN CASO DE EXISTIR)");
        $("#addinsumos").text("Nuevo insumo");
      }else{
        $("#title_tipo").html("PRODUCTOS DEL COMBO");
        $("#addinsumos").text("Nuevo Producto");
      }
    });

  });
</script>