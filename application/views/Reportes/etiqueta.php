<?php

    require_once('TCPDF/examples/tcpdf_include.php');
    require_once('TCPDF/tcpdf.php');
    $this->load->helper('url');
    $imglogo = base_url().'public/img/ops.png';
    if (isset($_GET['page'])) {
      $pagina=$_GET['page'];
    }else{
      $pagina=1;
    }
    $paginai=1;
    $style = array(
      'position' => 'C',
      'align' => 'C',
      'stretch' => false,
      'fitwidth' => true,
      'cellfitalign' => '',
      'border' => false,
      'hpadding' => 'auto',
      'vpadding' => 'auto',
      'fgcolor' => array(0,0,0),
      'bgcolor' => false, //array(255,255,255),
      'text' => true,
      'font' => 'helvetica',
      'fontsize' => 10,
      'stretchtext' => 4
  );
    foreach ($getventasd->result() as $item){
      $codigo = $item->codigo;
      $nombre = $item->nombre;
      $precio = $item->preciopos;
    }
    
//=======================================================================================
 
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(58, 155), true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('agb');
$pdf->SetTitle('Etiquetas');
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);



// set margins
$pdf->SetMargins('6', '1', '6');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(true, 6);// margen del footer

$pdf->SetFont('dejavusans', '', 17);
// add a page
while ($paginai <= $pagina){
  $pdf->AddPage('L');
  $html = '<table width="100%" border="0" >
              <tr>
                <td align="center"><img src="'.$imglogo.'" width="60px" ></td>
              </tr>
            </table>';
        //$pdf->writeHTML($html, true, false, true, false, '');
  $pdf->write1DBarcode($codigo, 'CODABAR', '', '', '', 35, 0.8, $style, 'N');
  $html = '<table width="100%" border="0" >
              <tr>
                <td align="center">'.$nombre.'</td>
              </tr>
              <tr>
                <td align="center">$ '.$precio.'</td>
              </tr>
            </table>';
        $pdf->writeHTML($html, true, false, true, false, '');
  $paginai++;
}
if (isset($_GET['print'])) {
  $pdf->IncludeJS('print(true);');
}

$pdf->Output('Captura.pdf', 'I');

//$pdf->Output('files/'.$GLOBALS["carpeta"].'/facturas/'.$GLOBALS["rrfc"].'_'.$GLOBALS["Folio"].'.pdf', 'F');

?>