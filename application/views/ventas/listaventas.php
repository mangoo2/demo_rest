<div class="row">
    <div class="col-md-12">
      <h2>Lista ventas</h2>
    </div>
</div>
<!--Statistics cards Ends-->

<!--Line with Area Chart 1 Starts-->
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Listado de ventas</h4>
            </div>
            <div class="col-md-12">
                <div class="col-md-8">
                    <div class="form-group">
                      <label class="col-sm-3 control-label">Tipo de servicio:</label>
                      <div class="col-sm-4 controls">
                        <select id="tipo" name="tipo"class="form-control" onchange="seleccionarTipo()">
                            <option value="1" <?php if(isset($_GET['tipo'])){ if($_GET['tipo']==1){ echo "selected"; } }?>>Mostrador</option>
                            <option value="2" <?php if(isset($_GET['tipo'])){ if($_GET['tipo']==2){ echo "selected"; } }?>>Mesa</option>
                            <option value="3" <?php if(isset($_GET['tipo'])){ if($_GET['tipo']==3){ echo "selected"; } }?>>A domicilio</option>
                        </select>
                     </div>
        
                  </div>
                </div>
                <!--<div class="col-md-3">
                    <form role="search" class="navbar-form navbar-right mt-1">
                        <div class="position-relative has-icon-right">
                          <input type="text" placeholder="Buscar" id="buscarvent" class="form-control round" oninput="buscarventa()">
                          <div class="form-control-position"><i class="ft-search"></i></div>
                        </div>
                    </form>
                </div>-->
            </div>
            <div class="card-body">
                <div class="card-block">
                    <!--------//////////////-------->
                    <br>
                    <table class="table table-striped" id="data-tables" style="width: 100%">
                          <thead>
                            <tr>
                              <th>Folio</th>
                              <th>Fecha</th>
                              <th>Vendedor</th>
                              <th>Monto</th>
                              <th>Tipo</th>
                              <th>Cliente</th>
                              <th>Estatus</th>
                              <th>Pagado</th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody id="tbodyresultadosvent">

                            <?php foreach ($ventas->result() as $item){ ?>
                            <tr id="trven_<?php echo $item->id_venta; ?>">
                                <td><?php echo $item->id_venta; ?></td>
                                <td><?php echo $item->reg; ?></td>
                                <td><?php echo $item->vendedor; ?></td>
                                <td><?php echo "$".number_format($item->monto_total,2,".",","); ?></td>
                                <td><?php 
                                  if($item->tipo_costo==1) echo "Mostrador";
                                  if($item->tipo_costo==2) echo "Mesa"; 
                                  if($item->tipo_costo==3) echo "A domicilio";   ?></td>
                                <td><?php echo $item->cliente; ?></td>

                                <td><?php if($item->cancelado==1){ echo '<span class="badge badge-danger">Cancelado</span>';} 
                                    else if($item->cancelado==0){ echo '<span class="badge badge-success">Activa</span>';}
                                ?>
                                </td>
                                <td>
                                    <?php if($item->pagado==1){ echo '<span class="badge badge-success">Pagado</span>'; }
                                    else if($item->pagado==0){ echo '<span class="badge badge-danger">Sin Pagar</span>';} ?>
                                </td>
                                <td>
                                    <button class="btn btn-raised gradient-blackberry white sidebar-shadow" onclick="ticket(<?php echo $item->id_venta; ?>)" title="Ticket" data-toggle="tooltip" data-placement="top">
                                    <i class="fa fa-book"></i>
                                    </button>
                                    <button class="btn btn-raised gradient-flickr white sidebar-shadow" onclick="cancelar(<?php echo $item->id_venta; ?>,<?php echo $item->monto_total; ?>)" title="Cancelar" data-toggle="tooltip" data-placement="top" <?php if($item->cancelado==1){ echo 'disabled';} ?>>
                                  <i class="fa fa-times"></i>
                                    </button>
                                <?php if($item->tipo_costo==3) { ?>
                                    <button class="btn btn-raised gradient-flickr white sidebar-shadow" onclick="pagar(<?php echo $item->id_venta; ?>,<?php echo $item->pagado; ?>,<?php echo $item->monto_total; ?>)" title="Pagado y entregado" data-toggle="tooltip" data-placement="top" <?php if($item->cancelado==1){ echo 'disabled';} ?>>
                                        <i class="fa fa-usd"></i>
                                    </button>
                                <?php } ?>
                              </td>
                                
                            </tr>
                              
                            <?php } ?>
                                
                          </tbody>
                        </table>
                        <!--<table class="table table-striped" id="data-tables2" style="display: none;width: 100%">
                          <thead>
                            <tr>
                              <th>Folio</th>
                              <th>Fecha</th>
                              <th>Vendedor</th>
                              <th>Monto</th>
                              <th>Cliente</th>
                              <th></th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody id="tbodyresultadosvent2">
                          </tbody>
                          
                        </table>
                        <div class="col-md-12">
                          <div class="col-md-7">
                            
                          </div>
                          <div class="col-md-5">
                            <?php echo $this->pagination->create_links() ?>
                          </div>
                        </div>-->
                        
                        
            <!--------//////////////-------->
                </div>
            </div>
        </div>
    </div>
</div>
<!------------------------------------------------>
<style type="text/css">
    #iframereporte{
        background: white;
    }
    iframe{
        height: 500px;
        border:0;
        width: 100%;
    }
</style>
<div class="modal fade text-left" id="iframeri" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Ticket</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!--<div class="modal-body">-->
            <div id="iframereporte"></div>
            <!--</div>-->
            <div class="modal-footer">
                <button type="button" class="btn btn-raised gradient-ibiza-sunset white" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade text-left" id="cancelar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Mensaje de confirmaci&oacute;n</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Desea cancelar el ticket No. <span id="NoTicket"></span> por un total de <span id="CantidadTicket"></span>?
                      <input type="hidden" id="hddIdVenta">
            </div>
            <div class="modal-footer">
                <button type="button" id="sicancelar" class="btn btn-raised gradient-purple-bliss white" data-dismiss="modal">Aceptar</button>
                <button type="button" class="btn btn-raised gradient-ibiza-sunset white" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var table;
  $(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('#sicancelar').click(function(){
        params = {};
        params.id = $('#hddIdVenta').val();
        $.ajax({
            type:'POST',
            url:'<?php echo base_url(); ?>ListaVentas/cancalarventa',
            data:params,
            async:false,
            success:function(data){
              toastr.success('Cancelado Correctamente','Hecho!');
              //$('#trven_'+params.id).remove();
              location.reload();
            }
        });
    });
    table = $("#data-tables").dataTable({});

  });
    function seleccionarTipo(){
      var tipo=$('#tipo option:selected').val();
      location.href="<?php echo base_url(); ?>ListaVentas?tipo="+tipo;
    }
  function ticket(id){
    $("#iframeri").modal();
    $('#iframereporte').html('<iframe src="<?php echo base_url(); ?>Ticket?id='+id+'"></iframe>');
  }
  function cancelar(ID,CantidadTicket){
    $('#cancelar').modal();
    $("#hddIdVenta").val(ID);
    $("#NoTicket").html(ID);
    $("#CantidadTicket").html(CantidadTicket);  
  }
function buscarventa(){
    var search=$('#buscarvent').val();
    if (search.length>2) {
        $.ajax({
            type:'POST',
            url: '<?php echo base_url(); ?>ListaVentas/buscarvent',
            data: {
                buscar: $('#buscarvent').val()
            },
            async: false,
            statusCode:{
                404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
                500: function(){ toastr.error('Error', '500');}
            },
            success:function(data){
                $('#tbodyresultadosvent2').html(data);
            }
        });
      $("#data-tables").css("display", "none");
        $("#data-tables2").css("display", "");
    }else{
        $("#data-tables2").css("display", "none");
        $("#data-tables").css("display", "");
    }
}

function pagar(id_venta,pagado,monto){
    title ="";
    txt="";
    if(pagado==0)
        apagar=1;
    else
        apagar=0;

    if(pagado==1){
        title ="Atención! Venta pagada";
        txt="¿Está seguro de cambiar el estatus a no pagado?"; 
        type='red';
    }else{
        title ="Atención!";
        txt="¿Está seguro de cambiar el estatus a venta pagada?"; 
        type='blue';
    }
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: title,
        content: txt,
        type: type,
        typeAnimated: true,
        buttons: 
        {
            // Si le da click al botón de "confirmar"
            confirmar: function () 
            {
                $.ajax({
                    type:'POST',
                    url: '<?php echo base_url(); ?>ListaVentas/pagarVenta',
                    data: {id_venta:id_venta,pagado:apagar,total:monto},
                    async: false,
                    statusCode:{
                        404: function(data){
                            toastr.error('Error!', 'No Se encuentra el archivo');
                        },
                        500: function(){
                            toastr.error('Error', '500');
                        }
                    },
                    success:function(data){
                        toastr.success('Hecho!', 'Estatus cambiado correctamente');
                        //table.ajax.reload();
                        setTimeout(function () { location.reload(); }, 1500);
                    }
                });
                
            },
            // Si le da click a "cancelar"
            cancelar: function () 
            {
                
            }
        }
    });
}
  
</script>