<div class="sesion_texto" style="width: 400px;
    height: 400px;
    position: absolute;
    top: 50%;
    left: 50%;
    margin-left: -200px;
    margin-top: -200px;
    z-index: 1;
    display: none;" align="center">
            <!-- style="background: #ffffffad; border-radius: 20px;" 
            
            -->   
        <img style="" src="<?php echo base_url(); ?>public/img/ico/ops.png" width="50%" class="m-t-90" alt="logo" />
        
        <h2 style="color: white;">Iniciando Sesión</h2>
        <h3 style="color: white;">Conectado a la base de datos</h3>
    </div>

    <div class="cx3"></div>
    <div class="cx2" style="z-index: -1;"></div>
    <div class="cx1"></div>
    
    <div class="login-box" align="center">
        <div class="card login_texto" style="width: 350px;border-radius: 25px;">
            <div class="body">
                <form class="form-horizontal" id="login-form" action="#" role="form" method="POST">
                    <div class="row align-center">
                        <div class="col-md-12">
                            <img style="" src="<?php echo base_url(); ?>public/img/ico/ops.png" width="50%" alt="logo" />
                            <h2>Accede a tu cuenta</h2>
                            <h5>Estamos contentos de que regreses</h5>
                            <div class="form-group input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">person</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" class="form-control" name="txtUsuario" id="txtUsuario" placeholder="Nombre de Usuario" required autofocus>
                                </div>
                            </div>
                            <div class="form-group input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">lock</i>
                                </span>
                                <div class="form-line">
                                    <input type="password" class="form-control" name="txtPass" id="txtPass" placeholder="Contraseña" required>
                                </div>
                            </div>
                            <div class="row"> 
                                <div class="col-xs-12">
                                    <button class="btn btn-block bg-pink waves-effect" type="submit" id="login-submit">INICIAR SESIÓN</button>
                                </div>
                            </div>
                            <br>
                           
                        </div>
                    </div>
                </form>
            </div>
        <div>    
    </div>
    </div>
    <div class="text-center" id="loader">
        <div class="lds-ripple"><div></div><div></div></div>
    </div>
    
    <div class="alert bg-light-green" id="success" style="display: none;">
        <i class="material-icons ">done</i> <strong>Acceso Correcto!</strong> Será redirigido al sistema 
    </div>
    
    <div class="alert bg-pink" id="error" style="display: none;">
        <i class="material-icons ">error</i> <strong>Error!</strong> El nombre de usuario y/o contraseña son incorrectos 
    </div>