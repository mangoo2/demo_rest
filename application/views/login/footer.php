    <!-- Jquery Core Js -->
    <script src="<?php echo base_url(); ?>app-assets/js/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url(); ?>app-assets/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url(); ?>app-assets/js/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="<?php echo base_url(); ?>app-assets/js/jquery.validate.js"></script>

    <!-- Custom Js -->
    <!--<script src="https://carta-digital.mx/sistema/public/js/login.js"></script>-->
</body>

</html>