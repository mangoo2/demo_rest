<div class="container">
	<div class="vd_title-section clearfix">
        <div class="vd_panel-header">
            <h1>Configuración</h1>
        	<small class="subtitle">Configuración de usuarios</small>
        </div>
    </div>
	<div class="vd_content-section clearfix">
		<div class="row">
			<div class="col-md-12">
				<div class="panel widget">
					<div class="panel-heading vd_bg-grey">
						<h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span> Usuarios </h3>
						<div class="vd_panel-menu">
								<div data-action="minimize" data-original-title="Minimize" data-toggle="tooltip" data-placement="bottom" class="menu entypo-icon">
									<i class="icon-minus3"></i>
								</div>
								<div class="tooltip fade bottom" role="tooltip" id="tooltip108722" style="display: block; top: 24px; left: -22.5px;">
									<div class="tooltip-arrow"></div><div class="tooltip-inner">Minimize</div>
								</div>
								<div data-action="close" data-original-title="Close" data-toggle="tooltip" data-placement="bottom" class=" menu entypo-icon">
									<i class="icon-cross"></i>
								</div>
						</div>
					</div>
					<div class="panel-body">
						<!--<a href="Personaladd" data-toggle="tooltip" data-original-title="Nuevo evento"><button data-target='#nuevaagenda' data-toggle="modal"  data-placement="bottom" data-original-title="Venta" title="Venta" class="btn vd_btn vd_bg-blue" type="button"><i class="fa fa-calendar"></i> Agregar nuevo</button></a>-->

						<div class="panel widget light-widget">
	                 		<div class="panel-body">
	                    		<div class="col-md-12">
									<!-------------------------------->	  
									<div class="col-md-5">
										<table class="table table-striped table-hover">
											<thead>
												<tr><th></th></tr>
											</thead>
											<tbody id="viewusuarios">
												<?php foreach ($usuarios->result() as $item){ ?>
                  						 			<tr  class="rowlink" style="cursor: pointer" data-id="<?php echo $item->UsuarioID; ?>">
						                          		<td><?php echo $item->Usuario; ?></td>
						                          		<td><?php echo $item->nombre; ?></td>
						                          		<td></td>
						                     
						                        </tr>


						                        
				                          
				                          
                						<?php } ?>
											</tbody>
										</table>
									</div>                  			
									<div class="col-md-7" id="form">
										<div class="form-group">
											<label class="col-md-5 control-label">Nombre de usuario</label>
											<div class="col-md-6">
												<input type="text" class="form-control" id="txtUsuario" name="txtUsuario" onchange="verificarusu()" />
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-5 control-label">Contraseña</label>
											<div class="col-md-6">
												<input type="password" class="form-control" id="txtPass" name="txtPass" />
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-5 control-label">Verificar la contraseña</label>
											<div class="col-md-6">
												<input type="password" class="form-control" id="txtPass2" name="txtPass2" />
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-5 control-label">Empleado</label>
											<div class="col-md-6">
												<select id="cmbPersonal" name="cmbPersonal" class="form-control">
													<?php foreach ($personal->result() as $item){ ?>
                  						 			
													<option value="<?php echo $item->personalId; ?>"><?php echo $item->nombre; ?></option>
													<?php } ?>
												</select> 
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-5 control-label">Perfil</label>
											<div class="col-md-6">
												<select id="cmbPerfiles" name="cmbPerfiles" class="form-control">
													<?php foreach ($perfiles->result() as $item){ ?>
                  						 			
													<option value="<?php echo $item->perfilId; ?>"><?php echo $item->nombre; ?></option>
													<?php } ?>
												</select>
											</div>
										</div>
										<div class="form-group">
											<button class="btn" id="btnCancelar">Cancelar</button>
											<button class="btn vd_btn vd_bg-green vd_white" id="guardarus">Guardar</button>
										</div>
										<input type="hidden" id="txtId" />
									</div>



	                    			<!-------------------------------->
	                    		</div>
	                    		
   							</div>
   						</div>	
   					</div>
   				</div>
   			</div>
   		</div>
   	</div>
	<script type="text/javascript">
		$(document).ready(function() {
				$('#data-tables').dataTable();
		} );
		
</script>
		