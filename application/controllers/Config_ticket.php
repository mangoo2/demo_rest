<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Config_ticket extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
    }
	public function index(){
        $data['configticket']=$this->ModeloCatalogos->configticket();
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('config/ticket',$data);
        $this->load->view('templates/footer');
	}
    function updateticket(){
        $titulo = $this->input->post('titulo');
        $mensaje1 = $this->input->post('mensaje1');
        $mensaje2 = $this->input->post('mensaje2');
        $fuente = $this->input->post('fuente');
        $tamano = $this->input->post('tamano');
        $margsup = $this->input->post('margsup');
        $this->ModeloCatalogos->configticketupdate($titulo,$mensaje1,$mensaje2,$fuente,$tamano,$margsup);

    }
    




}