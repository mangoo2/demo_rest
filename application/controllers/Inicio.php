<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloProductos');
    }
	public function index(){
        $data['total_rows'] = $this->ModeloProductos->filas();
        $data['totalexistencia'] = $this->ModeloProductos->totalproductosenexistencia();
            $data['totalproductopreciocompra'] = $this->ModeloProductos->totalproductopreciocompra();
            $data['totalproductoporpreciocompra'] = $this->ModeloProductos->totalproductoporpreciocompra();
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('inicio',$data);
        $this->load->view('templates/footer');
        unset($_SESSION['pro']);
        $_SESSION['pro']=array();
        unset($_SESSION['can']);
        $_SESSION['can']=array();
	}
    function notas(){
        $user = $this->input->post('user');
        $nota = $this->input->post('nota');
        $this->ModeloCatalogos->updatenota($user,$nota);
    }

}