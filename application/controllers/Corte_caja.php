<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Corte_caja extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        //$this->load->model('Personal/ModeloPersonal');
        //$this->load->model('Usuarios/ModeloUsuarios');
        $this->load->model('ModeloVentas');
    }
	public function index(){
            //$data['personal']=$this->ModeloPersonal->getpersonal();
            //carga de vistas
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            //$this->load->view('Personal/Personal',$data);

            $this->load->view('corte/corte');
            $this->load->view('templates/footer');
            $this->load->view('corte/jscorte');

	}
    function corte(){
        $inicio = $this->input->post('fecha1');
        $fin = $this->input->post('fecha2');
        $tipo = $this->input->post('tipo');
        $resultadoc=$this->ModeloVentas->corte($inicio,$fin,$tipo);
        $resultadocs=$this->ModeloVentas->cortesum($inicio,$fin,$tipo);

        $table="<table class='table table-striped table-bordered table-hover' id='sample_2'>
                    <thead>
                        <tr>
                            <th>No. venta</th>
                            <th>Tipo</th>
                            <th>Cajero</th>
                            <th>Cliente</th>
                            <th>Fecha</th>
                            <th>Subtotal</th>
                            <th>Total</th>
                            <th>Efectivo</th>
                            <th>Tarjeta</th>
                        </tr>
                    </thead>
                    <tbody>";
        $rowventas=0;
        $utilidad=0;
        foreach ($resultadoc->result() as $fila) { 
            if($fila->tipo_costo==1)
                $tipoc="Mostrador";
            if($fila->tipo_costo==2)
                $tipoc="Mesa";
            if($fila->tipo_costo==3)
                $tipoc="A Domicilio";
            $table .= "<tr>
                            <td>".$fila->id_venta."</td>
                            <td>".$tipoc."</td>
                            <td>".$fila->vendedor."</td>
                            <td>".$fila->Nom."</td>
                            <td>".$fila->reg."</td>
                            <td>$ ".number_format($fila->subtotal,2,'.',',')."</td>
                            <td>$ ".number_format($fila->monto_total,2,'.',',')."</td>";
                            if($fila->efectivo>0 && $fila->pagotarjeta>0){
                                $table .= "<td>$ ".number_format($fila->efectivo,2,'.',',')."</td>
                                    <td>$ ".number_format($fila->pagotarjeta,2,'.',',')."</td>";
                            }else if($fila->efectivo>0 && $fila->pagotarjeta==0){
                                $table .= "<td>$ ".number_format($fila->monto_total,2,'.',',')."</td>
                                    <td>$ ".number_format($fila->pagotarjeta,2,'.',',')."</td>";
                            }else if($fila->efectivo==0 && $fila->pagotarjeta>0){
                                $table .= "<td>$ ".number_format($fila->efectivo,2,'.',',')."</td>
                                    <td>$ ".number_format($fila->pagotarjeta,2,'.',',')."</td>";
                            }
                            else if($fila->efectivo==0 && $fila->pagotarjeta==0){
                                $table .= "<td>$ ".number_format($fila->efectivo,2,'.',',')."</td>
                                    <td>$ ".number_format($fila->pagotarjeta,2,'.',',')."</td>";
                            }
 
                        $table .= " </tr>";
                        $rowventas++;
                        //if($fila->tipo_costo==0){
                            $utilidad=$utilidad+($fila->precio-$fila->preciocompra)*$fila->cantidad;
                        //}
        }
        $table.="</tbody> </table>";

        $table2="<table class='table table-striped table-bordered table-hover' id='sample_2'>
                    <thead>
                        <tr>
                            <th>Turno</th>
                            <th>Apertura</th>
                            <th>Cierre</th>
                            <th>Utilidad</th>
                        </tr>
                    </thead>
                    <tbody>";
        $respuestaturno=$this->ModeloVentas->consultarturnoname($inicio,$fin);
        $obedt=0;
        foreach ($respuestaturno->result() as $fila) { 
            if ($fila->fechacierre=='0000-00-00') {
                $horac =date('H:i:s');
                $fechac =date('Y-m-d');
            }else{
                $horac =$fila->horac;
                $fechac =$fila->fechacierre;
            }
            $fecha =$fila->fecha;
            $horaa =$fila->horaa; 
            $totalventas=$this->ModeloVentas->consultartotalturno($fecha,$horaa,$horac,$fechac);
            $totalpreciocompra=$this->ModeloVentas->consultartotalturno2($fecha,$horaa,$horac,$fechac);
            $obed =$totalventas-$totalpreciocompra;
            $table2 .= "<tr>
                            <td>".$fila->nombre."</td>
                            <td>".$fila->horaa."</td>
                            <td>".$fila->horac."</td>
                            <td>".number_format($obed,2,'.',',')."</td>
                        </tr>";
            $obedt=$obedt+$obed;
        }
        $table2.="</tbody> </table>";

        $total=0;
        $subtotal=0;
        foreach ($resultadocs->result() as $fila) {
            $total = $fila->total;
            $subtotal = $fila->subtotal;
            $descuento = $fila->descuento;
        }

        $total = round($total,2);
        $subtotal = round($subtotal,2);
        $total=number_format($total,2,'.',',');
        $utilidad=number_format($utilidad,2,'.',',');
        //$obedt=number_format($obedt,2,'.',',');
        $utilidad=number_format($utilidad,2,'.',',');
        $array = array("tabla"=>$table,
                        "tabla2"=>$table2,
                        "dTotal"=>"".$total."",
                        "totalventas"=>$rowventas,
                        "dImpuestos"=>0,
                        "totalutilidad"=>"".$utilidad."",
                        "descuento"=>$descuento,
                        "dSubtotal"=>"".$subtotal."",
                    );
            echo json_encode($array);
    }
    

       
    
}
