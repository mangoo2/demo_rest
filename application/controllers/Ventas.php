<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ventas extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        error_reporting(0);
        $this->load->model('ModeloClientes');
        $this->load->model('ModeloProductos');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloVentas');
        date_default_timezone_set('America/Mexico_City');
    }
	public function index(){
        $data['sturno']=$this->ModeloVentas->turnos();
        $data['clientedefault']=$this->ModeloVentas->clientepordefecto();
        $data["comandas"]=$this->ModeloCatalogos->getselectwheren("ventas",array("pagado"=>0,"cancelado"=>0,"tipo_costo"=>2));
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('ventas/ventas',$data);
        $this->load->view('templates/footer');
        $this->load->view('ventas/jsventas');
	}
    public function searchcli(){
        $usu = $this->input->get('search');
        $results=$this->ModeloClientes->clientesallsearch($usu);
        echo json_encode($results->result());
    }
    public function searchproducto(){
        $pro = $this->input->get('search');
        $results=$this->ModeloProductos->productoallsearch($pro);
        echo json_encode($results->result());
    }

    public function addproducto(){
        $cant = $this->input->post('cant');
        $prod = $this->input->post('prod');
        $tipo = $this->input->post('tipo');

        $personalv=$this->ModeloProductos->getproducto($prod);
        //$oProducto = new Producto();
        foreach ($personalv->result() as $item){
            $id = $item->productoid;
            $codigo = $item->codigo;
            $nombre = $item->nombre;
            $preciopos = $item->preciopos;
            $tipo_prod = $item->tipo;
            $cant_prod = $item->cant_tot_prods;

            //if($tipo=="0")
                $costo=$preciopos;
            /*if($tipo=="1")
                $costo=$preciouber;
            if($tipo=="2")
                $costo=$preciorappi;
            if($tipo=="3")
                $costo=$preciodid;*/

            $oProducto=array("id"=>$id,"codigo"=>$codigo,"nombre"=>$nombre,'precioventa'=>$costo,'preciopos'=>$preciopos,'tipo_prod'=>$tipo_prod,'cant_prod'=>$cant_prod,"id_pro_sub"=>0);
        }

        //log_message('error', 'cant_prod: '.$cant_prod);
        if(in_array($oProducto, $_SESSION['pro'])){  // si el producto ya se encuentra en la lista de compra suma las cantidades
            $idx = array_search($oProducto, $_SESSION['pro']);
            //if($_SESSION['tipo_prod'][$idx]==0){
                $_SESSION['can'][$idx]+=$cant;
            /*}else{
                array_push($_SESSION['pro'],$oProducto);
                array_push($_SESSION['can'],$cant);
                array_push($_SESSION['tipo_prod'],$tipo_prod);
            }*/
        }
        else{ //sino lo agrega
            array_push($_SESSION['pro'],$oProducto);
            array_push($_SESSION['can'],$cant);
            array_push($_SESSION['tipo_prod'],$tipo_prod);
        }
        
        //======================================================================

        $count = 0;
        $n =array_sum($_SESSION['can']);
        foreach ($_SESSION['pro'] as $fila){
            $Cantidad=$_SESSION['can'][$count]; 
        
            /*if ($Cantidad>=$fila['canmayoreo']) {
                $precio=$fila['mayoreo'];
            }elseif ($Cantidad>=$fila['canmediomayoreo'] && $Cantidad<$fila['canmayoreo']) {
                $precio=$fila['mediomayoreo'];
            }else{
                $precio=$fila['precioventa'];
            }*/

            //if($tipo=="0")
                $costo=$fila['preciopos'];
            /*if($tipo=="1")
                $costo=$fila['preciouber'];
            if($tipo=="2")
                $costo=$fila['preciorappi'];
            if($tipo=="3")
                $costo=$fila['preciodid'];*/

            $cantotal=$Cantidad*$costo;
            ?>
            <tr class="producto_<?php echo $count;?>" id="producto_tabla">                                        
                <td>
                    <input type="hidden" name="vsproid" id="vsproid" value="<?php echo $fila['id'];?>">
                    <input type="hidden" id="tipo_prod" value="<?php echo $fila['tipo_prod'];?>">
                    <input type="hidden" id="de_prod" value="0">
                    <input type="hidden" id="cant_prod" value="<?php echo $cant_prod; ?>">
                    <?php echo $fila['codigo'];?>
                </td>                                        
                <td>
                    <input type="number" name="vscanti" id="vscanti" value="<?php echo $Cantidad;?>" readonly style="background: transparent;border: 0px; width: 80px;">
                </td>                                        
                <td><?php echo $fila['nombre'];?></td>                                        
                <td>$ <input type="text" name="vsprecio" id="vsprecio" value="<?php echo $costo;?>" readonly style="background: transparent;border: 0px;width: 100px;"></td>                                        
                <td>$ <input type="text" class="vstotal" name="vstotal" id="vstotal" value="<?php echo $cantotal;?>" readonly style="background: transparent;border: 0px;    width: 100px;"></td>                                        
                <td>                                            
                    <a class="danger" data-original-title="Eliminar" title="Eliminar" onclick="deletepro(<?php echo $count;?>, <?php echo $tipo_prod;?>)">
                        <i class="ft-trash font-medium-3"></i>
                    </a>
                </td>
            </tr> ?>
            <?php    
                $count++; 
        } //foreach de arreay sesion
    }


    public function addproductoCombo(){
        $cant = $this->input->post('cant');
        $prod = $this->input->post('prod');
        $tipo = $this->input->post('tipo');
        $count = $this->input->post('count_combo');
        //$id_pro_sub = $this->input->post('id_producto');

        $personalv=$this->ModeloProductos->getproducto($prod);
        //$oProducto = new Producto();
        foreach ($personalv->result() as $item){
            $id = $item->productoid;
            $codigo = $item->codigo;
            $nombre = $item->nombre;
            $preciopos = $item->preciopos;
            $tipo_prod = $item->tipo;
            $cant_prod = $item->cant_tot_prods;

            //if($tipo=="0")
                $costo=$preciopos;
            /*if($tipo=="1")
                $costo=$preciouber;
            if($tipo=="2")
                $costo=$preciorappi;
            if($tipo=="3")
                $costo=$preciodid;*/
        }        
        //======================================================================
        $html="";
        $cantotal=$cant*$costo;
                
        $html.='<tr class="producto_'.$count.'" id="producto_tabla">                                        
                <td>
                    <input type="hidden" name="vsproid" id="vsproid" value="'.$prod.'">
                    <input type="hidden" id="tipo_prod" value="'.$tipo_prod.'">
                    <input type="hidden" id="de_prod" value="0">
                    <input type="hidden" id="cant_prod" value="'.$cant_prod.'">
                    <input type="hidden" id="id_detalle_venta" value="0">
                    '.$codigo.'
                </td>                                        
                <td>
                    <input type="number" name="vscanti" id="vscanti" value="'.$cant.'" readonly style="background: transparent;border: 0px; width: 80px;">
                </td>                                        
                <td>'.$nombre.'</td>                                        
                <td>$ <input type="text" name="vsprecio" id="vsprecio" value="'.$costo.'" readonly style="background: transparent;border: 0px;width: 100px;"></td>                                        
                <td>$ <input type="text" class="vstotal" name="vstotal" id="vstotal" value="'.$cantotal.'" readonly style="background: transparent;border: 0px; width: 100px;"></td>                                        
                <td>                                            
                    <a class="danger" data-original-title="Eliminar" title="Eliminar" onclick="deletepro('.$count.','.$tipo_prod.')">
                        <i class="ft-trash font-medium-3"></i>
                    </a>
                </td>
            </tr>';

        //if($tipo_prod==1 && $cant_prod>1){
            $idcat=0; $idcat2=0; $aux=0;
            $get_catprod=$this->ModeloProductos->getCategosProdsCombo($id,$tipo_prod);
            foreach ($get_catprod as $cp){ 
                $cont_aux=0; $sel=""; 
                if($idcat != $cp->id_categoria && $cont_aux<=0){

                    /*log_message('error', 'idcat: '.$idcat); 
                    log_message('error', 'id_categoria: '.$cp->id_categoria); */
                    $getpc=$this->ModeloProductos->getProdsCombo($cp->id_categoria);
                    foreach ($getpc as $cp2){
                        $select="";
                        if($cp2->id_producto==$fila["id_pro_sub"]){
                            $select="selected";
                        }
                        $sel.='<option '.$select.' data-cod="'.$cp2->codigo.'" value="'.$cp2->id_producto.'">'.$cp2->nombre.'</option>';
                    }
                    foreach ($getpc as $cp2){ //log_message('error', 'nombre: '.$cp2->nombre); 
                        //if($cont_aux<=1){ 
                        //$cont_aux++; 
                        if($cp2->id_categoria != $idcat2){
                            $aux++;
                                
                            $html .='<tr id="de_producto" class="de_producto_'.$count.'">                                        
                                <td>
                                    <input type="hidden" name="vsproid" id="vsproid" value="'.$id.'">
                                    <input type="hidden" id="cant_prod'.$aux.'_'.$count.'" value="'.$cant_prod.'">
                                    <input type="hidden" id="tipo_prod" value="'.$cp2->tipo.'">
                                    <span id="cod_prod'.$aux.'_'.$count.'">'.$cp2->codigo.'</span>
                                    <input type="hidden" id="de_prod" value="1">
                                    <input type="hidden" id="id_detalle_venta" value="0">
                                </td>                                        
                                <td>
                                    <input type="number" name="vscanti" id="vscanti" value="'.$cp2->cantidad.'" readonly style="background: transparent;border: 0px; width: 80px;">
                                </td>                                        
                                <td> <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Producto del combo:</label>
                                        <select class="form-control prod_sel'.$aux.'_'.$count.'" id="nombre_prod" onchange="cambioProducto('.$aux.','.$count.',this.value)">
                                            <option value="">Elige un producto:</option>
                                            '.$sel.'
                                        </select>
                                    </div>
                                    </div>
                                </td>                                        
                                <td>$ <input type="text" name="vsprecio" id="vsprecio" value="0" readonly style="background: transparent;border: 0px;width: 100px;"></td>                                        
                                <td>$ <input type="text" class="vstotal" name="vstotal" id="vstotal" value="0" readonly style="background: transparent;border: 0px; width: 100px;"></td>                                        
                                <td>                                            
        
                                </td>
                            </tr>';
                        }  
                        $idcat2 = $cp2->id_categoria;
                    }
                }//if categoria
                $idcat = $cp->id_categoria;
                if($idcat == $cp->id_categoria){
                   $cont_aux++; 
                }

            }//foreach principiapl
            
        //} //if
        echo $html;
    }

    public function buscarComanda(){
        $id_venta = $this->input->post('id_venta');

        $count=0;
        $getPC=$this->ModeloVentas->getProdsVentaComanda($id_venta,0);
        foreach ($getPC as $item){
            $id = $item->id_producto;
            $codigo = $item->codigo;
            $nombre = $item->nombre;
            $tipo = $item->tipo;
            $mesa = $item->mesa;
            $id_detalle_venta=$item->id_detalle_venta;
            $cantidad = $item->cantidad;
            $precio = $item->precio;
            $cant_prod = $item->cant_tot_prods;
            $count++;
            $cantotal=$cantidad*$precio;

            if($tipo==0){
                echo '<tr class="producto_'.$count.'" id="producto_tabla">                                        
                    <td>
                        <input type="hidden" name="vsproid" id="vsproid" value="'.$id.'">
                        <input type="hidden" id="tipo_prod" value="'.$tipo.'">
                        <input type="hidden" id="de_prod" value="'.$tipo.'">
                        <input type="hidden" id="cant_prod" value="'.$cant_prod.'">
                        <input type="hidden" id="id_detalle_venta" value="'.$id_detalle_venta.'">
                        '.$codigo.'
                    </td>                                        
                    <td>
                        <input type="number" name="vscanti" id="vscanti" value="'.$cantidad.'" readonly style="background: transparent;border: 0px; width: 80px;">
                    </td>                                        
                    <td>'.$nombre.'</td>                                        
                    <td>$ <input type="text" name="vsprecio" id="vsprecio" value="'.$precio.'" readonly style="background: transparent;border: 0px;width: 100px;"></td>                                        
                    <td>$ <input type="text" class="vstotal" name="vstotal" id="vstotal" value="'.$cantotal.'" readonly style="background: transparent;border: 0px; width: 100px;"></td>                                        
                    <td>                                            
                        <a class="danger" data-original-title="Eliminar" title="Eliminar" onclick="deleteproComanda('.$id_detalle_venta.')">
                            <i class="ft-trash font-medium-3"></i>
                        </a>
                    </td>
                </tr>';
            }
        } //foreach
    }

    public function buscarComandaCombo(){
        $id_venta = $this->input->post('id_venta');

        $count=0; $html=""; $aux=0;
        $getPC=$this->ModeloVentas->getProdsVentaComanda($id_venta,1);
        foreach ($getPC as $item){
            $id = $item->id_producto;
            $codigo = $item->codigo;
            $nombre = $item->nombre;
            $tipo = $item->tipo;
            $mesa = $item->mesa;
            $id_detalle_venta=$item->id_detalle_venta;
            $cantidad = $item->cantidad;
            $precio = $item->precio;
            $cant_prod = $item->cant_tot_prods;
            $count++;
            $cantotal=$cantidad*$precio;

            if($tipo==1){
                $aux++;
                $html.= '<tr class="producto_'.$count.'" id="producto_tabla">                                        
                    <td>
                        <input type="hidden" name="vsproid" id="vsproid" value="'.$id.'">
                        <input type="hidden" id="tipo_prod" value="'.$tipo.'">
                        <input type="hidden" id="de_prod" value="'.$tipo.'">
                        <input type="hidden" id="cant_prod" value="'.$cant_prod.'">
                        <input type="hidden" id="id_detalle_venta" value="'.$id_detalle_venta.'">
                        '.$codigo.'
                    </td>                                        
                    <td>
                        <input type="number" name="vscanti" id="vscanti" value="'.$cantidad.'" readonly style="background: transparent;border: 0px; width: 80px;">
                    </td>                                        
                    <td>'.$nombre.'</td>                                        
                    <td>$ <input type="text" name="vsprecio" id="vsprecio" value="'.$precio.'" readonly style="background: transparent;border: 0px;width: 100px;"></td>                                        
                    <td>$ <input type="text" class="vstotal" name="vstotal" id="vstotal" value="'.$cantotal.'" readonly style="background: transparent;border: 0px; width: 100px;"></td>                                        
                    <td>                                            
                        <a class="danger" data-original-title="Eliminar" title="Eliminar" onclick="deleteproComanda('.$id_detalle_venta.')">
                            <i class="ft-trash font-medium-3"></i>
                        </a>
                    </td>
                </tr>';  
                $getPC=$this->ModeloVentas->getProdsVentaComandaCombo($id_venta);
                foreach ($getPC as $c){ 
                    $html .='<tr id="de_producto" class="de_producto_'.$count.'">                                        
                        <td>
                            <input type="hidden" name="vsproid" id="vsproid" value="'.$c->id_producto.'">
                            <input type="hidden" id="cant_prod'.$aux.'_'.$count.'" value="'.$c->cant_tot_prods.'">
                            <input type="hidden" id="tipo_prod" value="'.$tipo.'">
                            <span id="cod_prod'.$aux.'_'.$count.'">'.$c->codigo.'</span>
                            <input type="hidden" id="de_prod" value="1">
                        </td>                                        
                        <td>
                            <input type="number" name="vscanti" id="vscanti" value="'.$c->cantidad.'" readonly style="background: transparent;border: 0px; width: 80px;">
                        </td>                                        
                        <td>'.$c->nombre.'</td>                                         
                        <td>$ <input type="text" name="vsprecio" id="vsprecio" value="0" readonly style="background: transparent;border: 0px;width: 100px;"></td>                                        
                        <td>$ <input type="text" class="vstotal" name="vstotal" id="vstotal" value="0" readonly style="background: transparent;border: 0px; width: 100px;"></td>                                        
                        <td>                                            

                        </td>
                    </tr>';  
                }                                
            }
        } //foreach
        echo $html;
    }

    function productoclear(){
        unset($_SESSION['pro']);
        $_SESSION['pro']=array();
        unset($_SESSION['can']);
        $_SESSION['can']=array();
        unset($_SESSION['tipo_prod']);
        $_SESSION['tipo_prod']=array();
    }
    function deleteproducto(){
        $idd = $this->input->post('idd');
        unset($_SESSION['pro'][$idd]);
        $_SESSION['pro']=array_values($_SESSION['pro']);
        unset($_SESSION['can'][$idd]);
        $_SESSION['can'] = array_values($_SESSION['can']); 
        unset($_SESSION['tipo_prod'][$idd]);
        $_SESSION['tipo_prod'] = array_values($_SESSION['tipo_prod']); 
    }
    function prodComboSelect(){
        $id_producto = $this->input->post('id_producto');
        //log_message('error', 'id_pro_sub post: '.$id_producto);
         if(in_array($oProducto, $_SESSION['pro'])){  
            $idx = array_search($oProducto, $_SESSION['pro']);
            if($_SESSION['tipo_prod'][$idx]==1){
                array_push($_SESSION['id_pro_sub'],$id_producto);
            }
            //log_message('error', 'id_pro_sub agregardo de un combo existente: '.var_dump($_SESSION['id_pro_sub']));
        }
        //array_push($_SESSION['id_pro_sub'],$id_producto);
        //log_message('error', 'id_pro_sub: '.var_dump($_SESSION['id_pro_sub']));
        //echo $_SESSION['id_pro_sub'];
    }
    function ingresarventa(){
        $uss = $this->input->post('uss');
        $cli = $this->input->post('cli');
        $tipoc = $this->input->post('tipo_costo');
        $mpago = $this->input->post('mpago');
        $pagado = $this->input->post('pagado');
        $mesa = $this->input->post('mesa');
        $desc = $this->input->post('desc');
        $descu = $this->input->post('descu');
        $sbtotal = $this->input->post('sbtotal');
        $total = $this->input->post('total');
        $efectivo = $this->input->post('efectivo');
        $tarjeta = $this->input->post('tarjeta');
        $fecha = date("Y-m-d H:i:s");
        if($mesa=="")
            $mesa=0;
        $id=$this->ModeloVentas->ingresarventa($uss,$cli,$tipoc,$mpago,$sbtotal,$desc,$descu,$total,$efectivo,$tarjeta,$fecha,$pagado,$mesa);
        echo $id;
    }
    function ingresarventapro(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        $cont_prods_total=0;
        for ($i=0;$i<count($DATA);$i++) { 
            $idventa = $DATA[$i]->idventa;
            $producto = $DATA[$i]->producto;
            $cantidad = $DATA[$i]->cantidad;
            $precio = $DATA[$i]->precio;
            $tipo_prod = $DATA[$i]->tipo_prod;
            $de_prod = $DATA[$i]->de_prod;
            $id_detalle_venta = $DATA[$i]->id_detalle_venta;

            //log_message('error', 'id_detalle_venta: '.$id_detalle_venta);
            /*$get_ok=$this->ModeloCatalogos->getselectwheren("productos",array("productoid"=>$producto));
            foreach ($get_ok->result() as $key ) {
                $stockok=$key->stockok;
            }*/
            //$cont_prods_total++;
            $cont=0; $contpt=0; $cont2=0;
            if($tipo_prod==0 && $de_prod==0){
                $get_recete=$this->ModeloCatalogos->getselectwheren("productos_insumos",array("productoid"=>$producto,"activo"=>1));
                foreach ($get_recete->result() as $key){
                    $cont++;            
                }
            }/*else if($tipo_prod>0 && $de_prod==0){ //combo
                $contpi=0;
                $get_combo=$this->ModeloCatalogos->getselectwheren("productos_combo",array("id_producto"=>$producto,"activo"=>1));//prod del combo
                foreach ($get_combo->result() as $pc){
                    $contpi++;
                    $get_recete=$this->ModeloCatalogos->getselectwheren("productos_insumos",array("productoid"=>$pc->id_producto_combo,"activo"=>1));
                    foreach ($get_recete->result() as $key ) {
                        $contpt++;
                        $cont2++;
                        $cant=$key->cantidad;
                        $insumo=$key->insumo;
                        $tot_cant = $cantidad*$cant;    
                        //log_message('error', 'cant: '.$cant);             
                        $this->ModeloVentas->editaStockReceta($insumo,$tot_cant);
                    }
                    //log_message('error', 'cont_prods_total: '.$cont_prods_total);
                    log_message('error', 'contpt: '.$contpt);
                    log_message('error', 'contpi: '.$contpi);

                    if($contpt==0){ //producto cerrado, sin receta
                        $this->ModeloVentas->ingresarventadCombo($pc->id_producto_combo,$cantidad); //UPDATE productos
                    }else if($contpt>0 && $contpi>1 && $contpi<=2){ //guarda combo + pt
                        $this->ModeloVentas->ingresarventadReceta($idventa,$producto,$cantidad,$precio); //INSERT INTO venta_detalle
                    }             
                }
            }*/

            //log_message('error', 'cont: '.$cont);
            if($cont==0 && $tipo_prod==0 && $de_prod==0 && $id_detalle_venta==0){//producto de empaque
                $this->ModeloVentas->ingresarventad($idventa,$producto,$cantidad,$precio);
            }else if($cont>0 && $tipo_prod==0 && $de_prod==0 && $id_detalle_venta==0){ //okv-- es para producto de receta
                $this->ModeloVentas->ingresarventadReceta($idventa,$producto,$cantidad,$precio);
                foreach ($get_recete->result() as $key ) {
                    $cant=$key->cantidad;
                    $insumo=$key->insumo;
                    $tot_cant = $cantidad*$cant;    
                    //log_message('error', 'cant: '.$cant);             
                    $this->ModeloVentas->editaStockReceta($insumo,$tot_cant);
                }
            }

            /* ******************VENTAS DE COMBOS DINAMICOS********************** */
            if($tipo_prod==1 && $de_prod==0 && $id_detalle_venta==0){
                $this->ModeloVentas->ingresarventadReceta($idventa,$producto,$cantidad,$precio); //INSERTAR VENTA DETALLE COMO COMBO
            }
            if($tipo_prod==0 && $de_prod==1 && $id_detalle_venta==0){ //combo - YA SE GUARDÓ LA VENTA
                $get_recete=$this->ModeloCatalogos->getselectwheren("productos_insumos",array("productoid"=>$DATA[$i]->id_prod_combo,"activo"=>1));
                foreach ($get_recete->result() as $key ) {
                    $cant=$key->cantidad;
                    $insumo=$key->insumo;
                    $tot_cant = $cantidad*$cant;    
                    //log_message('error', 'cant: '.$cant);             
                    $this->ModeloVentas->editaStockReceta($insumo,$tot_cant);
                }
            }
        }
    }

    function ingresarventaproCombo(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        $cont_prods_total=0;
        for ($i=0;$i<count($DATA);$i++) { 
            $idventa = $DATA[$i]->idventa;
            $producto = $DATA[$i]->producto;
            $cantidad = $DATA[$i]->cantidad;
            $precio = $DATA[$i]->precio;
            $tipo_prod = $DATA[$i]->tipo_prod;
            $de_prod = $DATA[$i]->de_prod;

            log_message('error', 'tipo_prod: '.$tipo_prod); 
            /* ******************VENTAS DE COMBOS DINAMICOS********************** */
            if($tipo_prod==1 && $de_prod==0){
                $this->ModeloVentas->ingresarventadReceta($idventa,$producto,$cantidad,$precio); //INSERTAR VENTA DETALLE COMO COMBO
            }
            if($tipo_prod==0 && $de_prod==1){ //combo - YA SE GUARDÓ LA VENTA
                if($DATA[$i]->id_prod_combo!=0){
                    $this->ModeloCatalogos->Insert('venta_combo_prods',array("id_venta"=>$idventa,"id_producto"=>$DATA[$i]->id_prod_combo,"reg"=>date("Y-m-d H:i:s"))); //inserta el sabor/producto del combo
                }
                $contpi=0; $contpt=0; 
                $get_recete=$this->ModeloCatalogos->getselectwheren("productos_insumos",array("productoid"=>$DATA[$i]->id_prod_combo,"activo"=>1)); //obtiene insumos del producto de acuerdo al prod seleccionado de categorias.
                foreach ($get_recete->result() as $key ) {
                    $contpt++;
                    $cant=$key->cantidad;
                    $insumo=$key->insumo;
                    $tot_cant = $cantidad*$cant;    
                    //log_message('error', 'cant: '.$cant);             
                    $this->ModeloVentas->editaStockReceta($insumo,$tot_cant);
                }
                log_message('error', 'contpt: '.$contpt); 
                if($tipo_prod==0 && $contpt==0){ //producto cerrrado sin receta del combo
                    $get_combo=$this->ModeloCatalogos->getselectwheren("productos",array("productoid"=>$DATA[$i]->id_prod_combo));//prod del combo
                    foreach ($get_combo->result() as $pc){
                        if($pc->tipo==0){ //producto cerrado, sin receta
                            $this->ModeloVentas->ingresarventadCombo($pc->productoid,$cantidad); //UPDATE productos
                        }           
                    }
                }
            }
        }
    }

    function abrirturno(){
        $cantidad = $this->input->post('cantidad');
        $nombre = $this->input->post('nombre');
        $fecha =date('Y').'-'.date('m'). '-'.date('d'); 
        $horaa =date('H:i:s');
        $this->ModeloVentas->abrirturno($cantidad,$nombre,$fecha,$horaa);
    }
    function cerrarturno(){
        $id = $this->input->post('id');
        $fecha =date('Y').'-'.date('m'). '-'.date('d'); 
        $horaa =date('H:i:s');
        $this->ModeloVentas->cerrarturno($id,$horaa);
    }
    function consultarturno(){
        $id = $this->input->post('id');
        $vturno=$this->ModeloVentas->consultarturno($id);
        foreach ($vturno->result() as $item){
            $fecha= $item->fecha;
            $fechac= $item->fechacierre;
            $horaa= $item->horaa;
            $horac= $item->horac;
        }
        if ($horac=='00:00:00') {
            $horac =date('H:i:s');
            $fechac =date('Y-m-d');
        }
        $totalventas=$this->ModeloVentas->consultartotalturno($fecha,$horaa,$horac,$fechac);
        echo "<p><b>Total: ".number_format($totalventas,2,'.',',')."</b></p>";
        $totalpreciocompra=$this->ModeloVentas->consultartotalturno2($fecha,$horaa,$horac,$fechac);
        $obed =$totalventas-$totalpreciocompra;
        echo "<p><b>Utilidad: ".number_format($obed,2,'.',',')."</b></p>";
        $productos =$this->ModeloVentas->consultartotalturnopro($fecha,$horaa,$horac,$fechac);
        echo "<div class='panel-body table-responsive' id='table'>
                            <table class='table table-striped table-bordered table-hover' id='data-tables2'>
                            <thead class='vd_bg-green vd_white'>
                                <tr>
                                    <th>Producto</th>
                                    <th>Cantidad</th>
                                    <th>Precio</th>
                                    
                                </tr>
                            </thead>
                            <tbody>";
        foreach ($productos->result() as $item){
            echo "<tr><td>".$item->producto."</td><td>".$item->cantidad."</td><td> $".$item->precio."</td></tr>";
        }
        echo "</tbody> </table></div>";
        echo "<div style='    position: absolute; float: left; top: 0; left: 800px; background: white; width: 250px; z-index: 20;'>
                            <table class='table table-striped table-bordered table-hover' id='data-tables'>
                            <thead>
                                <tr>
                                    <td colspan='4' style='text-align: center;'>Productos mas vendido</td>
                                </tr>
                            </thead>
                            <tbody>";
        $productosmas =$this->ModeloVentas->consultartotalturnopromas($fecha,$horaa,$horac,$fechac);
        foreach ($productosmas->result() as $item){
            echo "<tr><td colspan='2' style='text-align: center;'>".$item->total." </td><td colspan='2' style='text-align: center;'>".$item->producto." </td></tr>";
        }
        echo "</tbody> </table></div>";

    }

    public function editarVentaPago(){
        $id_venta = $this->input->post("id_venta");
        $data = $this->input->post();
        unset($data["id_venta"]);
        $this->ModeloCatalogos->updateCatalogo("ventas",$data,array("id_venta"=>$id_venta));
    }

    public function getselectComandas(){
        $html="";
        $get_com=$this->ModeloCatalogos->getselectwheren("ventas",array("pagado"=>0,"cancelado"=>0,"tipo_costo"=>2));
        $html.= '<option value="0"></option>>';
        foreach ($get_com->result() as $c) {
            $html.= '<option data-mesac="'.$c->mesa.'" value="'.$c->id_venta.'">Mesa: '.$c->mesa.' - '.$c->reg.'</option>';
        }
        echo $html;
    }

}